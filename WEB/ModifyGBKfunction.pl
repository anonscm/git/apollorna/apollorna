#!/usr/bin/perl
# syntax: ModifyGBKfunction.pl <in_file> <out_file> 
# Inputs: <in_file>  a .gbk file describing the annotation of an organism
#         <out_file> the same as <in_file> except that: 
#                      - the carriage return is removed
#                        at the end of lignes of the type /note= ... :
#                      - '&' is replaced by 'and' everywhere
#                    To avoid errors when loading the file in Apollo
# Example of in_file:
#...
#     CDS             8238..9191
#                     /gene="talB"
#                     /locus_tag="b0008"
#                     /EC_number="2.2.1.2"
#                     /function="enzyme; Central intermediary metabolism:
#                     Non-oxidative branch, pentose pathway"
#                     /function="1.7.3 metabolism; central intermediary
#...
# Example of out_file:
#...
#     CDS             8238..9191
#                     /gene="talB"
#                     /locus_tag="b0008"
#                     /EC_number="2.2.1.2"
#                     /function="enzyme; Central intermediary metabolism:                     Non-oxidative branch, pentose pathway"
#                     /function="1.7.3 metabolism; central intermediary
#...

use strict;
use warnings;

if ($#ARGV != 1) {
die "ERROR: bad number of arguments.\nSyntax: ModifyGBKfunction.pl <in_file> <out_file>\n";
}

open (IN, $ARGV[0]) || die "ERROR: couldn't open the in file!";
open (OUT, ">".$ARGV[1]) || die "ERROR: couldn't open the out file!";

my $val_function="";
my $b_function="";
while(<IN>) {
   $_ =~ s/&/and/g;
   if ($b_function && rindex( $_, "/")) {
    # end of a function
    print OUT $val_function;
    $b_function="";
    $val_function="";
  }
  if ($b_function) {
      if (substr($_, length($_)-2,1) eq ":") {
        $val_function = $val_function . substr($_,1,length($_)-1);
      } else {
        $val_function = $val_function . $_ ;
      }
  } else {
    if (index( $_, "/function=") != -1) {
      # sometimes several /function
      $b_function="yes";
      if (substr($_, length($_)-2,1) eq ":") {
        $val_function = $val_function . substr($_,0,length($_)-2);
      } else {
        $val_function = $val_function . $_ ;
      }
    } else {
      print OUT $_ ;
    }
  }
}


close(IN);
close(OUT);
