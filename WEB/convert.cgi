#!/usr/bin/perl
=pod 

=head1 NAME
convert.pl
This CGI program allows the conversion of software output data (output of TranstermHP, MOSAIC, CRISPRFinder) and of GFF data
to Apollo reading formats (GFF and GAME XML)

=head1  SYNOPSIS 
	&convert.pl
=head1 DESCRIPTION
Read the data to convert in a textarea or in a loaded file
According to the selected input and output formats, convert the data to the specified format
and display the result in the current html page.

=cut

use CGI;
use strict;

my $APOLLO_EXECUTABLE = "/var/www/ApolloRNA/Apollo/apollo";
my $IP_FOR_DISPLAY = "147.99.96.163:0.0";
my $TMP_DIRECTORY = "/tmp/apollorna/"; # name of the tmp directory


MAIN:
{
	my($query) = new CGI;   # Create a new CGI object
        my($input_data);	# The input text

	# Create HTTP header and HTML start
	print $query->header, 
	$query->start_html('Convert result');

	# Get the data to convert: read in the textearea and if it is empty, load the data of the file
	if ($query->param('seqtext') ne '') {
		$input_data = $query->param('seqtext');
	}
	else {
		# load the file
		my($upload_filehandle) = $query->upload('uploaded_file') ;
		while (defined(my $line = <$upload_filehandle>)) {
			$input_data = $input_data.$line;
		}	
	}
	if ($input_data ne "") {
		# convert the input data
		my $result = &Convert($input_data, $query);
		# display the converted data
		if ($result ne "") {
			print "<PRE>";
			print $result;
			print "</PRE>";
			open COUNT, ">>count/convert_OK.txt" or dienice("Cant open file to WRITE");
			print COUNT localtime() . "\t" . $ENV{'REMOTE_ADDR'} . "\t" . $ENV{'REMOTE_HOST'} . "\t" . $query->param('input_format') . "\t" . $query->param('output_format') . "\t" . "\n";
			close COUNT;

		} 
		else {
			print "NO CONVERSION: check the input and output checked formats.";
			open COUNT, ">>count/convert_PB.txt" or dienice("Cant open file to WRITE");
			print COUNT localtime() . "\t" . $ENV{'REMOTE_ADDR'} . "\t" . $ENV{'REMOTE_HOST'} . "\t" . $query->param('input_format') . "\t" . $query->param('output_format') . "\t" . "\n";
			close COUNT;
		}
	} 
	else {
		print "ERROR: no data was loaded";
	}
	print $query->end_html;    # HTML end
}

=head2 function Convert

 Title        : Convert
 Usage        : &Convert($input_data, $query)
 Prerequisite : none
 Function     : Convert the data containing in the string $input_data 
		The input and output formats are reading in the query object. $
		If output format is GAMEXML:
			- If input data is GFF or Genbank, directly convert $input_data to GAMEXML,
			- Else, first convert in GFF, second convert the GFF to GAME XML
 Returns      : the converting data
 Args         : $input_data: string which represents input data
		$query: CGI object, which contains the conversion formats
 Globals      : none

=cut
sub Convert
{
	my ($input_data, $query) = @_;
	my ($result); # converted data

	# Get input and output formats
	my ($input_format)  = $query->param('input_format');
	my ($output_format) = $query->param('output_format');

	# Convert data to GFF if GFF OR GAME XML output format was choosen
	if ( ($output_format eq 'outgff') ||
	     (($output_format eq 'gamexml') && ($input_format ne 'gff') && ($input_format ne 'gbk')) ) {
		my @a_input_data = split(/\n/, $input_data);
		if ($input_format eq 'transterm') {
			$result = &ConvertTranstermToGFF(\@a_input_data); # convert transtermHP data to GFF
		} elsif ($input_format eq 'crispr') {
			$result = &ConvertCrisprToGFF(\@a_input_data); # convert crisprFinder data to GFF
		} elsif ($input_format eq 'mosaic') {
			$result = &ConvertMosaicToGFF(\@a_input_data); # convert MOSAIC data to GFF
		} elsif ($input_format eq 'patscan') {
			$result = &ConvertPatScanToGFF(\@a_input_data); # convert PatScan data to GFF
		}
	} 

	# If the output format is GAME XML 
	if ($output_format eq 'gamexml') {
		# if input_format is GBK, convert GBK data to GAMEXML
		if ($input_format eq 'gbk')  {
			$input_data =~ s/.*\/transl_table=.*\n//g;	# delete the lines containing /transl_table=
			$result = &ConvertToGameXml($input_data, "genbank");
		} # Else, convert GFF data to GAMEXML 
		else {
			# if the input format was GFF, get directly the data from the input data
			if ($input_format eq 'gff') {
				$result = $input_data;
			}
			# Add lines to the GFF data to note the start and stop genomic sequence positions. 
			if ($query->param('begin') =~ /^\d+$/ ) {
				$result = "begin\t.\tsize\t".$query->param('begin')."\t".$query->param('begin')."\t1\t.\t.\n".$result;	
			}
			if ($query->param('end') =~ /^\d+$/ ) {
				$result = $result."end\t.\tsize\t".$query->param('end')."\t".$query->param('end')."\t1\t.\t.\n";
			}

			# Convert to GAMEXML
			if ($result ne "") {
				my ($result2) = &ConvertToGameXml($result, "gff");
				$result = $result2;
			}
		}
	}
	return $result;
}

=head2 function ConvertTranstermToGFF

 Title        : ConvertTranstermToGFF
 Usage        : ConvertTranstermToGFF(\@a_input_data)
 Prerequisite : none
 Function     : Convert the transtermHP output to GFF and return the result: 
		Each line which starts with "TERM" is converted in a GFF line.
 Returns      : the converting data
 Args         : $ra_input_data: reference of an array, composed of the lines of the transtermHP output
 Globals      : none

=cut 
sub ConvertTranstermToGFF
{
	my ($ra_input_data) = @_;
	my($start);	# start position of the terminator
	my($end);	# stop position of the terminator
	my ($result);	# string which contains the result of the conversion in GFF format

	# for each line of the data
	foreach my $transterm_line (@$ra_input_data){
  		if ($transterm_line =~ /(TERM)\s([0-9]+)\s+([0-9]+)\s-\s([0-9]+)\s+(\+|\-)\s+([a-zA-Z])\s+([0-9]+)/){
			if ($3 < $4) {
				$start=$3;
				$end=$4;
			} 
			else{
				$start=$4;
				$end=$3;
			}
			$result = $result."TERM".$2."\tTransTermHP\tterminator\t".$start."\t".$end."\t$7\t".$5."\t.\n";
		} 
	}
	return $result;
}

=head2 function ConvertCrisprToGFF

 Title        : ConvertCrisprToGFF
 Usage        : ConvertCrisprToGFF(\@a_data)
 Prerequisite : none
 Function     : Convert the CrisprFinder output to GFF and return the result: 
		Each line which starts with "CRISPR id :" is converted in a GFF line.
 Returns      : the converting data
 Args         : $ra_data : reference of an array, composed of the lines of the CrisprFinder output
 Globals      : none

=cut 
sub ConvertCrisprToGFF
{
	my ($ra_data) = @_;
	my ($result);	# string which contains the result of the conversion in GFF format

	# for each line of the data
	foreach my $line (@$ra_data){
		if ($line =~ /CRISPR id : (([a-zA-Z0-9]|_)+)\s\[([0-9]+)\s,\s([0-9]+)\s\]/){
			$result = $result.$1."\tCRISPRFinder\trepeat_region\t".$3."\t".$4."\t.\t.\t.\n";
		}
	}
	return $result;
}

=head2 function ConvertMosaicToGFF

 Title        : ConvertMosaicToGFF
 Usage        : ConvertMosaicToGFF(\@a_data)
 Prerequisite : none
 Function     : Convert the MOSAIC loop output to GFF and return the result: 
		Each line is converted in a GFF line.
 Returns      : the converting data
 Args         : $ra_data: reference of an array, composed of the lines of the MOSAIC loop output
 Globals      : none

=cut 
sub ConvertMosaicToGFF
{
	my ($ra_data) = @_;
	my ($result);	# string which contains the result of the conversion in GFF format

	# for each line of the data
	foreach my $line (@$ra_data){
		if ($line =~ /(([a-zA-Z0-9]|_|-)+)\t([0-9]+)\t([0-9]+)/){
			$result = $result.$1."\tMOSAIC\tmisc_feature\t".$3."\t".$4."\t.\t.\t.\n";
		}
	}
	return $result;
}

=head2 function ConvertPatScanToGFF

 Title        : ConvertPatScanToGFF
 Usage        : ConvertPatScanToGFF(\@a_data)
 Prerequisite : none
 Function     : Convert the PatScan output to GFF and return the result: 
		Each line is converted in a GFF line.
 Returns      : the converting data
 Args         : $ra_data: reference of an array, composed of the lines of the PatScan output
 Globals      : none

=cut 
sub ConvertPatScanToGFF
{
	my ($ra_data) = @_;
	my ($result);	# string which contains the result of the conversion in GFF format
	my($start);	# start position of the pattern
	my($end);	# stop position of the pattern
	my ($strand);	# strand of the pattern
	my $pattern_name;# name of a pattern
	my $cpt = 1;	# pattern index

	# for each line of the data
	foreach my $line (@$ra_data){
		if ($line =~ /(.+)\[([0-9]+)\,([0-9]+)\]/){
			if ($2 < $3) {
				$start=$2;
				$end=$3;
				$strand="+";
			} 
			else{
				$start=$3;
				$end=$2;
				$strand="-";
			}
			$pattern_name = $1;
			$pattern_name =~ s/^>|:$//g;
			$pattern_name = $pattern_name."_pattern".$cpt;
			$cpt++;
			$result = $result.$pattern_name."\tPatScan\tmisc_feature\t".$start."\t".$end."\t.\t".$strand."\t.\n";
			
		}
	}
	return $result;
}

=head2 function ConvertToGameXml

 Title        : ConvertToGameXml
 Usage        : ConvertToGameXml($input_data, $input_format)
 Prerequisite : $input_data respects the syntax of its format
 Function     : Convert the $input_data from $input_format (gff, genbank) to Game XML and return the result: 
		Use apollo executable file to convert
 Returns      : the converting data
 Args         : $input_data: string containing data to convert in GAME XML
		$input_format: string containing the format of the $input_data
 Globals      : none

=cut 
sub ConvertToGameXml
{
	my ($input_data, $input_format) = @_;
	my ($result); # string which contains the result of the conversion in GAMEXML format

	# create apollorna temporary directory
	if (!-d $TMP_DIRECTORY) {
		mkdir($TMP_DIRECTORY,0777);
	}

	my $root_name_file = `date +\%s.\%N`; # root of the name of the temporary files
	$root_name_file =~ s/\s+//g;
	my $input_file = $TMP_DIRECTORY.$root_name_file.".in"; # way of the temporary input file
	my $output_file = $TMP_DIRECTORY.$root_name_file.".xml"; # way of the temporary xml file

	$result = &ConvertWithApollo($input_data, $input_file, $output_file, $input_format, "game");

	# change some output lines
	$result =~ s/.*Analysis.*\n//;
	$result =~ s/\<map_position seq.*/\<map_position\>/;
	# substitute special html characters
	$result =~ s/\</&lt;/g;
	$result =~ s/\>/&gt;/g;
	return $result;
}

=head2 function ConvertWithApollo

 Title        : ConvertWithApollo
 Usage        : ConvertWithApollo($input_data, $input_file, $output_file, $input_format, $output_format)
 Prerequisite : none
 Function     : Convert data $input_data from $input_format to $output_format. Use apollo executable file to convert.
		- Create a file, named $input_file, containing input_data
		- Call Apollo to convert data in the file, and create a output file, named $output_file
		- Read data included in $output_file, and return it.
 Returns      : the converting data
 Args         : $input_data: string containing data to convert in GAME XML
		$input_file: name of the file in which pastes $input_data. It is read by Apollo
		$output_file: name of the file created by Apollo containing the result data.
		$input_format: string containing the format of the $input_data (gff, genbank)
		$output_format: string containing the output format
 Globals      : none

=cut 
sub ConvertWithApollo
{	
	my ($input_data, $input_file, $output_file, $input_format, $output_format) = @_;
	my $output_data;

	# create a file containing the data
	open(INPUT_FILE, ">".$input_file);
	print INPUT_FILE $input_data;
	close(INPUT_FILE);
	
	# change DISPLAY environment variable and compute Apollo
	$ENV{DISPLAY} = $IP_FOR_DISPLAY;
	system $APOLLO_EXECUTABLE." -i ".$input_format." -f ".$input_file." -o ".$output_format." -w ".$output_file." > /dev/null";

	# open xml file and read xml data
	open(OUTPUT_FILE, $output_file) || print ("Impossible to open ".$output_file);
	while (my $line = <OUTPUT_FILE>) {
		$output_data = $output_data.$line;
	}
	close(OUTPUT_FILE);

	# delete temporary files
	unlink($output_file) || die "ERROR: file ".$output_file." doesn't deleted";
	unlink($input_file) || die "ERROR: file ".$input_file." doesn't deleted";

	return $output_data;
}
