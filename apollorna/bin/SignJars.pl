#!/usr/bin/perl -w
# bin/SignJars.pl signed the .jar files necessary for ApolloRNA web start 
# Signed jar files are put in the same directory of jar files: $APOLLO_ROOT/jars
# Key store is stored in the direcoty: $APOLLO_ROOT/bin
# !!!!!! ONLY for Linux
# NEED TO DELETE EXISTING $APOLLO_ROOT/jars/*.signed.jar files

# PARAMETERS to set #################################################
$APOLLO_ROOT = $ENV{"APOLLO_ROOT"};
$JAR_DIR = "$APOLLO_ROOT/jars";

$alias = "ApolloRNA";
$keypass = "ApolloRNA_keypass";
$storepass = "ApolloRNA_storepass";
$keystore = $APOLLO_ROOT."/bin/ApolloRNA_keystore";
$validity = 365;
$dname = " \"CN=Marie-Josee Cros, OU=BIA, O=INRA, L=Castanet, S=France, C=FR\"";
#####################################################################

# Generate a key
$cmd = "keytool -genkey -alias $alias -dname $dname -keypass $keypass -storepass $storepass -keystore $keystore -validity $validity";
print "$cmd\n";
system $cmd;

# Sign file with the key
foreach my $jar (`ls $JAR_DIR/*.jar`) {
  chomp($jar);
  $signedjar = $jar; $signedjar =~ s/\.jar/\.signed.jar/;
  $cmd = "jarsigner -keystore $keystore -storepass $storepass -keypass $keypass -signedjar $signedjar $jar $alias";
  print "$cmd\n";
  system $cmd;
}
