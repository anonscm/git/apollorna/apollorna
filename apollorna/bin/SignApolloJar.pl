#!/usr/bin/perl -w
# bin/SignDataJar.pl signed the data.jar file necessary for ApolloRNA web start in RNAspace
# Signed jar file is put in the same directory of jar files: $APOLLO_ROOT/jars
# !!!!!! ONLY for Linux
# /bin/ApolloRNA_keystore must be yet defined


# PARAMETERS to set #################################################
$APOLLO_ROOT = $ENV{"APOLLO_ROOT"};
$JAR_DIR = "$APOLLO_ROOT/jars";

$alias = "ApolloRNA";
$keypass = "ApolloRNA_keypass";
$storepass = "ApolloRNA_storepass";
$keystore = $APOLLO_ROOT."/bin/ApolloRNA_keystore";
$validity = 365;
$dname = " \"CN=Marie-Josee Cros, OU=BIA, O=INRA, L=Castanet, S=France, C=FR\"";
#####################################################################


# Sign file with the key
$cmd = "jarsigner -keystore $keystore -storepass $storepass -keypass $keypass -signedjar $JAR_DIR/apollo.signed.jar $JAR_DIR/apollo.jar $alias";
print "$cmd\n";
system $cmd;
