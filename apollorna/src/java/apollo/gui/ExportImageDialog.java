package apollo.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.border.TitledBorder;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import apollo.config.Config;
import apollo.datamodel.SequenceI;
import apollo.gui.ApolloFrame;
import apollo.gui.genomemap.ApolloPanel;
import apollo.gui.genomemap.StrandedZoomableApolloPanel;
import apollo.util.GuiUtil;


/**
 *  ExportImageDialog is a dialog window. It uses to export a genomic subsequence 
 *  in image files, with a specific zoom factor and a specific overlapping between 
 *  two images. The user can enter parameters, select a specific output file, and 
 *  call the export in images. If parameters are corrects, images are created: 
 *  these images are chained views from the selected subsequence.
 */
public class ExportImageDialog extends JDialog
{
    // Graphic components
    JTextField startPositionTextField;
    JTextField stopPositionTextField;
    JTextField zoomFactorTextField;
    JTextField overlappingTextField;
    JFileChooser imageFileChooser;
    JButton chooseFileButton;
    JTextArea	fileTextArea;
    
    // Panels from initial Apollo frame
    ApolloFrame apolloFrame;
    StrandedZoomableApolloPanel strandedZoomableSpeciePanel;
    
    int apolloPanelWidth; 
    int apolloPanelHeight; 
    
    // reference sequence: sequence of the first strandedZoomablePanel
    SequenceI referenceSequence; 
	
    // parameters of this window that the user can changed
    double zoomFactor;
    int startPosition;
    int stopPosition;
    int overlapping;
    
    File exportFile;
    
    // history variables
    File historyFile;
    Properties historyProperties;
    static String HISTORY_KEY = "exportFileName";
    
    static int CREATED_FILE_NUMBER_MAX = 200;	// max number of files to create
    static int CONFIRM_FILE_NUMBER = 50;	// limited file number before system asks confirmation
    double maxZoomFactorValue;		// max value of the zoom factor
    
    /**
     * Builder
     * @param apolloFrame
     */
    public ExportImageDialog(ApolloFrame apolloFrame) {
	super(apolloFrame, "Export images", true);
	this.apolloFrame = apolloFrame;
	this.strandedZoomableSpeciePanel = ((StrandedZoomableApolloPanel)
				     this.apolloFrame.getSyntenyPanel().getPanels().elementAt(0));
	ApolloPanel apolloPanel = this.strandedZoomableSpeciePanel.getApolloPanel();
	this.apolloPanelWidth  = apolloPanel.getWidth();
	this.apolloPanelHeight = apolloPanel.getHeight();
	this.referenceSequence = this.strandedZoomableSpeciePanel.getAnnotations().getRefSequence();
	
	// init max zoom factor
	maxZoomFactorValue = referenceSequence.getLength()*1.0/3.0;
		
	// init history file
	String historyFileName = Config.getAdapterHistoryFile();
	historyFile = new File(historyFileName);
	historyProperties = new Properties();
	
	initGUI();
	this.setVisible(true);
    }
    
    /**
     * Initialize graphic interface
     */
    private void initGUI() {
	// creation of 3 main panels 
	JPanel imageContainPanel = createImageContainPanel();
	JPanel filePanel = createFilePanel();
	JPanel buttonPanel = createButtonPanel();
	
	// add panels to the main panel
	JPanel mainPanel = new JPanel();
	mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
	
	mainPanel.add(Box.createVerticalStrut(10));
	mainPanel.add(imageContainPanel);
	mainPanel.add(Box.createVerticalStrut(20));
	mainPanel.add(filePanel);
	mainPanel.add(Box.createVerticalStrut(20));
	mainPanel.add(buttonPanel);
	mainPanel.add(Box.createVerticalStrut(10));
	
	initParameters();
	this.setContentPane(mainPanel);
	this.pack();
    }
    
    /**
     * Create and return a JPanel which contains fields of parameters about 
     * generated images
     * @return new JPanel to insert parameters for the generation of images
     */
    private JPanel createImageContainPanel() {
	DecimalFormat decimalFormat = (DecimalFormat)DecimalFormat.getInstance();
	decimalFormat.setMaximumFractionDigits(2);
	JPanel imageContainPanel = new JPanel();
	
	JLabel positionLabel = new JLabel("Genomic positions");
	JLabel zoomLabel = new JLabel("Zoom factor");
	JLabel overlappingLabel = new JLabel("<html>Overlapping <br>(A nucleotid number)</html>");
	JLabel toLabel = new JLabel("to", JLabel.CENTER);
	JLabel commentZoomLabel	= new JLabel("From 1.0 to "+decimalFormat.format(maxZoomFactorValue));
	JLabel commentoverlappingLabel = new JLabel("<html>Included in 0 and 1/2 of" +
						    "<br>the current window size</br></html>");
	startPositionTextField = GuiUtil.makeNumericTextField();
	stopPositionTextField = GuiUtil.makeNumericTextField();
	zoomFactorTextField   = new JTextField();
	overlappingTextField  = GuiUtil.makeNumericTextField();
	
	// box containing genomic positions
	Box genomicPositionBox = new Box(BoxLayout.X_AXIS);
	genomicPositionBox.add(startPositionTextField);
	genomicPositionBox.add(Box.createHorizontalStrut(5));
	genomicPositionBox.add(toLabel);
	genomicPositionBox.add(Box.createHorizontalStrut(5));
	genomicPositionBox.add(stopPositionTextField);
	
	TitledBorder imageContainBorder = BorderFactory.createTitledBorder(
				                             "Specifications of image contains");
	imageContainPanel.setBorder(imageContainBorder);
	imageContainPanel.setLayout(new GridLayout(3,3,10,5));
	imageContainPanel.add(positionLabel);
	imageContainPanel.add(genomicPositionBox);
	imageContainPanel.add(new JLabel(" "));
	imageContainPanel.add(zoomLabel);
	imageContainPanel.add(zoomFactorTextField);
	imageContainPanel.add(commentZoomLabel);
	imageContainPanel.add(overlappingLabel);
	imageContainPanel.add(overlappingTextField);
	imageContainPanel.add(commentoverlappingLabel);
	
	return imageContainPanel;
    }
    
    /**
     * Create and return a JPanel which contains the name of the file to create 
     * and a button to change this name
     * @return new JPanel which contains the name of the selected file and 
     * a button to change the name of the file
     */
    private JPanel createFilePanel() {
	// creation of file panel 
	JPanel filePanel = new JPanel();
	JLabel textLabel = new JLabel("Path and name(s) prefix of created file(s): ");
	textLabel.setVerticalAlignment(SwingConstants.TOP);
	
	Dimension textAreaDimension = new Dimension(50,45);
	fileTextArea = new JTextArea();
	fileTextArea.setPreferredSize(textAreaDimension);
	fileTextArea.setMinimumSize(textAreaDimension);
	
	// get fileTextArea uneditable, automatic line return at the end of line
	fileTextArea.setLineWrap(true);
	fileTextArea.setEditable(false);
	fileTextArea.setBackground(this.getBackground());
	
	chooseFileButton = new JButton("Browse to change...");
	chooseFileButton.addActionListener(new ChooseFileListener());
	
	// title of the panel
	TitledBorder fileBorder = BorderFactory.createTitledBorder("Specifications of image files");
	filePanel.setBorder(fileBorder);
	
	// panel which displays the path and the name of file(s)
	JPanel fileWayPanel = new JPanel();
	fileWayPanel.setLayout(new GridLayout(1, 2));
	fileWayPanel.add(textLabel);
	fileWayPanel.add(fileTextArea);
	
	// panel which contains the browse button
	JPanel browsePanel = new JPanel();
	browsePanel.setLayout(new BoxLayout(browsePanel, BoxLayout.X_AXIS));
	browsePanel.add(chooseFileButton);
	browsePanel.add(Box.createHorizontalGlue());
	
	// add the components to the panel
	filePanel.setLayout(new BoxLayout(filePanel, BoxLayout.Y_AXIS));
	filePanel.add(Box.createVerticalStrut(10));
	filePanel.add(fileWayPanel);
	filePanel.add(Box.createVerticalStrut(10));
	filePanel.add(browsePanel);
	filePanel.add(Box.createVerticalStrut(10));
	
	return filePanel;
    }
    
    /**
     * Create and return a JPanel which contains buttons to compute 
     * the number of generated files, to export images and to cancel
     * @return new JPanel which contains action buttons
     */
    private JPanel createButtonPanel() {
	JPanel buttonPanel = new JPanel();
	
	JLabel numberFileLabel = new JLabel("The number of created files " +
					    "is limited to " + CREATED_FILE_NUMBER_MAX);
	JButton numberFileButton = new JButton("Compute number of files");
	numberFileButton.addActionListener(new ComputeNumberFileListener());
	
	// box for compute number file buttons 
	Box numberFileBox = new Box(BoxLayout.X_AXIS);
	numberFileBox.add(numberFileLabel);
	numberFileBox.add(Box.createHorizontalStrut(10));
	numberFileBox.add(numberFileButton);
	
	JButton cancelButton = new JButton("Cancel");
	JButton exportButton = new JButton("Export");
	cancelButton.addActionListener(new CancelListener());
	exportButton.addActionListener(new ExportListener());
	
	// box for cancel and export buttons 
	Box buttonBox = new Box(BoxLayout.X_AXIS);
	buttonBox.add(cancelButton);
	buttonBox.add(Box.createHorizontalStrut(10));
	buttonBox.add(exportButton);
	
	buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
	buttonPanel.add(numberFileBox);
	buttonPanel.add(Box.createVerticalStrut(10));
	buttonPanel.add(buttonBox);
	return buttonPanel;
    }
    
    
    /**
     * Initialize default parameters of the window: genomic positions, overlapping, 
     * zoom factor and the selected file
     */
    private void initParameters() {
	double apolloZoomValue = strandedZoomableSpeciePanel.getZoomFactor();
	// init genomic positions
	startPositionTextField.setText(new Integer(referenceSequence.getRange().getStart()).toString());
	stopPositionTextField.setText(new Integer(referenceSequence.getRange().getEnd()).toString());
	
	// init zoom factor
	zoomFactorTextField.setText(new Double(apolloZoomValue).toString());
	
	// init overlapping
	overlappingTextField.setText(new Integer(0).toString());
	
	// init selected file if a defaut file name exist
	try {
	    historyProperties.load(new FileInputStream(historyFile));
	    String defaultExportFile = historyProperties.getProperty(HISTORY_KEY);
	    if (defaultExportFile != null) {
		fileTextArea.setText(defaultExportFile);
		exportFile = new File(defaultExportFile);
	    } else 
		exportFile = null;
	} catch(IOException ie){
	    System.out.println(ie.getMessage());
	}
    }
	
	
    /**
     * Call after clic on the choose file button
     * open a jfileChooser to select the file(s) to create
     */
    public void chooseFile() {
	imageFileChooser = new JFileChooser();
	imageFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
	imageFileChooser.setAccessory(new JLabel("<html>If files with<br>same name exist" +
						 "<br>in the selected<br>directory they will<br>be destroyed.</html>"));
	
	// if exportFile has a value, selected this file by default
	if (exportFile != null) {
	    imageFileChooser.setSelectedFile(exportFile);
	}
	
	int returnVal = imageFileChooser.showSaveDialog(this);
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    exportFile = imageFileChooser.getSelectedFile();
	    fileTextArea.setText(exportFile.getPath());
	    this.pack();
	}
    }
    
    /**
     * Check if values of parameters are corrects 
     * 
     * @return true if the parameters are corrects
     */
    private boolean checkParameters() {
	boolean isCorrect;
	
	// check the parameters about the sequence to draw
	isCorrect = checkSequenceParameters();
	if (isCorrect == false) 
	    return false;
  
	// check the parameters about selected file and directory
	isCorrect = checkFileParameters();
	if (isCorrect == false)
	    return false;
	
	return true;		
    }
	
	
    /**
     * Check if values of sequence parameters (positions, overlapping, zoom factor) 
     * exists and are corrects
     * @return true if values of sequence parameters are corrects
     */
    private boolean checkSequenceParameters() {
	String message = "<html>";
	boolean hasIncorrectParameter = false;
	
	// check that all fields have a value
	if ((zoomFactorTextField.getText().compareTo("") 	== 0) || 
	    (startPositionTextField.getText().compareTo("")     == 0) ||
	    (stopPositionTextField.getText().compareTo("") 	== 0) ||
	    (overlappingTextField.getText().compareTo("") 	== 0) ) {
	    hasIncorrectParameter = true;
	    message = message.concat("All fields have to have a value.<br>");
	    
	} else {
	    // check the zoom factor is a double and is included between 1 and max value
	    try {
		zoomFactor = Double.parseDouble(zoomFactorTextField.getText());
		
		if ((zoomFactor < 1.0) || (zoomFactor > maxZoomFactorValue)) {
		    hasIncorrectParameter = true;
		    message = message.concat("Incorrect zoom factor.<br>");
		}
	    } catch(NumberFormatException numberFormatException) {
		hasIncorrectParameter = true;
		message = message.concat("Zoom factor isn't a double value.<br>");
	    }
			
	    // get all the user entered values
	    startPosition = Integer.parseInt(startPositionTextField.getText());
	    stopPosition = Integer.parseInt(stopPositionTextField.getText());
	    overlapping	= Integer.parseInt(overlappingTextField.getText());
	    
	    int genomeStartPosition = referenceSequence.getRange().getStart();
	    int genomeStopPosition = referenceSequence.getRange().getEnd();
	    
	    // check start and stop positions are positives and inferiors to length of the sequence
	    if ( (startPosition < genomeStartPosition) || (startPosition > genomeStopPosition) ||
		 (stopPosition  < genomeStartPosition) || (stopPosition  > genomeStopPosition) ) {
		hasIncorrectParameter = true;
		message = message.concat("Positions aren't included in genomic sequence.<br>");
	    } else if (stopPosition - startPosition < 0) { 
		// check start position is inferior to stop position
		hasIncorrectParameter = true;
		message = message.concat("Start position is superior to stop position.<br>");
	    }
	    
	    // check overlapping included between 0 and 1/10 sequence length
	    int nuclNumberPerImage = (int)StrictMath.ceil(referenceSequence.getLength()*1.0/zoomFactor);
	    int maxOverlapping = nuclNumberPerImage/2;
	    if (overlapping > maxOverlapping) {
		hasIncorrectParameter = true;
		message = message.concat("Overlapping is too large.<br>");
	    }
	}
	
	message = message.concat("</html>");
	// if a parameter is incorrect, display a dialog which explains the error 
	if (hasIncorrectParameter == true) {
	    JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
	    return false;
	} else 
	    return true;
    }
	
    
    /**
     * Check if file parameters (writable directory, number of generated files) are corrects
     * @return true if values of file parameters are corrects
     */
    private boolean checkFileParameters() {
	String message = "<html>";
	boolean hasIncorrectParameter = false;
	String exportFileName = "";
	File exportDirectory = null;
	
	if (exportFile != null) {
	    exportFileName = exportFile.getName();
	    exportDirectory = exportFile.getParentFile();
	}
	
	// check name of the file isn't empty
	if (exportFileName.compareTo("") == 0) {
	    hasIncorrectParameter = true;
	    message = message.concat("You have to enter a name of file.<br>");
	} else if (exportFileName.matches(
	   "([a-z]|[A-Z]|[0-9])+|([a-z]|[A-Z]|[0-9])+([a-z]|[A-Z]|[0-9]|-|_)+([a-z]|[A-Z]|[0-9])+") == false) {
	    // check that the name of files is composed of letters, numbers and character '-' 
	    // (not at the begin or at the end of the name)
	    hasIncorrectParameter = true;
	    message = message.concat("Incorrect name of files. Letters, numbers and " +
	      "the characters '-' and '_' (not at the begin and at the end) are accepted.<br>");
	} else if (exportDirectory.canWrite() == false) {
	    // check it's possible to write in the directory
	    hasIncorrectParameter = true;
	    message = message.concat("Impossible to write in the selected directory.<br>");
	}
	
	// check the number of created files is inferior to CREATED_FILE_NUMBER_MAX
	int GeneratedFileNumber = this.computeGeneratedFileNumber(); 
	if (GeneratedFileNumber >= CREATED_FILE_NUMBER_MAX) {
	    hasIncorrectParameter = true;
	    message = message.concat("Impossible to create more than 1 000 files.<br>");
	} else if (GeneratedFileNumber > CONFIRM_FILE_NUMBER) {
	    int response = JOptionPane.showConfirmDialog(this,
			      "Do you want to create "+GeneratedFileNumber+" files ?",
			      "Confirmation",JOptionPane.YES_NO_OPTION);
	    if (response == JOptionPane.NO_OPTION)
		return false;
	    else 
		return true;
	} 
		
	message = message.concat("</html>");
	
	// if a parameter is incorrect, display a dialog which explains the error 
	if (hasIncorrectParameter == true) {
	    JOptionPane.showMessageDialog(this,message,"Error",JOptionPane.ERROR_MESSAGE);
	    return false;
	} else 
	    return true;
    }

    /**
     * Export in image files the selected subsequence of the synteny panel with a 
     * specific overlapping and a specific zoom factor
     * Process is to move the main synteny panel of apollo, to print it in an image
     * Repeating while the subsequence is not complety printed 
     */
    public void export() {		
	BufferedImage buffer;
	String exportFileNamePrefix = exportFile.getPath() + "_";
	int fileNumber = 1;
	
	int sequenceLength = referenceSequence.getLength();
	int imageLength = (int)StrictMath.ceil(sequenceLength*1.0/zoomFactor);
	int[] positions = new int[2];
	
	this.setModal(false);
	
	// save original positions
	int savedCenter = strandedZoomableSpeciePanel.getCentreBase();
	double savedZoom = strandedZoomableSpeciePanel.getZoomFactor();
	
	// initialization of positions of first window
	if ( (imageLength % 2) == 0) {
	    positions[0] = startPosition - 1;
	    positions[1] = startPosition + imageLength - 2;
	} else {
	    positions[0] = startPosition;
	    positions[1] = startPosition + imageLength - 1;
	}
	
	// create image file for the first first window
	buffer = new BufferedImage(apolloPanelWidth, apolloPanelHeight, 
				   BufferedImage.TYPE_INT_RGB);
	this.exportInBuffer(buffer, positions, imageLength);
	
	// save the graphics buffer in a file
	this.writeBuffer(buffer, exportFileNamePrefix+fileNumber);
	
	// for each window
	while (positions[1] < (stopPosition - 1)) {
	    positions[0] = positions[1] + 1 - overlapping;
	    positions[1] = positions[0] + imageLength - 1;
	    buffer = new BufferedImage(apolloPanelWidth, apolloPanelHeight, 
				       BufferedImage.TYPE_INT_RGB); 
	    
	    // draw in the buffer the subsequence with specific positions
	    this.exportInBuffer(buffer, positions,imageLength);
	    // save the graphics in a file
	    fileNumber++;
	    this.writeBuffer(buffer, exportFileNamePrefix+fileNumber);
	}
	
	// restore original positions
	strandedZoomableSpeciePanel.setCentreBase(savedCenter);
	strandedZoomableSpeciePanel.setZoomFactor(savedZoom);
	
	this.setModal(true);
	
	historicSave();	// save the selected file
	windowExit();	// exit
    }
    
    
    /**
     * Print the subsequence of genomic sequence, 
     * defined in the table "positions", in the bufferedImage
     * @param buffer       buffer in which prints the subsequence
     * @param positions    table of two integer: respectively, start and stop 
     *                     positions of the subsequence
     * @param imageLength  length of the subsequence
     */
    private void exportInBuffer(BufferedImage buffer, int[] positions, int imageLength) {
	ApolloPanel apolloPanel   = this.strandedZoomableSpeciePanel.getApolloPanel();
	Graphics2D  imageGraphics = buffer.createGraphics(); 
	
	imageGraphics.setClip(new Rectangle(0, 0, apolloPanelWidth,apolloPanelHeight));
	
	this.apolloFrame.getSyntenyPanel().invalidate();
	// Reset the panel position
	double curWidth = (double)apolloPanel.getVisibleBasepairWidth();
	this.strandedZoomableSpeciePanel.setZoomFactor(1.0);
	this.strandedZoomableSpeciePanel.setCentreBase(
		 strandedZoomableSpeciePanel.getCurationSet().getLow() + (int)curWidth / 2);
	
	// zoom on the specified positions
	this.strandedZoomableSpeciePanel.zoomToWidth(imageLength, positions);
	// paint the panel in the graphics
	apolloPanel.paintComponent(imageGraphics);
	
	imageGraphics.dispose();
    }
	
	
    /**
     * Create a png file which contains the drawing containing in the bufferedImage.
     * @param buffer   Buffer which contains the drawing of a subsequence of 
     *                  apollo genomic sequence
     * @param fileName Way of the resulting file 
     */
    private void writeBuffer(BufferedImage buffer, String fileName) {
	try {
	    // draw the graphics in a png file
	    ImageIO.write(buffer, "png", new File(fileName+".png"));
	}
	catch (IOException ioexception) {
	    System.out.println("Image write: " + ioexception.getMessage());
	}
    }
    
    /**
     * Compute the number of files which will be created with the current parameters
     * @return The number of files which will be created 
     */
    public int computeGeneratedFileNumber() {
	boolean areCorrectParameters = checkSequenceParameters();
	if (areCorrectParameters == true) {
	    int generatedFileNumber = 1;
	    int windowLength = (int)StrictMath.ceil(
		       referenceSequence.getLength() * 1.0 / zoomFactor);
	    int endPosition = startPosition + windowLength - 1;
	    
	    while (endPosition < stopPosition) {
		generatedFileNumber++;
		endPosition = endPosition + windowLength - overlapping;
	    }
	    return generatedFileNumber;
	} 
	else 
	    return -1;
    }
	
	
    /**
     * Get the path of the selected file, and save it in a historic file
     */
    public void historicSave() {
	// save the selected export file in the history file
	try {
	    historyProperties.setProperty(HISTORY_KEY,exportFile.getPath());
			historyProperties.store(new FileOutputStream(historyFile), 
						"DataAdapterChooser properties file");
			
	} catch(IOException ie) {
	    System.out.println(ie.getMessage());
	}
    }
	
    /**
     * exit of the current wondow
     */
    public void windowExit() {
	hide();
	dispose();
    }
    
    
    /**
     * internal class: listener about the choose file button
     *
     */
    public class ChooseFileListener 
	implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    // open a jFileChooser to select a file 
	    chooseFile();
	}
    }
    
    /**
     * internal class: listener about the export button
     *
     */
    public class ExportListener 
	implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    // if all parameters are ok, call export in images
	    if (checkParameters() == true) 
		export();
	}
    }
    
    /**
     * internal class: listener about the compute number of file button
     *
     */
    public class ComputeNumberFileListener 
	implements ActionListener {
	public void actionPerformed(ActionEvent e) {
	    int fileNumber = computeGeneratedFileNumber();
	    if (fileNumber != -1) {
		String message = new Integer(fileNumber).toString() + " file";
		if (fileNumber > 1) 
		    message = message.concat("s");
		message = message.concat(" will be created.");
		JOptionPane.showMessageDialog(null,message);
	    }
	}
    }
    
    /**
     * internal class: listener about the cancel button
     *
     */
    public class CancelListener 
	implements ActionListener  {
	public void actionPerformed(ActionEvent e) {
	    windowExit();
	}
    }
    
}
