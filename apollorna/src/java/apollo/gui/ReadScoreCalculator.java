package apollo.gui;

import apollo.datamodel.*;

/**
 * scores are given to the calculator
 */
public class ReadScoreCalculator extends ComputeAllScoreCalculator {

    /**
     * @param curation	curation on which computation will be done
     * @param winSize	initial size of the reading frame
     */
    public ReadScoreCalculator(CurationSet curation, int winSize, double [] s) {
	super(winSize, curation);
        setWinSize(winSize);

	char [] seqChars = curation.getRefSequence().getResidues( curation.getLow(), curation.getHigh() ).toCharArray();
	int l = seqChars.length;
	double v;

	scores=s;
	score_min = Double.POSITIVE_INFINITY;
	score_max = Double.NEGATIVE_INFINITY;
	score_mean = 0;
	for (int i=0; i<l; i++) {
	    v= scores[i];
	    if (v<score_min)  score_min=v;
	    if (v>score_max)  score_max=v;
	    score_mean+=v;
	}
	score_mean= score_mean/(double)l;
    }
    
    public void updateScores() {}

}
