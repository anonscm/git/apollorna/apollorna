package apollo.gui;

import java.text.NumberFormat;

import apollo.config.Config;
import apollo.util.FeatureList;
import apollo.datamodel.SeqFeatureI;
import apollo.datamodel.SequenceI;
import apollo.datamodel.StrandedFeatureSetI;
import apollo.gui.synteny.CurationManager;

import org.apache.log4j.*;


public class InformationSequenceController {

    protected final static Logger logger = LogManager.getLogger(InformationSequenceController.class);
	
    protected InformationSequenceViewer informationSequenceViewer;
    private NumberFormat myformat = NumberFormat.getInstance();	
    
    private static String entireGCValueString;
    private static String entireSequenceLenghtString;
    private static String entireNonATCGNucleotideValueString;
    private static String entireIntergenicValurString;
    private static boolean isFirstCompute = true;
    
    private double[] intergenicComputingResult;
    private boolean isGenomicPlusMinus;
    private int genomicPlusMinus;
    
    public InformationSequenceController(InformationSequenceViewer iSV){
	informationSequenceViewer = iSV;
	if(isFirstCompute){
	    myformat.setMinimumFractionDigits(1);
	    myformat.setMaximumFractionDigits(1);	
	}
    }
    
    
    /**
     * Compute all information in the selected sequence information viewer<br>
     * This is the only method which can be acceded from outside
     * @param iSV the information sequence viewer
     * @param features a featureList containing all current selection
     * @param isGenomicPlusMinus a boolean which indicate if the computation 
     * will be done with a genomic plus/minus
     * @param genomicPlusMinus the value of genomic plus/minus if needed
     * 
     */
    public void computeData(InformationSequenceViewer iSV, FeatureList features, boolean isGenomicPlusMinus, int genomicPlusMinus){
	informationSequenceViewer = iSV;
	this.isGenomicPlusMinus = isGenomicPlusMinus;
	this.genomicPlusMinus = genomicPlusMinus;
	
	computeIntergenic(features);
	computeGC(features);
    }
    
    
    
    /**
     * Compute the GC% and the number of non ATCG nucleotide of each feature in the feature list 
     * and display them in the information sequence viewer<br> 
     * This method also compute the same thing for the entire sequence and for the entire sequence too
     * it compute the intergenic value<br>
     * Those computation for the entire sequence occured once on the creation of the object
     * after that it is only displayed
     * @param features the feature list
     */
    private void computeGC(FeatureList features){
	char[] seqChars;
	double[] computingResult;
	double gcValue;
	double numberNonATCG;
	double intergenicValue;
	String gcValueString;
	String numberNonATCGString;
	String intergenicValueString;
	String sequenceLength;
	String typeSeq;
	String header;
	
	if(isFirstCompute){	
	    seqChars = CurationManager.getActiveCurationState().getCurationSet().getResidues().toCharArray();
	    computingResult = computeScore(seqChars);
	    gcValue = computingResult[0];
	    numberNonATCG = computingResult[1];
	    intergenicValue = computeIntergenicEntireSequence(seqChars);
	    
	    gcValueString = myformat.format(gcValue);
	    numberNonATCGString = Integer.toString((int)numberNonATCG);
	    sequenceLength = Integer.toString(seqChars.length);
	    
	    entireGCValueString = gcValueString;
	    entireSequenceLenghtString = sequenceLength;
	    entireNonATCGNucleotideValueString = numberNonATCGString;
	    entireIntergenicValurString = myformat.format(intergenicValue);
	    
	    informationSequenceViewer.getSequenceInformationDisplay().
		gcEntireSequenceValueLabel.setText(entireGCValueString);
	    informationSequenceViewer.getSequenceInformationDisplay().
		entireSequenceLenghtValueLabel.setText(entireSequenceLenghtString);
	    informationSequenceViewer.getSequenceInformationDisplay().
		nonATCGNucleotideEntireSequenceValueLabel.setText(entireNonATCGNucleotideValueString);
	    informationSequenceViewer.getSequenceInformationDisplay().
		intergenicEntireSequenceValueLabel.setText(entireIntergenicValurString);
	    
	    isFirstCompute = false;
	}
	
	for(int i=0; i<features.size(); i++){
	    SeqFeatureI feat = features.getFeature(i);
	    SequenceI refSeq = feat.getRefSequence();
	    
	    if (feat == null || refSeq == null)
		continue;
	    if(isGenomicPlusMinus){	
		typeSeq = "genomic +/-" + genomicPlusMinus+ " bases";
		int minus = genomicPlusMinus * feat.getStrand(); // hard wired for now
		int plus  = genomicPlusMinus * feat.getStrand();
		seqChars   = refSeq.getResidues(feat.getStart() - minus, feat.getEnd() + plus).toCharArray();
	    } else {
		typeSeq = "genomic";
		if(feat.getFeatureType().equals("Sequence selection"))
		    seqChars = refSeq.getResidues().toCharArray();
		else
		    seqChars = feat.getResidues().toCharArray();
	    }
	    
	    computingResult = computeScore(seqChars);
	    gcValue = computingResult[0];
	    numberNonATCG = computingResult[1];
	    header = (">" + Config.getDisplayPrefs().getHeader(feat) + " ("
		      + typeSeq + " sequence): " + seqChars.length + " residues");
	    
	    if(seqChars.length == 0){
		gcValueString = "0";
		numberNonATCGString = "0";
	    } else {
		gcValueString = myformat.format(gcValue);
		numberNonATCGString = Integer.toString((int)numberNonATCG);
	    }	
	    
	    intergenicValueString = myformat.format(intergenicComputingResult[i]);
	    informationSequenceViewer.getSequenceInformationDisplay().			
		addNewSection(header,gcValueString,numberNonATCGString,intergenicValueString);			
	}
	informationSequenceViewer.getSequenceInformationDisplay().
	    gcEntireSequenceValueLabel.setText(entireGCValueString);
	informationSequenceViewer.getSequenceInformationDisplay().
	    entireSequenceLenghtValueLabel.setText(entireSequenceLenghtString);
	informationSequenceViewer.getSequenceInformationDisplay().
	    nonATCGNucleotideEntireSequenceValueLabel.setText(entireNonATCGNucleotideValueString);
	informationSequenceViewer.getSequenceInformationDisplay().
	    intergenicEntireSequenceValueLabel.setText(entireIntergenicValurString);
    }
    
    
    
    /**
     * Compute the intergenic value of each feature in the feature list 
     * and display them in the information sequence viewer 
     * @param features the feature list
     */
    private void computeIntergenic(FeatureList features){
	int featCount = features.size();
	SeqFeatureI feat;
	intergenicComputingResult = null;
	
	if(CurationManager.getActiveCurationState().haveAnnots()){
	    for(int i=0; i<featCount; i++){
		feat = features.getFeature(i);
		if(feat.getFeatureType().equals("Sequence selection")){
		    int[] bounds = getBoundsFromSelectionHeader(Config.getDisplayPrefs().getHeader(feat));
		    if(bounds != null){
			feat.setLow(bounds[0]);
			feat.setHigh(bounds[1]);
		    }
		}
	    }			
	    features.sortByStart();//sort by low
	    intergenicComputingResult = computeIntergenicSection(features);
	} else {
	    intergenicComputingResult = new double[features.size()];
	    for(int i=0; i<features.size(); i++)
		intergenicComputingResult[i] = 100.;
	}
    }
    
    
    
    /**
     * This method extract from a sequence selection header the low and high bound
     * @param header the header
     * @return a int array<br>  
     * result[0] = low <br>
     * result[1] = high 
     */
    private int[] getBoundsFromSelectionHeader(String header){
	int[] result = new int[2];
	String remainHead;
	String boundStart = null;
	String boundEnd = null;
	
	//test if the feat is a sequence selection
	if((header.contains("from"))&&(header.contains("to"))){
	    remainHead = header;			
	    boundStart = remainHead.substring(remainHead.indexOf("from")+4,remainHead.indexOf("to"));
	    result[0] = Integer.parseInt(boundStart.trim());
	    
	    remainHead = header;			
	    boundEnd = remainHead.substring(remainHead.indexOf("to")+2);
	    result[1] = Integer.parseInt(boundEnd.trim());
	    
	    return result;
	}		
	return null;
    }

    /**
     * Static method which compute the GC score of a nucleotide sequence and count 
     * the non(A,T,C,G) nucleotide
     * @param seqChars the sequence
     * @return a double array<br> 
     * result[0] = scoreGC <br>
     * result[1] = numberNonATCG
     */
    public static double [] computeScore(char [] seqChars){
	double numberGC = 0.;
	double numberNonATCG = 0.;
	double[] result = new double[2];
	double scoreGC = 0.;
	
	for (int i=0; i<seqChars.length; i++) {
	    char nucleotide = seqChars[i];
	    
	    if ((nucleotide == 'C')||(nucleotide == 'G'))
		numberGC++;
	    else if((nucleotide != 'A')&&(nucleotide != 'T'))
		numberNonATCG++;
	}
	scoreGC = numberGC/(double)(seqChars.length) * 100.0;
	result[0] = scoreGC;
	result[1] = numberNonATCG;
	return result;
    }
    
    
    
    /**
     * Compute the intergenic value of each feature in the feature list
     * @param features the feature list
     * @return a double array of the size of feature number
     */
    public static double[] computeIntergenicSection(FeatureList features) {
	StrandedFeatureSetI sFSI = CurationManager.getActiveCurationState().getCurationSet().getAnnots();		
	int annotsCount = sFSI.size();
	double[] computingResult = new double[features.size()];
	int[] tempResult = new int[features.size()];
	int featSize;
	
	for(int i=0; i<features.size(); i++)
	    tempResult[i] = 0;
	
	for(int i=0; i<annotsCount; i++){
	    SeqFeatureI annot = sFSI.getFeatureAt(i);
	    if(annot.getFeatureType().equals("gene"))
		for(int j=0; j<features.size(); j++){
		    SeqFeatureI feat = features.getFeature(j);
		    if(feat.overlaps(annot.getRangeClone())){					
			tempResult[j] += computeNumberOfOverlapedNucleotide(feat, annot);
		    }				
		}
	}
	
	for(int i=0; i<features.size(); i++){
	    SeqFeatureI feat = features.getFeature(i);
	    if(feat.getFeatureType().equals("Sequence selection"))
		featSize = feat.getRefSequence().getResidues().toCharArray().length;
	    else
		featSize = feat.getResidues().toCharArray().length;			
	    
	    if(featSize != 0)
		computingResult[i] = (double)((featSize-tempResult[i])/(double)featSize)*100.;
	    else
		computingResult[i] = 0;
	}		
	return computingResult;
	
    }
    
    
    
    /**
     * Compute the intergenic value of the entire necleotide sequence
     * @param seqChars the entire nucleotide sequence
     * @return the intergenic value
     */
    public static double computeIntergenicEntireSequence(char[] seqChars) {
	StrandedFeatureSetI sFSI = CurationManager.getActiveCurationState().getCurationSet().getAnnots();		
	int annotsCount = sFSI.size();
	double computingResult;
	int tempResult = 0;
	
	for(int i=0; i<annotsCount; i++){
	    SeqFeatureI annot = sFSI.getFeatureAt(i);
	    if(annot.getFeatureType().equals("gene")){
		tempResult += annot.getResidues().toCharArray().length;
	    }
	}				
	
	int sequenceSize = seqChars.length;
	if(sequenceSize != 0)
	    computingResult = (double)((sequenceSize-tempResult)/(double)sequenceSize)*100.;
	else
	    computingResult = 0;
	
	return computingResult;		
    }
    
    
    
    /**
     * Compute how many nucleotide in the feature are nucleotide overlap those from the annotation
     * @param feat the feature to analyse
     * @param annot the annotation to compare with
     * @return the number of feature nucleotide which overlap annotation nucleotide
     */
    private static int computeNumberOfOverlapedNucleotide(SeqFeatureI feat, SeqFeatureI annot){
	int boundLow = 0;
	int boundHigh = 0;
	
	if(feat.getLow() <= annot.getLow())
	    boundLow = annot.getLow();
	else
	    boundLow = feat.getLow();
	
	if(feat.getHigh() <= annot.getHigh())
	    boundHigh = feat.getHigh();
	else
	    boundHigh = annot.getHigh();
	
	return Math.abs(boundHigh - boundLow + 1);
    }	

}


