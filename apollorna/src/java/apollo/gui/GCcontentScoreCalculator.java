package apollo.gui;

import apollo.datamodel.*;

/**
 * GCcontentScoreCalculator for quantitative variables graph visualization
 */
public class GCcontentScoreCalculator extends ComputeAllScoreCalculator {

    boolean is_2SDcutoff;


    public GCcontentScoreCalculator(CurationSet curation, int winSize, boolean is_2SDcutoff_arg) {
	super(winSize, curation);
	is_2SDcutoff = is_2SDcutoff_arg;
	updateScores();
    }

    public void updateScores() {
      int winSize = getWinSize();
      if (computedWinSize != winSize) {
	computedWinSize = winSize;

	char [] seqChars = curation.getRefSequence().getResidues( curation.getLow(), curation.getHigh() ).toCharArray();
	char c_remove, c_add;
	int l;
	int nG,nC,i,j;
	double s = 0;

	l = seqChars.length;
	scores = new double[l];
	
	i = halfWinSize;
	nG =0; nC = 0;
	for (j= i - halfWinSize; j<i + halfWinSize +1; j++) {
	    c_add = seqChars[j];
	    if (c_add=='G')  nG+=1;
	    else if (c_add=='C')  nC+=1;
	}
	s = 100. * (nG + nC)/(double)winSize;
	scores[i] = s;
	score_min = s;
	score_max = s;
	score_mean = s;
	
	for (i=halfWinSize+1; i<l-halfWinSize; i++) {
	    c_remove = seqChars[i - halfWinSize -1];
	    c_add = seqChars[i + halfWinSize];
	    if (c_remove=='G')  nG=nG-1;
	    else if (c_remove=='C')  nC=nC-1;
	    if (c_add=='G')  nG+=1;
	    else if (c_add=='C')  nC+=1;
	    s = 100. * (nG + nC)/(double)winSize;
	    scores[i] = s;
	    score_min = Math.min(s, score_min);
	    score_max = Math.max(s, score_max);
	    score_mean += s;
	}
	score_mean = score_mean / (double)(l-winSize+1);
	
	for (i=0; i<halfWinSize; i++) scores[i] = score_mean;
	for (i=l-halfWinSize; i<l; i++) scores[i] = score_mean;

	if (is_2SDcutoff) compute2SDcutoff(l);

	// For test
	//String file_name = "G-C";
	//if (is_2SDcutoff) file_name += "_2SD";
	//writeFile(file_name, l);
      }
    }   

}
