package apollo.gui;

import apollo.datamodel.*;

/**
 * Dinucleotide Bias Score Calculator
 */
public class DinucleotideBiasScoreCalculator extends ComputeAllScoreCalculator {

    char X;
    char Y;

    public DinucleotideBiasScoreCalculator(CurationSet curation, int winSize, char X_arg, char Y_arg) {
	super(winSize, curation);
	X=X_arg;
	Y=Y_arg;
	updateScores();
    }

    public void updateScores() {    
      int winSize = getWinSize();
      if (computedWinSize != winSize) {
	computedWinSize = winSize;

	char [] seqChars = curation.getRefSequence().getResidues( curation.getLow(), curation.getHigh() ).toCharArray();
	char c_remove, c_remove_next, c_add, c_before;
	int l;
	int nX,nY,nXY,i,j;
	double s = 0;
	
	l = seqChars.length;
	scores = new double[l];
	
	i = halfWinSize;
	nX =0; nY = 0; nXY=0;
	c_before='B'; // not an IUPAC nucleotide for index -1 on the sequence
	for (j= i - halfWinSize; j<i + halfWinSize +1; j++) {
	    c_add = seqChars[j];
	    if (c_add==X)  nX+=1;
	    if (c_add==Y) {
		nY+=1;
		if (c_before==X) nXY+=1;
	    }
            c_before = c_add;
	}
	if (nX!=0 && nY!=0) s = (nXY * winSize)/(double)(nX * nY);
	else s = 0;
	scores[i] = s;
	score_min = s;
	score_max = s;
	score_mean = s;
	
	c_remove =  seqChars[0];
	for (i=halfWinSize+1; i<l-halfWinSize; i++) {
	    c_remove_next = seqChars[i - halfWinSize];
	    c_add = seqChars[i + halfWinSize];
	    if (c_remove==X)  {
		nX-=1;
		if (c_remove_next == Y) nXY-=1;
	    } 
	    if (c_remove==Y)  nY-=1;
	    if (c_add==X)  nX+=1;
	    if (c_add==Y) {
		nY+=1;
		if (c_before==X) nXY+=1;
	    }
	    c_remove = c_remove_next;
            c_before = c_add;
	    if (nX!=0 && nY!=0) s = (nXY * winSize)/(double)(nX * nY);
	    else s = 0;
	    scores[i] = s;
	    score_min = Math.min(s, score_min);
	    score_max = Math.max(s, score_max);
	    score_mean += s;
	}
	score_mean = score_mean / (double)(l-winSize+1);
	
	for (i=0; i<halfWinSize; i++) scores[i] = score_mean;
	for (i=l-halfWinSize; i<l; i++) scores[i] = score_mean;

	// For test
	//writeFile("Bias"+X+Y, l);
      }
    }
    
}
