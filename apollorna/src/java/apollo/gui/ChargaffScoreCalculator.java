package apollo.gui;

import apollo.datamodel.*;

/**
 * Chargaff variable score calculator
 */
public class ChargaffScoreCalculator extends ComputeAllScoreCalculator {

    char X;
    char Y;

    public ChargaffScoreCalculator(CurationSet curation, int winSize, char X_arg, char Y_arg) {
	super(winSize, curation);
	X=X_arg;
	Y=Y_arg;
	updateScores();
    }

    public void updateScores() {    
      int winSize = getWinSize();
      if (computedWinSize != winSize) {
	computedWinSize = winSize;

	char [] seqChars = curation.getRefSequence().getResidues( curation.getLow(), curation.getHigh() ).toCharArray();
	char c_remove, c_add;
	int l;
	int nX,nY,i,j;
	double s = 0;
	
	l = seqChars.length;
	scores = new double[l];
	
	i = halfWinSize;
	nX =0; nY = 0;
	for (j= i - halfWinSize; j<i + halfWinSize +1; j++) {
	    c_add = seqChars[j];
	    if (c_add==X)  nX+=1;
	    else if (c_add==Y)  nY+=1;
	}
	if ((nX + nY) != 0) s = (nX - nY)/(double)(nX + nY) * 100;
	else s = 0;
	scores[i] = s;
	score_min = s;
	score_max = s;
	score_mean = s;
	
	for (i=halfWinSize+1; i<l-halfWinSize; i++) {
	    c_remove = seqChars[i - halfWinSize-1];
	    c_add = seqChars[i + halfWinSize];
	    if (c_remove==X)  nX-=1;
	    else if (c_remove==Y)  nY-=1;
	    if (c_add==X)  nX+=1;
	    else if (c_add==Y)  nY+=1;
	    if ((nX + nY) != 0) s = (nX - nY)/(double)(nX + nY) * 100.;
	    else s = 0;
	    scores[i] = s;
	    score_min = Math.min(s, score_min);
	    score_max = Math.max(s, score_max);
	    score_mean += s;
	}
	score_mean = score_mean / (double)(l-winSize+1);
	
	for (i=0; i<halfWinSize; i++) scores[i] = score_mean;
	for (i=l-halfWinSize; i<l; i++) scores[i] = score_mean;

	// For test
	//writeFile("Chargaff"+X+Y, l);
      }
    }
    
}
