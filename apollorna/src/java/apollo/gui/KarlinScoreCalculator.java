package apollo.gui;

import apollo.datamodel.*;

/**
 * Karlin variable Score Calculator
 */
public class KarlinScoreCalculator extends ComputeAllScoreCalculator {

    boolean is_2SDcutoff;
    boolean is_GCCGonly;

    public KarlinScoreCalculator(CurationSet curation, int winSize, boolean is_2SDcutoff_arg, boolean is_GCCGonly_arg) {
	super(winSize, curation);
	is_2SDcutoff = is_2SDcutoff_arg;
	is_GCCGonly = is_GCCGonly_arg;
	updateScores();
    }

    public void updateScores() {    
      int winSize = getWinSize();
      if (computedWinSize != winSize) {
	computedWinSize = winSize;

	char [] seqChars = curation.getRefSequence().getResidues( curation.getLow(), curation.getHigh() ).toCharArray();
	char c_remove, c_add, c_before;
	int l,ll,i,j;
	int nA,nT,nG,nC,nAA,nAT,nAG,nAC,nTA,nTT,nTG,nTC,nGA,nGT,nGG,nGC,nCA,nCT,nCG,nCC;
	double dbAA,dbAT,dbAG,dbAC,dbTA,dbTT,dbTG,dbTC,dbGA,dbGT,dbGG,dbGC,dbCA,dbCT,dbCG,dbCC,s;
	DinucleotideBiasScoreCalculator db_winAA,db_winAT,db_winAG,db_winAC,db_winTA,db_winTT,db_winTG,
	    db_winTC,db_winGA,db_winGT,db_winGG,db_winGC,db_winCA,db_winCT,db_winCG,db_winCC;
	db_winAA=null;db_winAT=null;db_winAG=null;db_winAC=null;db_winTA=null;db_winTT=null;db_winTG=null;db_winTC=null;
	db_winGA=null;db_winGT=null;db_winGG=null;db_winGC=null;db_winCA=null;db_winCT=null;db_winCG=null;db_winCC=null;
	l = seqChars.length;
	scores = new double[l];
	
	// Compute dinucleotide bias on all the sequence
	nA=0;nT=0;nG=0;nC=0;
	nAA=0;nAT=0;nAG=0;nAC=0;
	nTA=0;nTT=0;nTG=0;nTC=0;
	nGA=0;nGT=0;nGG=0;nGC=0;
	nCA=0;nCT=0;nCG=0;nCC=0;
	c_before='B'; // not an IUPAC nucleotide for index -1 on the sequence
	for (i=halfWinSize; i<l-halfWinSize; i++) {
	    c_add = seqChars[i];
	    if (c_add=='A') {
		nA+=1;
		if (c_before=='A') nAA+=1;
		else if (c_before=='T') nTA+=1;
		else if (c_before=='G') nGA+=1;
		else if (c_before=='C') nCA+=1;
	    } else 
		if (c_add=='T') {
		    nT+=1;
		    if (c_before=='A') nAT+=1;
		    else if (c_before=='T') nTT+=1;
		    else if (c_before=='G') nGT+=1;
		    else if (c_before=='C') nCT+=1;
		} else 
		    if (c_add=='G') {
		    nG+=1;
		    if (c_before=='A') nAG+=1;
		    else if (c_before=='T') nTG+=1;
		    else if (c_before=='G') nGG+=1;
		    else if (c_before=='C') nCG+=1;
		} else 
			if (c_add=='C') {
			    nC+=1;
			    if (c_before=='A') nAC+=1;
			    else if (c_before=='T') nTC+=1;
			    else if (c_before=='G') nGC+=1;
			    else if (c_before=='C') nCC+=1;
			}
            c_before = c_add;
	}
	ll = l -winSize + 1;
	if (nA!=0) dbAA=(nAA*ll)/(double)(nA*nA);
	else dbAA=0;
	if (nA!=0 && nT!=0) {dbAT=(nAT*ll)/(double)(nA*nT);dbTA=(nTA*ll)/(double)(nT*nA);}
	else {dbAT=0;dbTA=0;}
	if (nA!=0 && nG!=0) {dbAG=(nAG*ll)/(double)(nA*nG);dbGA=(nGA*ll)/(double)(nG*nA);}
	else {dbAG=0;dbGA=0;}
	if (nA!=0 && nC!=0) {dbAC=(nAC*ll)/(double)(nA*nC);dbCA=(nCA*ll)/(double)(nC*nA);}
	else {dbAC=0;dbCA=0;}
	if (nT!=0) dbTT=(nTT*ll)/(double)(nT*nT);
	else dbTT=0;
	if (nT!=0 && nG!=0) {dbTG=(nTG*ll)/(double)(nT*nG);dbGT=(nGT*ll)/(double)(nG*nT);}
	else {dbTG=0;dbGT=0;}
	if (nT!=0 && nC!=0) {dbTC=(nTC*ll)/(double)(nT*nC);dbCT=(nCT*ll)/(double)(nC*nT);}
	else {dbTC=0;dbCT=0;}
	if (nG!=0) dbGG=(nGG*ll)/(double)(nG*nG);
	else dbGG=0;
	if (nG!=0 && nC!=0) {dbGC=(nGC*ll)/(double)(nG*nC);dbCG=(nCG*ll)/(double)(nC*nG);}
	else {dbGC=0;dbCG=0;}
	if (nC!=0) dbCC=(nCC*ll)/(double)(nC*nC);
	else dbCC=0;

	// Compute dinucleotide bias on sliding window
	if (!is_GCCGonly) {
	    db_winAA = new DinucleotideBiasScoreCalculator(curation, winSize, 'A', 'A');
	    db_winAT = new DinucleotideBiasScoreCalculator(curation, winSize, 'A', 'T');
	    db_winAG = new DinucleotideBiasScoreCalculator(curation, winSize, 'A', 'G');
	    db_winAC = new DinucleotideBiasScoreCalculator(curation, winSize, 'A', 'C');
	    db_winTA = new DinucleotideBiasScoreCalculator(curation, winSize, 'T', 'A');
	    db_winTT = new DinucleotideBiasScoreCalculator(curation, winSize, 'T', 'T');
	    db_winTG = new DinucleotideBiasScoreCalculator(curation, winSize, 'T', 'G');
	    db_winTC = new DinucleotideBiasScoreCalculator(curation, winSize, 'T', 'C');
	    db_winGA = new DinucleotideBiasScoreCalculator(curation, winSize, 'G', 'A');
	    db_winGT = new DinucleotideBiasScoreCalculator(curation, winSize, 'G', 'T');
	    db_winGG = new DinucleotideBiasScoreCalculator(curation, winSize, 'G', 'G');
	    db_winCA = new DinucleotideBiasScoreCalculator(curation, winSize, 'C', 'A');
	    db_winCT = new DinucleotideBiasScoreCalculator(curation, winSize, 'C', 'T');
	    db_winCC = new DinucleotideBiasScoreCalculator(curation, winSize, 'C', 'C');
	}
	db_winGC = new DinucleotideBiasScoreCalculator(curation, winSize, 'G', 'C');
	db_winCG = new DinucleotideBiasScoreCalculator(curation, winSize, 'C', 'G');

	// Compute Karlin signature
	score_mean=0;
	score_min=Double.POSITIVE_INFINITY;
	score_max=Double.NEGATIVE_INFINITY;
	for (i=halfWinSize; i<l-halfWinSize; i++) {
	    if (!is_GCCGonly) 
		s=(Math.abs(db_winAA.scores[i]-dbAA)+Math.abs(db_winAT.scores[i]-dbAT)+
		   Math.abs(db_winAG.scores[i]-dbAG)+Math.abs(db_winAC.scores[i]-dbAC)+
		   Math.abs(db_winTA.scores[i]-dbTA)+Math.abs(db_winTT.scores[i]-dbTT)+
		   Math.abs(db_winTG.scores[i]-dbTG)+Math.abs(db_winTC.scores[i]-dbTC)+
		   Math.abs(db_winGA.scores[i]-dbGA)+Math.abs(db_winGT.scores[i]-dbGT)+
		   Math.abs(db_winGG.scores[i]-dbGG)+Math.abs(db_winGC.scores[i]-dbGC)+
		   Math.abs(db_winCA.scores[i]-dbCA)+Math.abs(db_winCT.scores[i]-dbCT)+
		   Math.abs(db_winCG.scores[i]-dbCG)+Math.abs(db_winCC.scores[i]-dbCC))/16.0;
	    else	       
		s=(Math.abs(db_winGC.scores[i]-dbGC)+Math.abs(db_winCG.scores[i]-dbCG))/2.0;
	    scores[i]=s;
	    score_mean +=s;
	    score_min = Math.min(s, score_min);
	    score_max = Math.max(s, score_max);
	}
	score_mean = score_mean / (double)(l-winSize+1);

	for (i=0; i<halfWinSize; i++) scores[i] = score_mean;
	for (i=l-halfWinSize; i<l; i++) scores[i] = score_mean;

	if (is_2SDcutoff) compute2SDcutoff(l);

	// For test
	//String file_name = "Karlin";
	//if (is_GCCGonly) file_name += "GCCG";
	//if (is_2SDcutoff)  file_name += "_2SD";	
	//writeFile(file_name, l);
      }
    }
    
}
