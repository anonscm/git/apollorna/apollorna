package apollo.gui.menus;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.log4j.*;

import apollo.gui.ApolloFrame;
import apollo.gui.synteny.CurationManager;
import apollo.gui.synteny.GuiCurationState;
import apollo.gui.rna.InteractionSearchSetDialog;
import apollo.gui.rna.SecondaryStructureView;
import apollo.gui.rna.FoldBasedSearchMethod;

/**
 * class RNAMenu represents the menu to support non protein coding RNA identification
 */
public class RNAMenu extends JMenu
{
  protected final static Logger logger = LogManager.getLogger(HelpMenu.class);

  private ApolloFrame frame;
  private JMenuItem   secondaryStructureComputeMenuItem;
  private JMenuItem   interactionSearchMenuItem;
  private JMenuItem   aboutMenuItem;
  private JDialog     aboutBox;

  /**
   * Builder: initalization of the rna menu
   * @param frame Main apollo frame
   */
  public RNAMenu(ApolloFrame frame)
    {
    super("RNA");
    this.frame = frame;
    menuInit();

    // add a menu listener to manage the menu
    this.addMenuListener(new MenuListener() {
      public void menuSelected(MenuEvent e) {
	// If no sequence is loaded in Apollo, disable the items "Search
        // RNA/RNA interaction"and "Secondary structure compute" in the RNA menu
        if ((getActiveCurState().getCurationSet().getRefSequence() == null)) {
	    secondaryStructureComputeMenuItem.setEnabled(false);
	    interactionSearchMenuItem.setEnabled(false);
        } else 
	    // if the sequence is reverse complement, disable the item "Search
	    // RNA/RNA interaction"
	    if (getActiveCurState().getSZAP().isReverseComplement() == true) {
		secondaryStructureComputeMenuItem.setEnabled(true);
		interactionSearchMenuItem.setEnabled(false);
	    } else {
	        // Test if external required softwares can be launched
		if (isLaunchableRNAfold() && isLaunchableConvert())
		    secondaryStructureComputeMenuItem.setEnabled(true);
		else
		    secondaryStructureComputeMenuItem.setEnabled(false);
		if (isLaunchableRNAfold() && isLaunchableRNAeval())
		    interactionSearchMenuItem.setEnabled(true);
		else
		    interactionSearchMenuItem.setEnabled(false);
       }
      }
	    
	    public void menuCanceled(MenuEvent e) { }

	    public void menuDeselected(MenuEvent e) { }
      });
  }

  /**
   * Menu initialization
   */
  public void menuInit()
  {
    secondaryStructureComputeMenuItem = new JMenuItem(
        "Secondary structure compute...");
    interactionSearchMenuItem = new JMenuItem("Search RNA/RNA Interaction");
    aboutMenuItem = new JMenuItem("About RNA menu");
   
    add(secondaryStructureComputeMenuItem);
    add(interactionSearchMenuItem);
    add(aboutMenuItem);

    secondaryStructureComputeMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        new SecondaryStructureView(getActiveCurState().getSelectionManager()
            .getSelection(), getActiveCurState().getController());
      }
    });

    interactionSearchMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        // Creation of the window to search interaction
        new InteractionSearchSetDialog(frame, getActiveCurState());
      }
    });
    aboutMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
	showAboutBox();
      }
    });
   }

  /**
   * Get GuiCurationState
   * @return GuiCurationState
   */
  private GuiCurationState getActiveCurState()
  {
    return CurationManager.getCurationManager().getActiveCurState();
  }


  /**
   * Test if RNAfold can be launched
   */
  public boolean isLaunchableRNAfold()
  {
      boolean isLaunchable = false;
      try{
	  Process process = Runtime.getRuntime().exec(SecondaryStructureView.getRNAfoldCommand());
	  OutputStream outputStream = process.getOutputStream();   
	  
	  // write the RNAfold exit command
	  String command = "@\n";
	  outputStream.write(command.getBytes());
	  outputStream.flush();
	  outputStream.close();
	  
	  process.waitFor();
	  process.destroy();
	  
	  isLaunchable = true;
      }	            
      catch(Exception e) {}
      return isLaunchable;
  }

  /**
   * Test if RNAeval can be launched
   */
  public boolean isLaunchableRNAeval()
  {
      boolean isLaunchable = false;
      try{ 
	  Process process =  Runtime.getRuntime().exec(FoldBasedSearchMethod.getRNAevalCommand());
	  OutputStream outputStream = process.getOutputStream();   
	  
	  // write the RNAeval exit command
	  String command = "@\n";
	  outputStream.write(command.getBytes());
	  outputStream.flush();
	  outputStream.close();
	  
	  process.waitFor();
	  process.destroy();
	  
	  isLaunchable = true;		      
      }							 
      catch(Exception e) {}
      return isLaunchable;
  }

  /**
   * Test if convert of ImageMagick (requires ghostscript) can be launched
   */
  public boolean isLaunchableConvert()
  {
    boolean isLaunchable = false;

    try {
	//  create a temporary postscript test file
	File tmpDir = new File(SecondaryStructureView.getTmpDir());
	if (!tmpDir.isDirectory()) tmpDir.mkdir();
	String tmpGifFileName = tmpDir+"test.gif";
	String tmpPSFileName = tmpDir+"test.ps";
	FileWriter tmpPSFileWriter = new FileWriter(tmpPSFileName);
	tmpPSFileWriter.write("%!PS-Adobe-3.0 EPSF-3.0\n");
	tmpPSFileWriter.close();

	isLaunchable = SecondaryStructureView.convert(tmpPSFileName,tmpGifFileName,"");
	File tmpGifFile = new File(tmpGifFileName);
	File tmpPSFile = new File(tmpPSFileName);
	
	if (isLaunchable && !tmpGifFile.exists()) 
            // ghostscript is not installed
	    isLaunchable=false;
	tmpGifFile.delete();
	tmpPSFile.delete();
    }		  
    catch(IOException e) {logger.error(e.getMessage(), e);}
    catch(Exception e) {logger.error(e.getMessage(), e);}
    return isLaunchable;
  }

  /**
   * Display a message window on how to activate TNA menu items
   */
  public void showAboutBox() {
    if (aboutBox == null) {      
      JPanel    groupPanel   = new JPanel   (new BorderLayout());
      JPanel    buttonPanel  = new JPanel   (true);      
      JTextPane   messBox    = new JTextPane();
      JScrollPane messPane   = new JScrollPane(messBox);
	      	      
      final String downloadAddress = "http://carlit.toulouse.inra.fr/ApolloRNA/download.html";
      final String userguideAddress = "http://carlit.toulouse.inra.fr/ApolloRNA/apollornauserguide.html";
      
      messBox.setEditable(false);
      messBox.setBackground(Color.white);
      
      messPane.setPreferredSize(new Dimension(695,190));
      messPane.setMinimumSize(new Dimension(695,190));
      
      SimpleAttributeSet attrs = new SimpleAttributeSet();
      
      StyleConstants.setAlignment(attrs, StyleConstants.ALIGN_LEFT);
      StyleConstants.setForeground(attrs,Color.DARK_GRAY);
      
      StyledDocument doc = messBox.getStyledDocument();
      
      Style regular = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
      Style s = doc.addStyle("button", regular);	        
      JButton buttonDownload = new JButton(downloadAddress);	 
      JButton buttonUserguide = new JButton(userguideAddress);
      
      buttonDownload.setLocation(0, -10);
      buttonDownload.setMargin(new Insets(6,-2,-4,-2));
      buttonDownload.addActionListener(new ActionListener(){
	      public void actionPerformed(ActionEvent arg0) {
		  apollo.util.HTMLUtil.loadIntoBrowser(downloadAddress);
	      }	    	  
      });
      
      buttonUserguide.setLocation(0, -10);
      buttonUserguide.setMargin(new Insets(6,-2,-4,-2));
      buttonUserguide.addActionListener(new ActionListener(){
	      public void actionPerformed(ActionEvent arg0) {
		  apollo.util.HTMLUtil.loadIntoBrowser(userguideAddress);
	      }	    	  
      });
      
      try {	    	  
	  StyleConstants.setForeground(attrs,Color.DARK_GRAY);                
	  doc.insertString(0,"\nThe RNA menu proposes to: \n\t- visualize the predicted secondary structure of selected sequence, \n\t- search for RNA/RNA interaction between selected sequence and specifies regions. \nFor a user guide, see ",attrs); 
	  StyleConstants.setComponent(s, buttonUserguide);
	  doc.insertString(doc.getLength(), userguideAddress, doc.getStyle("button"));
	  
	  doc.insertString(doc.getLength(),"\n\nThe items are automatically activated when the required external softwares are installed: \n\t- RNAfold (Vienna RNA package), convert (ImageMagick) for the secondary structure prediction, \n\t- RNAfold, RNAeval (Vienna RNA package) for RNA/RNA interaction search.\nFor sofwares installation, see ",attrs); 
	  StyleConstants.setComponent(s, buttonDownload);
	  doc.insertString(doc.getLength(), downloadAddress, doc.getStyle("button"));

	  
      } catch (Exception e) {
	  logger.error(e.getMessage(), e);
      }
      messBox.setCaretPosition(0);  // Scroll text box up to top (default is scrolled to bottom)
      
      JButton   button = new JButton  ("Close");
      
      aboutBox = new JDialog(frame, "About RNA menu", false);
      aboutBox.getContentPane().add(groupPanel, BorderLayout.CENTER);      
      
      groupPanel.add(messPane,    BorderLayout.CENTER);
      groupPanel.add(buttonPanel, BorderLayout.SOUTH);
      
      buttonPanel.add(button);
      
      button.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
		  aboutBox.setVisible(false);
	      }
      } );
    }
    aboutBox.setResizable(false);
    aboutBox.pack();
    aboutBox.show();
  }

}
