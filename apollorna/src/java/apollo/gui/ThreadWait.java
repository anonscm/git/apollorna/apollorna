package apollo.gui;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * This class is a independent thread that displays a progress bar while data 
 * is being loaded.
 */
public class ThreadWait extends Thread 
{
	private JDialog 	progressDialog 	;
	private JPanel 		containerPanel 	;
	private JProgressBar 	progressBar 	;
	
	/**
	 * ThreadWait constructor
	 * 
	 * @param parentFrame	Frame on which the dialog box will be attached
	 * @param dialogTitle	Title of the dialog box
	 */
	public ThreadWait(JFrame parentFrame, String dialogTitle){
		this.progressDialog = new JDialog(parentFrame,dialogTitle, true);
	}
	
	public ThreadWait(JDialog parentFrame, String dialogTitle){
		this.progressDialog = new JDialog(parentFrame,dialogTitle);
	}

	/**
	 * Initialises and starts the thread
	 */
	public void run() {
		this.init();
	}
	
	/**
	 * Stops the thread
	 */
	public void stopWait() {
		this.progressDialog.setVisible(false);
		this.progressDialog.dispose();
	}
	
	
	/**
	 * Initialises graphical components
	 */
	public void init() {
		containerPanel 	= new JPanel();
		progressBar 	= new JProgressBar();
		this.progressBar.setIndeterminate(true);
		this.progressBar.setStringPainted(false);
		this.progressBar.setPreferredSize(new Dimension(250, 20));
		this.containerPanel.add(this.progressBar);
		this.containerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.containerPanel.setPreferredSize(new Dimension(300,50));
		this.progressDialog.getContentPane().add(this.containerPanel);
		this.progressDialog.setLocation(300,300);
		this.progressDialog.pack();
		this.progressDialog.setVisible(true);
	}
}
