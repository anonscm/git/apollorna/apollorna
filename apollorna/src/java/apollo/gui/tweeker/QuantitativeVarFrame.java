package apollo.gui.tweeker;

import java.util.HashMap;
import java.text.DecimalFormat;

import java.awt.event.*;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.WindowConstants;
import javax.swing.JColorChooser;
import javax.swing.JTable;
import javax.swing.table.*;
import javax.swing.AbstractCellEditor;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.ChangeListener;
import javax.swing.JSlider;
import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JOptionPane;

import org.apache.log4j.*;

import apollo.gui.genomemap.StrandedZoomableApolloPanel;
import apollo.gui.synteny.CurationManager;

/**
 * This class is a frame used to sets or unsets additionnal graphs on Apollo main 
 * visualisation panel
 */
public class QuantitativeVarFrame extends JFrame {
    
    protected final static Logger logger = LogManager.getLogger(QuantitativeVarControler.class);
    
    private JPanel filePanel = new JPanel();
    private JPanel computePanel = new JPanel();
    private JButton closeButton = new JButton("Close");
    private QuantitativeVarTable graphsTable;
    private QuantitativeVarControler quantitativeVarControler;
    private QuantitativeVarComputedTable computedGraphsTable;
    private String[] varNamesTable = new String [] {"GC%", "GC% with 2 SD cutoff", "ChargaffAT","ChargaffGC", 
	      "Normalized Dinucleotide GC", "Normalized Dinucleotide CG", "Normalized Dinucleotide AA", 
	      "Dinucleotide GC bias", "Dinucleotide CG bias", "Dinucleotide AA bias", "Karlin Signature", 
              "Karlin Signature with 2 SD cutoff","Karlin GC CG","Karlin GC CG with 2 SD cutoff"  };
    
    public static QuantitativeVarFrame quantitativeVarFrame = null;
    
    /**
     * QuantitativeVarFrame constructor
     * Creates and initialises graphical interface components
     */
    private QuantitativeVarFrame (){
	super("Show a quantitative variable...");
	quantitativeVarControler = new QuantitativeVarControler(this, this.getSZAP());
	
	this.graphsTable = new QuantitativeVarTable(new QuantitativeVarTableModel(5));
	this.graphsTable.setPreferredScrollableViewportSize(new Dimension(630,80));
	this.graphsTable.setDefaultEditor(Color.class, new QuantitativeVarTableCellColorEditor(this.graphsTable));
	this.graphsTable.setDefaultEditor(JButton.class, new QuantitativeVarTableCellLoadEditor(this.graphsTable));
	this.graphsTable.setDefaultRenderer(Color.class, new QuantitativeVarTableCellColorRenderer(true));
	this.graphsTable.setDefaultRenderer(JButton.class, new QuantitativeVarTableCellLoadRenderer());
	
	this.computedGraphsTable = new QuantitativeVarComputedTable(new QuantitativeComputedVarTableModel(this.varNamesTable));
	this.computedGraphsTable.setPreferredScrollableViewportSize(new Dimension(630,420));
	this.computedGraphsTable.setRowHeight(30);
	this.computedGraphsTable.getModel().addTableModelListener(new TableModelListener(){
		public void tableChanged(TableModelEvent e) {
		    if(e.getColumn() == 1)
			QuantitativeVarFrame.this.handleComputedGraphsVisibilityEvent(e, this);
		    else {
			if(e.getColumn() == 4 && e.getType() == TableModelEvent.UPDATE) {
			    QuantitativeVarFrame.this.handleComputedGraphsFrameSizeChange();
			}
		    }
		}
	    });
	this.computedGraphsTable.setDefaultRenderer(Color.class, new QuantitativeVarTableCellColorRenderer(true));
	this.computedGraphsTable.setDefaultEditor(Color.class, new QuantitativeVarTableCellColorEditor(this.computedGraphsTable));
	
	this.setGraphsTableWidths();	
	this.setTablesColumnsResizable(false);
	
	this.closeButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent event) {
		    QuantitativeVarFrame.this.setVisible(false);
		    QuantitativeVarFrame.this.dispose();
		}
	    });
	
	this.filePanel.add(new JScrollPane(this.graphsTable));
	this.computePanel.add(new JScrollPane(this.computedGraphsTable));
	
	this.getContentPane().add(filePanel, BorderLayout.NORTH);
	this.getContentPane().add(computePanel, BorderLayout.CENTER);
	this.getContentPane().add(closeButton, BorderLayout.SOUTH);
	
	this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	this.pack();
    }
    
    /**
     * Method that handle reading frame size modifications
     */
    public void handleComputedGraphsFrameSizeChange() {
	if(((Boolean)this.computedGraphsTable.getValueAt(this.computedGraphsTable.getEditingRow(),1)).booleanValue()) {
	    JSlider currentSlider = ((JSlider)this.computedGraphsTable.getValueAt(this.computedGraphsTable.getEditingRow(),3));
	    
	    int newValue = currentSlider.getValue();
	    try{
		newValue = Integer.parseInt((String)this.computedGraphsTable.getValueAt(this.computedGraphsTable.getEditingRow(),4));
	    } catch(Exception numberException) {
		logger.error("Not a valid integer ! " + numberException.getMessage());
	    }
	    BoundedRangeModel bRM = (BoundedRangeModel)((QuantitativeVarComputedTable)this.getComputedTable()).getSliderModels().get(this.getComputedTable().getValueAt(this.computedGraphsTable.getEditingRow(),0));
	    bRM.setValue(newValue);
	    currentSlider.setModel(bRM);
	    if(newValue > bRM.getMaximum() || newValue < bRM.getMinimum()) {
		this.computedGraphsTable.setValueAt(new String(Integer.toString(bRM.getValue())),this.computedGraphsTable.getEditingRow(), 4);
	    }

	    quantitativeVarControler.updateScores();
	    
	    DecimalFormat df = new DecimalFormat();
	    df.setMaximumFractionDigits(2);
	    df.setMinimumFractionDigits(2);
	    computedGraphsTable.setValueAt(df.format(this.quantitativeVarControler.getMin()),computedGraphsTable.getEditingRow(), 5);
	    computedGraphsTable.setValueAt(df.format(this.quantitativeVarControler.getMax()),computedGraphsTable.getEditingRow(), 6);
	    computedGraphsTable.setValueAt(df.format(this.quantitativeVarControler.getMean()),computedGraphsTable.getEditingRow(), 7);
	    
	    this.computedGraphsTable.repaint();
	}
    }
    
    /**
     * Method that handles graphs visibility modifications
     * @param event	event that has occured
     * @param listener	event manager registered to the component that notify user's actions
     */
    public void handleComputedGraphsVisibilityEvent(TableModelEvent event, TableModelListener listener) {
	if(((Boolean)this.computedGraphsTable.getModel().getValueAt(this.computedGraphsTable.getEditingRow(),1)).booleanValue() == true) 
	    this.showComputedGraph(event.getFirstRow());
	else
	    this.hideComputedGraph(this.computedGraphsTable, event.getFirstRow());
    }
    
    /**
     * Singleton implantation
     * Create a new object if i doesn't exist or return existing one
     * @return	an instance of this class
     */
    public static QuantitativeVarFrame openQuantitativeVarFrame (){
	if (QuantitativeVarFrame.quantitativeVarFrame == null){
	    QuantitativeVarFrame.quantitativeVarFrame = new QuantitativeVarFrame();
	    QuantitativeVarFrame.quantitativeVarFrame.setVisible(true);
	} else {
	    QuantitativeVarFrame.quantitativeVarFrame.setVisible(true);
	}
	return QuantitativeVarFrame.quantitativeVarFrame;
    }
    
    /**
     * Sets columns widths for both JTables
     */
    public void setGraphsTableWidths() {
	this.graphsTable.getColumnModel().getColumn(1).setMaxWidth(this.graphsTable.getModel().getColumnName(1).length() * 10);
	this.graphsTable.getColumnModel().getColumn(2).setMaxWidth(this.graphsTable.getModel().getColumnName(2).length() * 10);
	this.graphsTable.getColumnModel().getColumn(3).setMaxWidth(this.graphsTable.getModel().getColumnName(3).length() * 10);
	this.graphsTable.getColumnModel().getColumn(4).setMaxWidth(this.graphsTable.getModel().getColumnName(4).length() * 10);
	this.graphsTable.getColumnModel().getColumn(0).setMaxWidth(this.graphsTable.getModel().getColumnName(0).length() * 50);
	
	this.computedGraphsTable.getColumnModel().getColumn(1).setMaxWidth(this.graphsTable.getColumnModel().getColumn(1).getMaxWidth());
	this.computedGraphsTable.getColumnModel().getColumn(2).setMaxWidth(this.graphsTable.getColumnModel().getColumn(2).getMaxWidth());
	this.computedGraphsTable.getColumnModel().getColumn(3).setMaxWidth(this.graphsTable.getColumnModel().getColumn(3).getMaxWidth());
	this.computedGraphsTable.getColumnModel().getColumn(4).setMaxWidth(this.graphsTable.getColumnModel().getColumn(4).getMaxWidth());
	this.computedGraphsTable.getColumnModel().getColumn(5).setMaxWidth(this.graphsTable.getColumnModel().getColumn(4).getMaxWidth());
	this.computedGraphsTable.getColumnModel().getColumn(6).setMaxWidth(this.graphsTable.getColumnModel().getColumn(4).getMaxWidth());
	this.computedGraphsTable.getColumnModel().getColumn(7).setMaxWidth(this.graphsTable.getColumnModel().getColumn(4).getMaxWidth());
	this.computedGraphsTable.getColumnModel().getColumn(0).setMaxWidth(this.graphsTable.getColumnModel().getColumn(0).getMaxWidth());
    }
    
    /**
     * Sets columns to resizeable or not
     * @param state	true - column are resizeable, false - not resizeable
     */
    public void setTablesColumnsResizable(boolean state) {
	for(int i = 0; i<this.graphsTable.getColumnCount();i++) {
	    this.graphsTable.getColumnModel().getColumn(i).setResizable(state);
	}
	for(int i = 0; i<this.computedGraphsTable.getColumnCount();i++) {
	    this.computedGraphsTable.getColumnModel().getColumn(i).setResizable(state);
	}
    }
    
    /**
     * Adds a JSlider to a new computed graph
     * @param model	model of the JSlider
     */
    public void setSlider (BoundedRangeModel model) {
	DecimalFormat df = new DecimalFormat();
	df.setMaximumFractionDigits(2);
	df.setMinimumFractionDigits(2);

	this.computedGraphsTable.addSliderModel((String)this.computedGraphsTable.getValueAt(this.computedGraphsTable.getEditingRow(),0),model);
	this.computedGraphsTable.setValueAt(Integer.toString(model.getValue()),this.computedGraphsTable.getEditingRow(),4);
	this.computedGraphsTable.setValueAt(df.format(this.quantitativeVarControler.getMin()),this.computedGraphsTable.getEditingRow(),5);
	this.computedGraphsTable.setValueAt(df.format(this.quantitativeVarControler.getMax()),this.computedGraphsTable.getEditingRow(),6);
	this.computedGraphsTable.setValueAt(df.format(this.quantitativeVarControler.getMean()),this.computedGraphsTable.getEditingRow(),7);
    }
    
    /**
     * Removes the JSlider associated with a graph
     */
    public void unSetSlider () {
	this.computedGraphsTable.removeSliderModel((String)this.computedGraphsTable.getValueAt(this.computedGraphsTable.getEditingRow(),0));
	this.computedGraphsTable.setValueAt(new String(""),this.computedGraphsTable.getEditingRow(),4);
	this.computedGraphsTable.setValueAt(new String(""),this.computedGraphsTable.getEditingRow(),5);
	this.computedGraphsTable.setValueAt(new String(""),this.computedGraphsTable.getEditingRow(),6);
	this.computedGraphsTable.setValueAt(new String(""),this.computedGraphsTable.getEditingRow(),7);
     }
    
    /**
     * @return reference to Apollo main panel
     */
    public StrandedZoomableApolloPanel getSZAP() { 
	return CurationManager.getCurationManager().getActiveCurState().getSZAP();
    }
    
    /**
     * @return table that manage graphs created from external values
     */
    public JTable getGraphsTable() {
	return this.graphsTable;
    }
    
    /**
     * @return table that manage graphs computed by Apollo
     */
    public JTable getComputedTable() {
	return this.computedGraphsTable;
    }
    
    /**
     * Asks to load data to the controler and to create a new grah
     * @param tableConcerned	table that asks to create a new graph
     */
    public void loadData(JTable tableConcerned) {
	this.quantitativeVarControler.loadData(tableConcerned);
    }
    
    /**
     * Removes an existing graph
     * @param tableConcerned	table that asks to delete a graph
     */
    public void deleteGraph(JTable tableConcerned) {
	this.graphsTable.getModel().setValueAt(new Boolean(false),this.graphsTable.getEditingRow(),1);
	this.setGraphVisibility(tableConcerned);
	this.quantitativeVarControler.deleteGraphView(this.graphsTable);
	this.graphsTable.getModel().setValueAt(new String(""),this.graphsTable.getEditingRow(),0);
	this.graphsTable.removeRow(this.getGraphsTable().getEditingRow());
	this.graphsTable.repaint();
    }
    
    /**
     * Asks to change a graph visibility
     * @param tableConcerned	table that asks to change graph visibility
     */
    public void setGraphVisibility(JTable tableConcerned) {
	boolean state = ((Boolean)tableConcerned.getModel().getValueAt(tableConcerned.getEditingRow(),1)).booleanValue();
	this.quantitativeVarControler.setGraphVisible(tableConcerned,state);
    }
    
    /**
     * Asks to change graph's color
     * @param tableConcerned	table that asks to delete a graph
     * @param newGraphColor		new color
     */
    public void setGraphColor(JTable tableConcerned, Color newGraphColor) {
	this.quantitativeVarControler.setGraphColor(tableConcerned, newGraphColor);
    }
    
    /**
     * Asks to display a new computed graph
     * @param indexGraph	index of graph in the related table
     */
    public void showComputedGraph(int indexGraph) {
	this.quantitativeVarControler.addComputedGraphView(indexGraph);
	this.getSZAP().repaint();
    }
    
    /**
     * Asks to hide an existing graph
     * @param tableConcerned table that asks to hide a graph
     * @param indexGraph	index of graph in the related table
     */
    public void hideComputedGraph(JTable tableConcerned, int indexGraph) {
	this.setGraphVisibility(tableConcerned);
	tableConcerned.getModel().setValueAt(Color.BLACK,tableConcerned.getEditingRow(),2); 
	this.quantitativeVarControler.removeComputedGraphView();
	this.getSZAP().repaint();
    }
    
    /**
     * This class is a specialisation of the JTable class.
     * It was needed to redefine the method removeRow(int) to be able to
     * rearranged the JTable. As JButton in the JTable has to be renderered and editor
     * not in same way, it was also necessary to redefine 
     * getCellRenderer(int, int) and getCellEditor(int, int)
     */
    class QuantitativeVarTable extends JTable {
	
	/**
	 * Constructor
	 * @param model	table model
	 */
	public QuantitativeVarTable(TableModel model) {
	    super(model);
	}
	
	/**
	 * Returns the right renderer for elements to be displayed in the JTable
	 */
	public TableCellRenderer getCellRenderer(int row, int col) {
	    if(col == 4)
		return new QuantitativeVarTableCellDeleteRenderer();
	    else 
		return super.getCellRenderer(row,col);
	}
	
	/**
	 * Returns the right editor for components to be able to reveive user's command
	 * in the JTable
	 */
	public TableCellEditor getCellEditor(int row, int col) {
	    if(col == 4)
		return new QuantitativeVarTableCellDeleteEditor(this);
	    else 
		return super.getCellEditor(row,col);
	}
	
	/**
	 * Handles changes that occur in the JTable
	 */
	public void tableChanged(TableModelEvent event) {
	    if (event.getColumn() == 1 && (this.getModel().getValueAt(this.getEditingRow(),0).equals(new String("")) != true))
		QuantitativeVarFrame.this.setGraphVisibility(this);
	    else
		super.tableChanged(event);
	}
	
	/**
	 * Remove a row from the table model
	 * @param row	row to remove
	 */
	public void removeRow(int row) {
	    ((QuantitativeVarTableModel)this.getModel()).removeRow(row);
	}
    }
    
    /**
     * This class is a specialisation of the JTable class.
     * Rendering and editing the JSlider has to be done in different ways, if it already
     * has a model associated with.
     */
    class QuantitativeVarComputedTable extends JTable {
	private HashMap sliderModels;
	
	/**
	 * Constructor
	 * @param model		table model
	 */
	public QuantitativeVarComputedTable(TableModel model) {
	    super(model);
	    this.sliderModels = new HashMap();
	}
	
	/**
	 * Returns the right object depending on the model of the JSlider
	 */
	public TableCellRenderer getCellRenderer(int row, int col) {
	    if(col == 3) {
		if(((Boolean)this.getValueAt(row,1)).booleanValue() == true) {
		    BoundedRangeModel currentSliderModel = (BoundedRangeModel)sliderModels.get(this.getValueAt(row,0));
		    if(currentSliderModel != null)
			return new QuantitativeVarTableCellSliderRenderer(currentSliderModel);
		    else 
			return new QuantitativeVarTableCellSliderRenderer(new DefaultBoundedRangeModel());
		}
		return new QuantitativeVarTableCellSliderRenderer(new DefaultBoundedRangeModel());
	    } else
		return super.getCellRenderer(row,col);		
	}
	
	/**
	 * Returns the right object depending on the model of the JSlider
	 */
	public TableCellEditor getCellEditor(int row, int col) {
	    if(col == 3) {
		if(((Boolean)this.getValueAt(row,1)).booleanValue() == true) {
		    BoundedRangeModel currentSliderModel = (BoundedRangeModel)sliderModels.get(this.getValueAt(row,0));
		    if(currentSliderModel != null)
			return new QuantitativeVarTableCellSliderEditor(this, currentSliderModel);
		    else 
			return new QuantitativeVarTableCellSliderEditor(this, new DefaultBoundedRangeModel());
		}
		return new QuantitativeVarTableCellSliderEditor(this, new DefaultBoundedRangeModel());
	    } else
		return super.getCellEditor(row,col);
	}
	
	/**
	 * @return collection of models associated with each JSlider
	 */
	public HashMap getSliderModels() {
	    return this.sliderModels;
	}
	
	/**
	 * Binds a new model to a JSlider and so to a computed graph
	 * @param varName	name of the computed variable
	 * @param model		JSlider model
	 */
	public void addSliderModel(String varName, BoundedRangeModel model) {
	    this.sliderModels.put(varName, model);
	}
	
	/**
	 * Removes a model form the collection
	 * @param varName	name of the associated variable
	 */
	public void removeSliderModel(String varName) {
	    this.sliderModels.remove(varName);
	}
    }
    
    /**
     * Redefinition of the table model.
     * Manages deactivation of components in the JTable
     */
    class QuantitativeVarTableModel extends AbstractTableModel {
	
	private String[] columnNames = {"Quantitative Variable", "Show", "Color", "Load Data", "Delete"};
	private Object[][] rowData;
	
	/**
	 * Constructor
	 * @param numberOfLines	number of variables to be displayed
	 */
	public QuantitativeVarTableModel (int numberOfLines){
	    this.rowData = new Object[numberOfLines][this.columnNames.length];
	    for (int i=0; i<numberOfLines;i++){
		this.rowData[i][0] = new String("");
		this.rowData[i][1] = new Boolean(false);
		this.rowData[i][2] = Color.black;
		this.rowData[i][3] = new JButton();
		this.rowData[i][4] = new JButton();
	    }
	}
	
	/**
	 * Returns the number of rows
	 */
	public int getRowCount() {
	    return this.rowData.length;
	}
	
	/**
	 * Returns the number of columns
	 */
	public int getColumnCount() {
	    return this.columnNames.length;
	}
	
	/**
	 * Returns column's header
	 */
	public String getColumnName(int col) {
	    return this.columnNames[col].toString();
	}
	
	/**
	 * Return value of a particular cell
	 */
	public Object getValueAt(int row, int col) {
	    return this.rowData[row][col];
	}
	
	/**
	 * Indicates if a cell is editable or not
	 */
	public boolean isCellEditable(int row, int col) {
	    /* Set of tests to determine which cells are editable in the JTable
	     * Within a row, all cells are deactivated until data is loaded.
	     * It's only possible to load data from the first row to the last one.*/
	    if(row != 0) {
    		if(!this.rowData[row-1][0].equals("")) {
		    if(col != 0) {
			if(!this.rowData[row][0].equals("") || col == 3)
			    return true;
			else
			    return false;
		    } else return false;
    		} else return false;
	    } else {
    		if(col != 0 ) {
		    if(!this.rowData[row][0].equals("") || col == 3)
			return true;
		    else
			return false;
    		} else return false; 
	    }
	}
	
	/**
	 * Sets a value to a particular cell
	 */
	public void setValueAt(Object value, int row, int col) {
	    this.rowData[row][col] = value;
	    this.fireTableCellUpdated(row, col);
	}
	
	/**
	 * Return objects class of a column
	 */
	public Class getColumnClass(int col) {
	    return this.getValueAt(0,col).getClass();
	}
	
	/**
	 * Removes a row from the model and reaaranges the table
	 * @param row	row to be deleted
	 */
	public void removeRow(int row) {
	    if(row != this.getRowCount()-1) {
    		for (int i = row; i < this.getRowCount();i++) {
		    if(i != this.getRowCount()-1) {
			for (int j = 0; j < this.getColumnCount();j++) {
			    this.rowData[i][j] = this.rowData[i+1][j];
			}
		    } else {
			this.rowData[i][0] = new String("");
			this.rowData[i][1] = new Boolean(false);
			this.rowData[i][2] = Color.BLACK;
			this.rowData[i][3] = new JButton();
			this.rowData[i][4] = new JButton();
		    }
    		}
    		this.fireTableDataChanged();
	    }
	}
    }
    
    /**
     * Redefinition of the table model for computed variables
     */
    class QuantitativeComputedVarTableModel extends AbstractTableModel {
	
	private String[] columnNames = {"Computed Variable", "Show", "Color", "Frame", "Size","Min","Max","Mean"};
	private Object[][] rowData;
	private int sizeOfData;
	
	/**
	 * Constructor
	 * @param varNamesTable names of variables to be calculated
	 */
	public QuantitativeComputedVarTableModel (String[] varNamesTable){
	    this.sizeOfData = varNamesTable.length; 
	    this.rowData = new Object[sizeOfData][this.columnNames.length];
	    for (int i=0; i<sizeOfData;i++){
		this.rowData[i][0] = new String(varNamesTable[i]);
		this.rowData[i][1] = new Boolean(false);
		this.rowData[i][2] = Color.BLACK;
		this.rowData[i][3] = new JSlider();
		this.rowData[i][4] = new String();
		this.rowData[i][5] = new String();
		this.rowData[i][6] = new String();
		this.rowData[i][7] = new String();
	    }
	}
	
	/**
	 * Returns the number of rows
	 */
	public int getRowCount() {
	    return this.rowData.length;
	}
	
	/**
	 * Returns the number of columns
	 */
	public int getColumnCount() {
	    return this.columnNames.length;
	}
	
	/**
	 * Returns column's header
	 */
	public String getColumnName(int col) {
	    return this.columnNames[col].toString();
	}
	
	/**
	 * Return value of a particular cell
	 */
	public Object getValueAt(int row, int col) {
	    return this.rowData[row][col];
	}
	
	/**
	 * Indicates if a cell is editable or not
	 */
	public boolean isCellEditable(int row, int col) {
	    if(col != 0) {
    		if(col !=1) {
		    boolean graphVisible = ((Boolean)this.rowData[row][1]).booleanValue();
		    if(graphVisible)
			return true;
		    else
			return false;
    		} else 
		    return true;
	    }
	    else return false;
	}
	
	/**
	 * Sets a value to a particular cell
	 */
	public void setValueAt(Object value, int row, int col) {
	    this.rowData[row][col] = value;
	    this.fireTableCellUpdated(row, col);
	}
	
	/**
	 * Return objects class of a column
	 */
	public Class getColumnClass(int col) {
	    return this.getValueAt(0,col).getClass();
	}
    }
    
    /**
     * This class is used to bring a JColorChooser up when the user's asks for
     */
    class QuantitativeVarTableCellColorEditor extends AbstractCellEditor
	implements TableCellEditor, ActionListener {
	private Color currentColor;
	private JButton button;
	private JColorChooser colorChooser;
	private JDialog dialog;
	protected static final String EDIT = "edit";
	private JTable tableConcerned;
	
	public QuantitativeVarTableCellColorEditor(JTable tableC)  {
	    this.tableConcerned = tableC;
	    this.button = new JButton();
	    this.button.setActionCommand(EDIT);
	    this.button.addActionListener(this);
	    this.button.setBorderPainted(false);
	    
	    //Set up the dialog that the button brings up.
	    this.colorChooser = new JColorChooser();
	    this.dialog = JColorChooser.createDialog(button,
						     "Pick a Color",
						     true,  //modal
						     this.colorChooser,
						     this,  //OK button handler
						     null); //no CANCEL button handler
	}
	
	/**
	 * Handles events from the editor button and from
	 * the dialog's OK button.
	 */
	public void actionPerformed(ActionEvent e) {
	    if (EDIT.equals(e.getActionCommand())) {
		//The user has clicked the cell, so
		//bring up the dialog.
		this.button.setBackground(currentColor);
		this.colorChooser.setColor(currentColor);
		this.dialog.setVisible(true);
		
		//Make the renderer reappear.
		this.fireEditingStopped();
		
	    } else { //User pressed dialog's "OK" button.
		this.currentColor = colorChooser.getColor();
		QuantitativeVarFrame.this.setGraphColor(this.tableConcerned, currentColor);
	    }
	}
	
	//Implement the one CellEditor method that AbstractCellEditor doesn't.
	public Object getCellEditorValue() {
	    return this.currentColor;
	}
	
	//Implement the one method defined by TableCellEditor.
	public Component getTableCellEditorComponent(JTable table,
						     Object value,
						     boolean isSelected,
						     int row,
						     int column) {
	    this.currentColor = (Color)value;
	    return this.button;
	}
    }
    
    /**
     * This class is used to display a label with the graph color
     */
    class QuantitativeVarTableCellColorRenderer extends JLabel implements TableCellRenderer {
	Border unselectedBorder = null;
	Border selectedBorder = null;
	boolean isBordered = true;
	
	public QuantitativeVarTableCellColorRenderer(boolean isBordered) {
	    this.isBordered = isBordered;
	    this.setOpaque(true);
	}
	
	public Component getTableCellRendererComponent(JTable table, Object color,
						       boolean isSelected, boolean hasFocus, int row, int column) {
	    
	    Color newColor = (Color)color;
	    this.setBackground(newColor);
	    
	    if (this.isBordered) {
		if (isSelected) {
		    if (this.selectedBorder == null) {
			this.selectedBorder = BorderFactory.createMatteBorder(2,5,2,5,
									      table.getSelectionBackground());
		    }
		    this.setBorder(this.selectedBorder);
		} else {
		    if (this.unselectedBorder == null) {
			this.unselectedBorder = BorderFactory.createMatteBorder(2,5,2,5,
										table.getBackground());
		    }
		    this.setBorder(this.unselectedBorder);
		}
	    }
	    
	    this.setToolTipText("RGB value: " + newColor.getRed() + ", "
				+ newColor.getGreen() + ", " + newColor.getBlue());
	    return this;
	}
    }
    
    /**
     * This class is used to display a specific JButton when the Table is displayed
     */
    class QuantitativeVarTableCellLoadRenderer extends JButton implements TableCellRenderer {
	
	public QuantitativeVarTableCellLoadRenderer() {
	    this.setOpaque(true);
	}
	
	public Component getTableCellRendererComponent(JTable table, Object color,
						       boolean isSelected, boolean hasFocus,
						       int row, int column) {
	    this.setText("Load...");
	    return this;
	}
    }
    
    /**
     * This class is used to loading of data when the user's asks for
     */
    class QuantitativeVarTableCellLoadEditor extends AbstractCellEditor 
	implements TableCellEditor, ActionListener {
	
	private JButton loadButton;
	private JTable tableConcerned;
	
	public QuantitativeVarTableCellLoadEditor(JTable tableC) {
	    this.loadButton = new JButton("Load...");
	    this.loadButton.addActionListener(this);
	    this.tableConcerned = tableC;
	}
	
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
	    return loadButton;
	}
	
	public Object getCellEditorValue() {
	    return loadButton;
	}
	
	public void actionPerformed(ActionEvent e) {
	    QuantitativeVarFrame.this.loadData(this.tableConcerned);
	}
    }
    
    /**
     * This class is used to display a specific JButton when the Table is displayed
     */
    class QuantitativeVarTableCellDeleteRenderer extends JButton implements TableCellRenderer {
	
	public QuantitativeVarTableCellDeleteRenderer() {
	    setOpaque(true);
	}
	
	public Component getTableCellRendererComponent(JTable table, Object color,
						       boolean isSelected, boolean hasFocus,
						       int row, int column) {
	    this.setForeground(Color.red);
	    this.setFont(new Font("Arial", Font.BOLD, 10));
	    this.setText("X");
	    return this;
	}
    }
    
    /**
     * This class is used to asks for removing a graph when the user's asks for
     */
    class QuantitativeVarTableCellDeleteEditor extends AbstractCellEditor 
	implements TableCellEditor, ActionListener {
	
	private JButton deleteButton;
	private JTable tableConcerned;
	
	public QuantitativeVarTableCellDeleteEditor(JTable tableC) {
	    this.deleteButton = new JButton();
	    this.deleteButton.setForeground(Color.red);
	    this.deleteButton.setFont(new Font("Arial", Font.BOLD, 10));
	    this.deleteButton.setText("X");
	    this.deleteButton.addActionListener(this);
	    this.tableConcerned = tableC;
	}
	
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
	    return this.deleteButton;
	}
	
	public Object getCellEditorValue() {
	    return this.deleteButton;
	}
	
	public void actionPerformed(ActionEvent e) {
	    if(!this.tableConcerned.getValueAt(this.tableConcerned.getEditingRow(),0).equals(""))
		QuantitativeVarFrame.this.deleteGraph(this.tableConcerned);
	}
    }
    
    /**
     * This class is used to display a JSlider when the Table is displayed
     */
    class QuantitativeVarTableCellSliderRenderer extends JSlider implements TableCellRenderer {
	private BoundedRangeModel sliderBoundedRangeModel;
	public QuantitativeVarTableCellSliderRenderer(BoundedRangeModel model) {
	    super(model);
	    this.sliderBoundedRangeModel = model;
	}
	
	public Component getTableCellRendererComponent(JTable table, Object color,
						       boolean isSelected, boolean hasFocus,
						       int row, int column) {
	    this.setOrientation(JSlider.HORIZONTAL);
	    this.setMajorTickSpacing( (this.sliderBoundedRangeModel.getMaximum() - this.sliderBoundedRangeModel.getMinimum()) / 4);
	    this.setMinorTickSpacing( (this.sliderBoundedRangeModel.getMaximum() - this.sliderBoundedRangeModel.getMinimum()) / 10);
	    this.setPaintTicks( true );
	    this.setPaintLabels( false );
	    return this;
	}
    }
    
    /**
     * This class is used to modify components associated with the JSlider when
     * it's value changes
     */
    class QuantitativeVarTableCellSliderEditor extends AbstractCellEditor 
	implements TableCellEditor, ChangeListener {
	
	private JSlider frameSizeSlider;
	private JTable tableConcerned;
	private BoundedRangeModel sliderBoundedRangeModel;
	
	public QuantitativeVarTableCellSliderEditor(JTable tableC, BoundedRangeModel model) {
	    this.tableConcerned = tableC;
	    this.sliderBoundedRangeModel = model;
	    this.frameSizeSlider = new JSlider(model);
	    this.frameSizeSlider.setOrientation(JSlider.HORIZONTAL);
	    this.frameSizeSlider.setMajorTickSpacing( (this.sliderBoundedRangeModel.getMaximum() - this.sliderBoundedRangeModel.getMinimum()) / 4);
	    this.frameSizeSlider.setMinorTickSpacing( (this.sliderBoundedRangeModel.getMaximum() - this.sliderBoundedRangeModel.getMinimum()) / 10);
	    this.frameSizeSlider.setPaintTicks( true );
	    this.frameSizeSlider.setPaintLabels( false );
	    this.frameSizeSlider.addChangeListener(this);
	    
	}
	
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
	    return this.frameSizeSlider;
	}
	
	public Object getCellEditorValue() {
	    return this.frameSizeSlider;
	}
	
	public void stateChanged(ChangeEvent e) {
	    this.tableConcerned.setValueAt(new String(new Integer(this.frameSizeSlider.getValue()).toString()),this.tableConcerned.getEditingRow(),4);
	}
    }
    
}
