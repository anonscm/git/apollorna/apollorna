package apollo.gui.tweeker;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import java.util.HashMap;
import java.lang.NumberFormatException;

import java.awt.Dimension;
import java.awt.Color;

import javax.swing.JFileChooser;
import javax.swing.JProgressBar;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JSlider;
import javax.swing.BoundedRangeModel;

import org.apache.log4j.*;

import apollo.gui.ComputeAllScoreCalculator;
import apollo.gui.ReadScoreCalculator;
import apollo.gui.ChargaffScoreCalculator;
import apollo.gui.GCcontentScoreCalculator;
import apollo.gui.NormalizedDinucleotideScoreCalculator;
import apollo.gui.DinucleotideBiasScoreCalculator;
import apollo.gui.KarlinScoreCalculator;
import apollo.gui.ThreadWait;
import apollo.gui.genomemap.StrandedZoomableApolloPanel;
import apollo.gui.genomemap.GraphView;
import apollo.util.IOUtil;

/**
 * This class is designed to handle actions received from the graphical 
 * interface <code>QuantitativeVarFrame</code>
 */
public class QuantitativeVarControler {

    protected final static Logger logger = LogManager.getLogger(QuantitativeVarControler.class);

    private QuantitativeVarFrame quantitativeVarFrame;  /* Reference to the frame */
    private StrandedZoomableApolloPanel sZAP;   /* Reference to Apollo main panel */
    private HashMap calculatorsMap;  /* Collection of ScoreCalculators binded to the interface objects */
    private HashMap multipleVarIndexes;  /* Collection used for renaming variables with same name */
    private final String historyFilePath = 	/* Path of the history file to keep last opened file path */		
	IOUtil.expandSquiggle("~/.apollo") + 
	File.separatorChar + 
	"graphs.history";
    
    /**
     * QuantitativeVarControler constructor
     * @param qVF	frame from which action are received
     * @param szap	Apollo main panel
     */
    public QuantitativeVarControler(QuantitativeVarFrame qVF, StrandedZoomableApolloPanel szap){
	this.quantitativeVarFrame = qVF;
	this.sZAP = szap;
	this.calculatorsMap = new HashMap();
	this.multipleVarIndexes = new HashMap();
    }
    
    /**
     * method that reads a file where scores for each position of the graph are kept
     * @param path	path of the file
     * @return	scores read
     */
    public double[] readFile(String path){
	double[] scores;
	File dataFile = new File(path);
	Vector scoresRead = new Vector();
	
	try{
	    FileReader fileReader = new FileReader(dataFile);
	    BufferedReader bufferedReader = new BufferedReader(fileReader);
	    
	    String dataRead = bufferedReader.readLine();
	    while (dataRead!=null){
		try{
		    scoresRead.add(new Double(Double.parseDouble(dataRead)));
		}catch (NumberFormatException numberFormatException){
		    logger.error("ERROR: Unable to read file: " + path);
		}
		
		dataRead = bufferedReader.readLine();
	    }
	} catch(FileNotFoundException notFoundException){
	    logger.error("ERROR: File not found : " + path);
	} catch (IOException ioException){
	    logger.error("ERROR: Unable to read file: " + path);
	}
	scores = new double[scoresRead.size()];
	for(int i=0;i<scoresRead.size();i++)
	    scores[i] = ((Double)scoresRead.get(i)).doubleValue();
	logger.info("Number of values read: " + scores.length);
	return scores;
    }
    
    /**
     * Creates a new graph
     * @param tableConcerned	graphical table from which the demand has been made
     * @param scores		scores used to display the graph
     * @return	true if the new graph is successfully created, false otherwise
     */
    public boolean createGraphView(JTable tableConcerned, double[] scores){
	ReadScoreCalculator readSC = new ReadScoreCalculator(this.sZAP.getCurationSet(),0,scores);
	this.calculatorsMap.put((String)tableConcerned.getModel().getValueAt(tableConcerned.getEditingRow(),0),readSC);
	this.sZAP.addGraph(readSC, false, Color.BLACK); 
	return true;
    }
    
    /**
     * Change graph's color
     * @param tableConcerned	graphical table from which the demand has been made
     * @param newGraphColor		new color
     * @return	true if color has been successfully changed, false otherwise
     */
     public boolean setGraphColor(JTable tableConcerned, Color newGraphColor) {
	ComputeAllScoreCalculator c  = (ComputeAllScoreCalculator) this.calculatorsMap.get((String)tableConcerned.getValueAt(tableConcerned.getEditingRow(),0));
	for (GraphView g : this.sZAP.getGraphs())
	    if (g.calculator == c) g.setPlotColour(newGraphColor);
	this.sZAP.repaint();
	return true;
    }
   
    /**
     * Show or hide a graph
     * @param tableConcerned	graphical table from which the demand has been made
     * @param state			true - show graph, false - hide graph
     * @return	true if graph has been successfully shown or hidden, false otherwise
     */
    public boolean setGraphVisible(JTable tableConcerned, boolean state) {
	ComputeAllScoreCalculator c  = (ComputeAllScoreCalculator) this.calculatorsMap.get((String)tableConcerned.getValueAt(tableConcerned.getEditingRow(),0));
	for (GraphView g : this.sZAP.getGraphs())
	    if (g.calculator == c) g.setVisible(state);
	return (true);
    }
    
    /**
     * Delete an existing graph
     * @param tableConcerned	graphical table from which the demand has been made
     */
    public void deleteGraphView(JTable tableConcerned) {
	ComputeAllScoreCalculator c  = (ComputeAllScoreCalculator) this.calculatorsMap.get((String)tableConcerned.getValueAt(tableConcerned.getEditingRow(),0));
	this.sZAP.removeGraph(c);
	this.calculatorsMap.remove( (String)tableConcerned.getValueAt(tableConcerned.getEditingRow(),0) );
    }
    
    /**
     * Brings up a Dialog in order to select a file.
     * The default directory comes from an history file, if it exists
     * @return	file selected
     */
    public String browseFiles() {
	JFileChooser jFileChooser;
	String historyPath = null;
	
	if(IOUtil.findFile(this.historyFilePath) == null) {
	    jFileChooser = new JFileChooser();
	} else {
	    try{
		historyPath = IOUtil.readFile(this.historyFilePath);
	    } catch (IOException ioException) {
		logger.error("ERROR: Unable to read history file.");
	    };
	    jFileChooser = new JFileChooser(historyPath.trim());
	}			
	
	int jFileChooserAction = jFileChooser.showOpenDialog(null);
	
	if (jFileChooserAction == JFileChooser.APPROVE_OPTION){
	    return jFileChooser.getSelectedFile().getAbsolutePath();
	} else 
	    return null;
    }
    
    /**
     * method that load external data for a new graph to be made and make related changes
     * to <code>QuantitativeVarFrame</code>
     * @param tableConcerned	graphical table from which the demand has been made
     */
    public void loadData(JTable tableConcerned) {
	String browsePath = this.browseFiles();
	
	if(!this.quantitativeVarFrame.getGraphsTable().getModel().getValueAt(tableConcerned.getEditingRow(),0).equals("")) {
	    this.deleteGraphView(tableConcerned);
	}
		else {
		    if(browsePath != null) {
			this.quantitativeVarFrame.validate();
			ThreadWait tWait = new ThreadWait(this.quantitativeVarFrame,"Loading data...");
			tWait.start();
			ThreadLoad loadThread = new ThreadLoad(this,browsePath,tWait, tableConcerned);
			loadThread.start();
			
			this.setVariableName(browsePath, tableConcerned.getEditingRow());
			this.writeHistoryFile(browsePath);
		    }
		}
    }
    
    /**
     * Sets the variable name on <code>QuantitativeVarFrame</code>
     * @param browsePath	file path
     * @param indexGraph	index of graph concerned
     */
    public void setVariableName(String browsePath, int indexGraph) {
	String varName = "";
	int value;
	
	if(browsePath.indexOf(File.separatorChar) != -1)
	    varName = browsePath.substring(browsePath.lastIndexOf(File.separatorChar)+1);
	else
	    varName = browsePath;
	
	if(this.isVariableAlreadyLoaded(varName)) {
	    value = ((Integer)this.multipleVarIndexes.get(varName)).intValue();
	    varName = varName + "(" + value + ")";
	}
	
	this.quantitativeVarFrame.getGraphsTable().getModel().setValueAt(varName, indexGraph,0);
    }
    
    /**
     * Tests is a variable has already be loaded. If true, then it will changed number associated
     * with it. If not, it will create a new entry in attribute <code>multipleVarIndexes</code>
     * @param varName	variable name
     * @return	true if variable is already present, false otherwise
     */
    public boolean isVariableAlreadyLoaded(String varName) {
	int value;
	
	if(this.multipleVarIndexes.containsKey(varName)) {
	    value = ((Integer)this.multipleVarIndexes.get(varName)).intValue();
	    value++;
	    this.multipleVarIndexes.put(varName,new Integer(value));
	    return true;
	} else {
	    value = 1;
	    this.multipleVarIndexes.put(varName,new Integer(value));
	    return false;
	}
    }
    
    /**
     * Adds a new graph computed by Apollo
     * @param indexGraph	index of the graph
     */
    public void addComputedGraphView(int indexGraph) {
	BoundedRangeModel sliderModel;
	GraphView graphCreated;

	// Sequence length may be lower than 101 (default window size)
	int default_win_size = this.sZAP.getCurationSet().getHigh()-this.sZAP.getCurationSet().getLow();
	if (default_win_size%2==0) default_win_size+=1;
	default_win_size=Math.min(101,default_win_size);
		
	switch(indexGraph) {
	case 0: GCcontentScoreCalculator gcSC = new GCcontentScoreCalculator(this.sZAP.getCurationSet(),default_win_size,false);
	    this.sZAP.addGraph(gcSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),gcSC);
	    sliderModel = gcSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 1: GCcontentScoreCalculator gc_SDSC = new GCcontentScoreCalculator(this.sZAP.getCurationSet(),default_win_size,true);
	    this.sZAP.addGraph(gc_SDSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),gc_SDSC);
	    sliderModel = gc_SDSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 2: ChargaffScoreCalculator chATSC = new ChargaffScoreCalculator(this.sZAP.getCurationSet(),default_win_size,'A','T');
	    this.sZAP.addGraph(chATSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),chATSC);
	    sliderModel = chATSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 3: ChargaffScoreCalculator chGCSC = new ChargaffScoreCalculator(this.sZAP.getCurationSet(),default_win_size,'G','C');
	    this.sZAP.addGraph(chGCSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),chGCSC);
	    sliderModel = chGCSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 4: NormalizedDinucleotideScoreCalculator nGCSC = new NormalizedDinucleotideScoreCalculator(this.sZAP.getCurationSet(),default_win_size,'G','C');
	    this.sZAP.addGraph(nGCSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),nGCSC);
	    sliderModel = nGCSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 5: NormalizedDinucleotideScoreCalculator nCGSC = new NormalizedDinucleotideScoreCalculator(this.sZAP.getCurationSet(),default_win_size,'C','G');
	    this.sZAP.addGraph(nCGSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),nCGSC);
	    sliderModel = nCGSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 6: NormalizedDinucleotideScoreCalculator nAASC = new NormalizedDinucleotideScoreCalculator(this.sZAP.getCurationSet(),default_win_size,'A','A');
	    this.sZAP.addGraph(nAASC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),nAASC);
	    sliderModel = nAASC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 7: DinucleotideBiasScoreCalculator bGCSC = new DinucleotideBiasScoreCalculator(this.sZAP.getCurationSet(),default_win_size,'G','C');
	    this.sZAP.addGraph(bGCSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),bGCSC);
	    sliderModel = bGCSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 8: DinucleotideBiasScoreCalculator bCGSC = new DinucleotideBiasScoreCalculator(this.sZAP.getCurationSet(),default_win_size,'C','G');
	    this.sZAP.addGraph(bCGSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),bCGSC);
	    sliderModel = bCGSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 9: DinucleotideBiasScoreCalculator bAASC = new DinucleotideBiasScoreCalculator(this.sZAP.getCurationSet(),default_win_size,'G','C');
	    this.sZAP.addGraph(bAASC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),bAASC);
	    sliderModel = bAASC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 10: KarlinScoreCalculator kSC = new KarlinScoreCalculator(this.sZAP.getCurationSet(),default_win_size, false, false);
	    this.sZAP.addGraph(kSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),kSC);
	    sliderModel = kSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 11: KarlinScoreCalculator k_SDSC = new KarlinScoreCalculator(this.sZAP.getCurationSet(),default_win_size, true, false);
	    this.sZAP.addGraph(k_SDSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),k_SDSC);
	    sliderModel = k_SDSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	case 12: KarlinScoreCalculator kGCCGSC = new KarlinScoreCalculator(this.sZAP.getCurationSet(),default_win_size, false, true);
	    this.sZAP.addGraph(kGCCGSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),kGCCGSC);
	    sliderModel = kGCCGSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	default: KarlinScoreCalculator kGCCG_SDSC = new KarlinScoreCalculator(this.sZAP.getCurationSet(),default_win_size, true, true);
	    this.sZAP.addGraph(kGCCG_SDSC,false, Color.BLACK);
	    this.calculatorsMap.put((String)this.quantitativeVarFrame.getComputedTable().getModel().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0),kGCCG_SDSC);
	    sliderModel = kGCCG_SDSC.getModel();
	    this.initSlider(sliderModel);
	    break;
	}
    }
    
    /**
     * Delete an existing graph computed by Apollo
     * @param indexGraph	index of the graph
     */
    public void removeComputedGraphView() {
	
	ComputeAllScoreCalculator c  = (ComputeAllScoreCalculator) this.calculatorsMap.get( (String)this.quantitativeVarFrame.getComputedTable().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0) );
	this.sZAP.removeGraph(c);
	this.calculatorsMap.remove((String)this.quantitativeVarFrame.getComputedTable().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0) );
	this.quantitativeVarFrame.unSetSlider();
    }
    
    /**
     * This method initialises a slider that will be associated with a computed graph
     * @param sliderModel	model on which slider is built
     */
    public void initSlider(BoundedRangeModel sliderModel) {
	JSlider sliderComponent = new JSlider(sliderModel);
	sliderComponent.setOrientation(JSlider.HORIZONTAL);
		sliderComponent.setMajorTickSpacing( (sliderModel.getMaximum() - sliderModel.getMinimum()) / 4);
		sliderComponent.setMinorTickSpacing( (sliderModel.getMaximum() - sliderModel.getMinimum()) / 10);
		sliderComponent.setPaintTicks( true );
		sliderComponent.setPaintLabels( false );
		this.quantitativeVarFrame.setSlider(sliderModel);
    }
    
    /**
     * Method that writes the history file to keep
     * @param path	file path
     */
    public void writeHistoryFile(String path) {
	try{
	    IOUtil.writeFile(this.historyFilePath, path);
	} catch (IOException ioException) {
	    logger.error("ERROR: Unable to save path for history.");
	}		
    }
    
    
    /**
     * Thread that handles data loading
     */
    public class ThreadLoad extends Thread {
	
	private QuantitativeVarControler quantitativeVarControler;
	private String browsePath;
	private ThreadWait tWait;
	private JTable tableConcerned;
	
	/**
	 * ThreadLoad constructor
	 * @param qVC		controler that calls this constructor
	 * @param string	Title of the dialog box
	 * @param tWait		ThreadWait to display the progress bar
	 * @param tableC	graphical table from which the demand has been made
	 */
	public ThreadLoad(QuantitativeVarControler qVC, String string, ThreadWait tWait, JTable tableC) {
	    this.tWait = tWait;
	    this.quantitativeVarControler = qVC;
	    this.browsePath = string;
	    this.tableConcerned = tableC;
	}
	
	/**
	 * Method thats run the thread and creates a graph with data loaded
	 */
	public void run() {
	    double[] scoresRead = this.quantitativeVarControler.readFile(this.browsePath);
	    boolean loadDone;
	    tWait.stopWait();
	    loadDone = this.quantitativeVarControler.createGraphView(this.tableConcerned,scoresRead);
	    
	    if(loadDone) {
		this.quantitativeVarControler.quantitativeVarFrame.getGraphsTable().getModel().setValueAt(new Boolean(true),this.tableConcerned.getEditingRow(),1);
		this.quantitativeVarControler.quantitativeVarFrame.getGraphsTable().repaint();
	    } else {
		this.quantitativeVarControler.quantitativeVarFrame.getGraphsTable().getModel().setValueAt(new String(""),this.tableConcerned.getEditingRow(),0);
	    }
	    
	}
    }

  public double  getMean() {
      ComputeAllScoreCalculator c  = (ComputeAllScoreCalculator) this.calculatorsMap.get( (String)this.quantitativeVarFrame.getComputedTable().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0) );
      return c.getMean();
  }

  public double  getMin() {
      ComputeAllScoreCalculator c  = (ComputeAllScoreCalculator) this.calculatorsMap.get( (String)this.quantitativeVarFrame.getComputedTable().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0) );
      return c.getMin();
  }

  public double  getMax() {
      ComputeAllScoreCalculator c  = (ComputeAllScoreCalculator) this.calculatorsMap.get( (String)this.quantitativeVarFrame.getComputedTable().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0) );
      return c.getMax();
  }

    public void updateScores() {
      ComputeAllScoreCalculator c  = (ComputeAllScoreCalculator) this.calculatorsMap.get( (String)this.quantitativeVarFrame.getComputedTable().getValueAt(this.quantitativeVarFrame.getComputedTable().getEditingRow(),0) );
      c.updateScores();
    }


}
