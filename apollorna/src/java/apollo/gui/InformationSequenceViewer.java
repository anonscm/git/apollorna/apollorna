package apollo.gui;

import apollo.config.Config;
import apollo.dataadapter.DataLoadEvent;
import apollo.dataadapter.DataLoadListener;
import apollo.datamodel.RangeI;
import apollo.datamodel.SeqFeatureI;
import apollo.datamodel.SequenceI;
import apollo.editor.AnnotationChangeEvent;
import apollo.editor.AnnotationChangeListener;
import apollo.gui.ControlledObjectI;
import apollo.gui.Controller;
import apollo.gui.Selection;
import apollo.gui.event.FeatureSelectionEvent;
import apollo.gui.event.FeatureSelectionListener;
import apollo.gui.tweeker.QuantitativeVarFrame;
import apollo.seq.io.FastaFile;
import apollo.util.FeatureList;
import apollo.util.NumericKeyFilter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.log4j.*;


/**
 * This class is a window to display several information of a selected
 * sequence, like the GC% or the no-(A,T,C,G) nucleotide. 
 */
public class InformationSequenceViewer extends JFrame 
  implements DataLoadListener
{
  protected final static Logger logger = LogManager.getLogger(InformationSequenceViewer.class);

  private FeatureList features;

  // graphic components
  private JScrollPane   seqScrollPane;
  private JTextArea     seqTextArea;
  private JRadioButton  currentlySelectedButton;
  private JRadioButton  genomicButton;
  private JRadioButton  genomicPlusMinusButton;
  private JTextField    genomicPlusMinusField;
  private JTextField    informationTextField;
  
  private SequenceInformationDisplay sequenceInformationDisplay;
  private JCheckBox followSelectionCheckBox;

  // dynamic objects of the window
  private Selection selection;
  private Controller controller;
  private InformationSequenceController informationSequenceController;

  // static variables about the graphic window apparence
  private static final int defaultGenomicPlusMinus = 20;
  protected static final Color bgColor = new Color(221, 238, 221);
  protected static final Color panelColor = new Color(221, 238, 230);
  private static final Font seqFont = new Font("Courier", 0, 12);
  private static final Font textFieldFont = new Font("Serif", Font.BOLD, 12);

    // global variable used to number the window drawing secondary structure
  private static int lastWindowNo = 0;

  // value of the shift applied on the sequence
  private int genomicPlusMinus = defaultGenomicPlusMinus;
  // to organize different window location
  private int offset = 0;
  // number of the current window: need it to create the name of the secondary
  // structure image
  private int currentWindowNo;
  
  private Dimension informationPanelDimension;
  private int sectionPanelHeigh = 100;

  /**
   * Builder : post-processes the selection and call window intialization 
   * 
   * @param selection   sequence selection
   * @param controller  controller of the main frame
   */
  public InformationSequenceViewer(Selection selection, Controller controller)
  {
    this(selection, controller, 0);
  }

  /**
   * Builder : post-processes the selection and call window intialization
   * 
   * @param selection   sequence selection
   * @param controller  controller of the main frame
   * @param offset      shift to display this window
   */
  public InformationSequenceViewer(Selection selection, Controller controller,
      int offset)
  {
    this.selection = selection;
    this.controller = controller;

    // Offset is for horizontally offsetting new sequence windows so they're
    // not all smack on top of each other.
    this.offset = offset;    
    
    informationSequenceController = new InformationSequenceController(this);

    // If all exons in transcript present, it replaces exons w/ trans
    // so user sees transcript seq not all the exons seq
    FeatureList consolidatedFeatures = selection.getConsolidatedFeatures();
    init(consolidatedFeatures, controller);    
  }

  /**
   * Builder: call window intialization
   * 
   * @param feat        a sequence feature
   * @param controller  controller of the main frame
   */
  public InformationSequenceViewer(SeqFeatureI feat, Controller controller)
  {
    FeatureList f = new FeatureList(1);
    f.addFeature(feat); // Only a single feature - no need to consolidate
    init(f, controller);
  }

  /**
   * intialize GUI, process features
   * 
   * @param features selected features
   * @param controller controller of the main frame
   */
  private void init(FeatureList features, Controller controller)
  {
      // initialization of the number of the current window
      incLastWindowNo();
      this.currentWindowNo = getLastWindowNo();      

      initGui();
      setLocation(offset, 0);
      processFeatures(features);
      // inner class that deals with selection and window control
      new SelSequenceInfoFeatureListener(controller);

      this.pack();
      setVisible(true);
  }

  /**
   * go through all_features feat list and add features to feature list, if gene
   * is found, add its transcripts not the gene. if a feature has result
   * sequence enable result button
   * 
   * @param all_features selected features
   */
  private void processFeatures(FeatureList all_features)
  {
    // use to catch an error
    // when the selected sequence results from a selection
    // with the middle button of the mouse (doesn't result of a clic about a
    // feature)
    // Reference sequence is equal to the selected sequence (Incoherent).
    // So, it's impossible to extend the sequence with the checkbutton
    // A solution is in this case to unabled this checkbutton
    // seqAndRefAreEqual is true if Reference sequence is equal to the selected
    // sequence
    boolean seqAndRefAreEqual = false;
    this.features = new FeatureList();

    for (int i = 0; i < all_features.size(); i++) {
      SeqFeatureI feat = all_features.getFeature(i);
      if ( feat.getRefSequence() != null && 
           feat.getRefSequence().getResidues().compareTo(feat.getResidues()) == 0) {
        seqAndRefAreEqual = true;
      }

      if (feat.isAnnotTop() && feat.hasKids()) {
        Vector transcripts = feat.getFeatures();
        features.addAll(transcripts);
      } else {
        /*
         * whatever it is, add it and deal with it (as long as it has some
         * residues. This is to keep 0-residue start/stop codons from showing
         * up)
         */
        if (feat.getResidues().length() > 0)
          features.addFeature(feat);
      }
    }
    currentlySelectedButton.setSelected(true);

    if (seqAndRefAreEqual == true) {
      // unable the genomic plus minus checkbutton
      genomicPlusMinusButton.setEnabled(false);
      genomicPlusMinusField.setEnabled(false);
    } else {
      genomicPlusMinusButton.setEnabled(true);
      genomicPlusMinusField.setEnabled(true);
    }
    displaySequences();
  }

  /**
   * Method getTitle of the JFrame String that appears in window menus list:
   * name of the sequence if there is only one sequence else the number of
   * sequences
   * 
   * @return name of the sequence or the nb of sequences (if the number is >1)
   */
  public String getTitle()
  {    
      return "Selected Sequence Information";
  }

  /**
   * set up graphical components
   */
  private void initGui()
  {
    int leftWidth = 470; // width of the left part in the window
    int leftHeigh = 370; // width of the left part in the window
    int rightWidth = 400; // width of the RIGHT part in the window
    Dimension textDimension       = new Dimension(rightWidth, 30);
    Dimension buttonBoxDimension  = new Dimension(rightWidth, 40);
    informationPanelDimension = new Dimension(leftWidth, leftHeigh);
   
    sequenceInformationDisplay = new SequenceInformationDisplay(informationPanelDimension);
    
    // sequence
    Dimension scrollpaneDimension = new Dimension(rightWidth, 200);

    seqTextArea = new JTextArea();
    seqTextArea.setFont(seqFont);
    seqTextArea.setEditable(false);
    seqScrollPane = new JScrollPane(seqTextArea);
    seqScrollPane.setMinimumSize(scrollpaneDimension);
    seqScrollPane.setPreferredSize(scrollpaneDimension);
    seqScrollPane.setMaximumSize(scrollpaneDimension);

    // information text field
    informationTextField = new JTextField(" ");
    informationTextField.setBackground(bgColor);
    informationTextField.setFont(textFieldFont);
    informationTextField.setForeground(Color.RED);
    informationTextField.setEditable(false);
    informationTextField.setBorder(null);
    informationTextField.setMinimumSize(textDimension);
    informationTextField.setMaximumSize(textDimension);
    informationTextField.setPreferredSize(textDimension);

    // radio button to select initial sequence
    genomicButton = new JRadioButton("Corresponding genomic sequence");
    genomicButton.setBackground(bgColor);
    genomicButton.setAlignmentX(0.0f);
    genomicButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        displayGenomicSequence();
        currentlySelectedButton = genomicButton;
      }
    });
    // Box which contains genomic radio button
    Box genomicBox = new Box(BoxLayout.X_AXIS);
    genomicBox.add(genomicButton);
    genomicBox.add(Box.createHorizontalGlue());
    genomicBox.setPreferredSize(textDimension);
    genomicBox.setMinimumSize(textDimension);
    genomicBox.setMaximumSize(textDimension);

    // radio button to select initial sequence +/- x bases
    genomicPlusMinusButton = new JRadioButton(
        "Corresponding genomic sequence +/-");
    genomicPlusMinusButton.setBackground(bgColor);
    GenomicPlusMinusListener plusMinusListener = new GenomicPlusMinusListener();
    genomicPlusMinusButton.addActionListener(plusMinusListener);

    // group of the radio buttons to select sequence
    ButtonGroup radioButtonGroup = new ButtonGroup();
    radioButtonGroup.add(genomicButton);
    radioButtonGroup.add(genomicPlusMinusButton);

    // Adds the possibility to choose the genomic range to export
    Dimension fieldDim = new Dimension(5, 20);
    genomicPlusMinusField = new JTextField(Integer.toString(genomicPlusMinus),
        5);
    genomicPlusMinusField.setMinimumSize(fieldDim);
    genomicPlusMinusField.setPreferredSize(fieldDim);
    genomicPlusMinusField.setMaximumSize(fieldDim);
    genomicPlusMinusField.addKeyListener(NumericKeyFilter.getFilter());
    genomicPlusMinusField.addActionListener(plusMinusListener);

    JLabel bases = new JLabel("bases");

    Box genomicPlusMinusBox = new Box(BoxLayout.X_AXIS);
    genomicPlusMinusBox.add(genomicPlusMinusButton);
    genomicPlusMinusBox.add(genomicPlusMinusField);
    genomicPlusMinusBox.add(bases);
    genomicPlusMinusBox.add(Box.createHorizontalGlue());
    genomicPlusMinusBox.setPreferredSize(textDimension);
    genomicPlusMinusBox.setMinimumSize(textDimension);
    genomicPlusMinusBox.setMaximumSize(textDimension);
    // Color of the buttons
    final Color backgroundButtonColor = Color.white;

    // sequence save button
    JButton seqSaveButton = new JButton("Save sequence as...");
    seqSaveButton.setBackground(backgroundButtonColor);
    seqSaveButton.addActionListener(new SeqSaveListener());
    

    // close button
    JButton closeButton = new JButton("Close");
    closeButton.setBackground(backgroundButtonColor);
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });
    // new sequence window button
    JButton newSeqWindow = new JButton("New info window...");
    newSeqWindow.setBackground(backgroundButtonColor);
    newSeqWindow.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        new InformationSequenceViewer(selection, controller, offset + 20);
      }
    });
    
    // box for close button, new info window button and save
    Box closeSeqBox = new Box(BoxLayout.X_AXIS);
    closeSeqBox.add(seqSaveButton);
    closeSeqBox.add(Box.createHorizontalStrut(10));
    closeSeqBox.add(closeButton);
    closeSeqBox.add(Box.createHorizontalStrut(10));
    closeSeqBox.add(newSeqWindow);
    closeSeqBox.add(Box.createHorizontalGlue());
    closeSeqBox.setPreferredSize(buttonBoxDimension);
    closeSeqBox.setMinimumSize(buttonBoxDimension);
    closeSeqBox.setMaximumSize(buttonBoxDimension);

    // follow selection checkbox
    followSelectionCheckBox = new JCheckBox("Follow external selection", false);
    followSelectionCheckBox.setBackground(bgColor);

    // box which contains the checkbox
    Box followHorizontalBox = new Box(BoxLayout.X_AXIS);
    followHorizontalBox.add(Box.createHorizontalGlue());
    followHorizontalBox.add(followSelectionCheckBox);
    followHorizontalBox.add(Box.createHorizontalStrut(30));
    followHorizontalBox.setPreferredSize(buttonBoxDimension);
    followHorizontalBox.setMinimumSize(buttonBoxDimension);
    followHorizontalBox.setMaximumSize(buttonBoxDimension);
      
    // right part of the window
    Container rightContainer = new Container();
    rightContainer.setLayout(new BoxLayout(rightContainer, BoxLayout.Y_AXIS));
    rightContainer.add(Box.createVerticalStrut(10));
    rightContainer.add(seqScrollPane);
    rightContainer.add(informationTextField);
    rightContainer.add(genomicBox);
    rightContainer.add(genomicPlusMinusBox);
    rightContainer.add(closeSeqBox);
    rightContainer.add(followHorizontalBox);
    
    //left part of the window
    Container leftContainer = new Container();
    leftContainer.setLayout(new BoxLayout(leftContainer,BoxLayout.Y_AXIS));
    leftContainer.add(sequenceInformationDisplay.getContentPane());
    leftContainer.setBackground(bgColor);

    // add information (to the left) and right part to the main container
    Container content = this.getContentPane();
    content.setBackground(bgColor);
    content.setLayout(new BoxLayout(content, BoxLayout.X_AXIS));
    content.add(Box.createHorizontalStrut(10));
    content.add(leftContainer);
    content.add(rightContainer);

    currentlySelectedButton = genomicButton;

    // add listeners when the current window is closed
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) { }
    });
    addWindowListener(new WindowAdapter() {
      public void windowClosed(WindowEvent e) { }
    });
  }

  /**
   * call the appropriated method to display sequence
   */
  private void displaySequences()
  {
    currentlySelectedButton.doClick();
    setTitle(getTitle());
  }

  /**
   * display selected sequence 
   */
  private void displayGenomicSequence()
  {
    SeqFeatureI feat;
    String fastas         = "";
    String seq;
    String selectedSequence  = "";
    int feat_count = features.size();
    String[][] otherSequence = null;
    if(feat_count > 1)
    	otherSequence = new String[feat_count][2];

    // compute the selected sequence(s) in the fasta format
    for (int i = 0; i < feat_count; i++) {
      feat = features.getFeature(i);
      seq = feat.getResidues();
      fastas += featureFastaString(seq, feat, "genomic");
      
    }
    // display fasta sequence(s) in the textarea
    seqTextArea.setText(fastas);
    seqTextArea.setCaretPosition(0);
    
    displayComputeResults(feat_count, features,false);
    
  }

  /**
   * display selected sequence +/- x nucleotids 
   */
  private void displayGenomicPlusMinus()
  {
    SeqFeatureI feat;
    SequenceI refSeq;
    int minus;
    int plus;
    String seq;
    String fastas         = "";
    String selectedSequence  = "";
    int feat_count = features.size();
    String[][] otherSequence = null;
    if(feat_count > 1)
    	otherSequence = new String[feat_count][2];

    // compute the selected sequence(s) in the fasta format
    for (int i = 0; i < feat_count; i++) {
      feat    = features.getFeature(i);
      refSeq  = feat.getRefSequence();
      if (feat == null || refSeq == null)
        continue;
      minus = genomicPlusMinus * feat.getStrand(); // hard wired for now
      plus  = genomicPlusMinus * feat.getStrand();
      seq   = refSeq.getResidues(feat.getStart() - minus, feat.getEnd() + plus);
      fastas += featureFastaString(seq, feat, "genomic +/-" + genomicPlusMinus
          + " bases");
      
    }

    // display fasta sequence(s) in the textarea
    seqTextArea.setText(fastas);
    seqTextArea.setCaretPosition(0);  
    
    displayComputeResults(feat_count,features,true);
    
  }

    
  /** 
   * Compute the different information by getting the selection in a feature listand display it
   * @param feat_count the number of feature selected to compute
   * @param features the feature list 
   * @param isGenomicPlusMinus a boolean which indicate if the computation 
   * will be done with a genomic plus/minus
   */
  private void displayComputeResults(int feat_count, FeatureList features, boolean isGenomicPlusMinus)
  {
      informationTextField.setText("");
      sequenceInformationDisplay.resetAllSection();
      sequenceInformationDisplay.manageSectionSize(feat_count);
      
      if(feat_count>0) {
	  if(feat_count>1) {
	      informationTextField.setText("WARNING: several sequences: "+feat_count); }		  
      } else {
	  informationTextField.setText("WARNING: no selected sequence!");
      }	 
	  
      informationSequenceController.computeData(this,features, isGenomicPlusMinus,genomicPlusMinus);
	 
      this.validate();
      this.repaint();
  }
  
  
  /**
   * @return the sequenceInformationDisplay
   */
  public SequenceInformationDisplay getSequenceInformationDisplay() {
  	return sequenceInformationDisplay;
  }

  /**
   * @param sequenceInformationDisplay the sequenceInformationDisplay to set
   */
  public void setSequenceInformationDisplay(SequenceInformationDisplay sequenceInformationDisplay) {
  	this.sequenceInformationDisplay = sequenceInformationDisplay;
  }
  
  

  /**
   * Generate the text contained in the fasta file of the sequence
   * 
   * @param seq   nucleotidic sequence
   * @param feat  feature
   * @param type  type of the sequence
   * @return      text of the fasta file
   */
  private String featureFastaString(String seq, RangeI feat, String type)
  {
    String header = (">" + Config.getDisplayPrefs().getHeader(feat) + " ("
        + type + " sequence): " + seq.length() + " residues");
    header = header + "\n";
    return FastaFile.format(header, seq, 50);
  }

  
  /**
   * Listens for changed in the genomic plus minus range used if the radio
   * button is clicked and when the an action is performed on the text field
   */
  private class GenomicPlusMinusListener 
    implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      String txt = genomicPlusMinusField.getText();
      try {
        genomicPlusMinus = Integer.parseInt(txt);
      }
      catch (NumberFormatException nfe) { // Mostly in case of overflow
        genomicPlusMinus = defaultGenomicPlusMinus;
        genomicPlusMinusField
            .setText(Integer.toString(defaultGenomicPlusMinus));
        String msg = "Could not convert " + txt + "\nto an integer, using "
            + defaultGenomicPlusMinus + " instead";
        JOptionPane.showMessageDialog(InformationSequenceViewer.this, msg,
            "Error", JOptionPane.ERROR_MESSAGE);
      }
      if (currentlySelectedButton != genomicPlusMinusButton) {
        currentlySelectedButton = genomicPlusMinusButton;
        genomicPlusMinusButton.setSelected(true);
      }
      displayGenomicPlusMinus();
    }
  }

  
  /**
   * Listens to sequence save button, brings up save file chooser, and writes
   * file
   */
  private class SeqSaveListener 
    implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      if (features.size() > 0) {
        JFileChooser chooser = new JFileChooser();
        int returnVal = chooser.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File file = chooser.getSelectedFile();
          if (file == null) { // file might be null
            // is this message overkill - should it just exit?
            JOptionPane.showMessageDialog(null, "No file selected");
            return;
          }
          try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(seqTextArea.getText());
            writer.close();
          }
          catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Couldn't write file to " + file);
          }
        }
      }
    }
  } 

  /**
   * Deals with selection listening and being a controlled object to get in
   * controllers window list implements ControlledObjectI so it can be added to
   * Controller, so it show up in Windows menu
   */
  private class SelSequenceInfoFeatureListener
      implements FeatureSelectionListener, AnnotationChangeListener,
      ControlledObjectI
  {
    private Controller controller;

    private SelSequenceInfoFeatureListener(Controller controller)
    {
      setController(controller);
      controller.addListener(this); // adds this inner class as listener
    }

    /**
     * Sets the Controller for the object (ControlledObjectI)
     */
    public void setController(Controller controller)
    {
      this.controller = controller;
      // this is to get the window to show up in the windows menu
    }

    /**
     * Gets the Controller for the object(ControlledObjectI)
     */
    public Controller getController()
    {
      return controller;
    }

    public Object getControllerWindow()
    {
      return InformationSequenceViewer.this;
    }

    /**
     * Whether controller should remove as listener on window closing
     */
    public boolean needsAutoRemoval()
    {
      return true;
    }

    /**
     * FeatureSelectionListener - handle selection. Only deals with selection if
     * this is the most recent instance
     */
    public boolean handleFeatureSelectionEvent(FeatureSelectionEvent e)
    {
      // if followSelectionCheckBox not checked dont handle
      if (!followSelectionCheckBox.isSelected() && !e.forceSelection())
        return false;

      // if all exons of trans present, deletes exons adds trans
      FeatureList consolidatedFeatures = e.getSelection()
          .getConsolidatedFeatures();
      processFeatures(consolidatedFeatures);
      return true;
    }

    public boolean handleAnnotationChangeEvent(AnnotationChangeEvent evt)
    {
      if (evt.isEndOfEditSession()) {
        /*
         * Perhaps it would be better to check the vector and only process if
         * the changed features is something that is visible, but this is much
         * simpler. What really is wrong here is that deletes and additions to
         * the feature is not really dealt with and that should be fixed
         */
        processFeatures(features);
      }
      else if (evt.isDelete()) {
        SeqFeatureI gone = evt.getChangedFeature();
        boolean handled = false;
        for (int i = features.size() - 1; i >= 0 && !handled; i--) {
          handled = features.remove(gone);
        }
      }
      return true;
    }
  }

  /**
   * Region changing means a new data set is being loaded. Get rid of this
   * window. (Not working yet--doesn't seem to get called when region changes.
   * Probably need to add window listener when new SeqWindow is created.) Do we
   * want this? I think sima likes that the seq window lingers.
   */
  public boolean handleDataLoadEvent(DataLoadEvent e)
  {
    this.hide();
    this.dispose();
    controller.removeListener(this);
    return true;
  }
 
  /**
   * get the value of the global variable lastWindowNo (the last number of a
   * window)
   * 
   * @return the value of the global variable lastWindowNo
   */
  public static int getLastWindowNo()
  {
    return lastWindowNo;
  }

  /**
   * increment the global variable lastWindowNo
   */
  public static void incLastWindowNo()
  {
    lastWindowNo = lastWindowNo + 1;
  }

  /**
   * get the number of the current window
   * 
   * @return the number of the current window
   */
  public int getCurrentWindowNo()
  {
    return currentWindowNo;
  }

  /**
   * set the number of the current window
   * 
   * @param currentWindowNo number if the current window
   */
  public void setCurrentWindowNo(int currentWindowNo)
  {
    this.currentWindowNo = currentWindowNo;
  }

  
  /**
   * Method that allow to add a componant to a specific place in a specific container which have a gridBagLayout 
   * @param source the component 
   * @param target the container in which we want to add the component
   * @param constraints the GridBad constrains to use
   * @param x x position in the grig
   * @param y y position in the grig
   * @param w width of the grid
   * @param h height og the grid
   */
  public void addToGridBag(Component source, Container target, GridBagConstraints constraints, int x, int y, int w, int h){
      constraints.gridx = x;
      constraints.gridy = y;
      constraints.gridwidth = w;
      constraints.gridheight = h;
      target.add(source,constraints);
  }
    
	
	
  /**
 * This class create the entire left panel of the main selected sequence information window
 *
 */
 public class SequenceInformationDisplay extends Container{	  
     private Container contentPane;	  

     private JScrollPane group1ScrollPane;
     private JScrollPane group2ScrollPane;
     private JPanel group1Panel;
     private JPanel group2Panel;
     
     private JLabel gcEntireSequenceNameLabel;
     private JLabel entireSequenceLenghtNameLabel;	
     private JLabel nonATCGNucleotideEntireSequenceNameLabel;
     private JLabel intergenicEntireSequenceNameLabel;
     
     private Dimension contentPaneDimension;
     
     private final static int group1ScrollPaneHeigh = 300;
     private final static int group2ScrollPaneHeigh = 100;	  
     private final static int group2PanelHeigh = 75;
     
     JLabel entireSequenceLenghtValueLabel;
     JLabel gcEntireSequenceValueLabel;
     JLabel nonATCGNucleotideEntireSequenceValueLabel;
     JLabel intergenicEntireSequenceValueLabel;
     
     
     public SequenceInformationDisplay(Dimension dimension){
	 contentPaneDimension = dimension;
	 contentPane = new Container();
	 contentPane.setLayout(new BoxLayout(contentPane,BoxLayout.Y_AXIS));
	 contentPane.setPreferredSize(dimension);
	 contentPane.setMaximumSize(dimension);		  
	 contentPane.setBackground(bgColor);
	 
	 group1Panel = new JPanel();
	 group1Panel.setBackground(bgColor);
	 group1Panel.setLayout(new BoxLayout(group1Panel,BoxLayout.Y_AXIS));
	 
	 group1ScrollPane = new JScrollPane(group1Panel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	 group1ScrollPane.setBorder(BorderFactory.createTitledBorder("Selected Sequence information"));
	 group1ScrollPane.setBackground(bgColor);
	 group1ScrollPane.setPreferredSize(new Dimension(contentPaneDimension.width,group1ScrollPaneHeigh));
	 group1ScrollPane.setMaximumSize(new Dimension(contentPaneDimension.width,group1ScrollPaneHeigh));
	 
	 group2Panel = new JPanel();
	 group2Panel.setBackground(panelColor);		  
	 group2Panel.setPreferredSize(new Dimension(contentPaneDimension.width,group2PanelHeigh));
	 group2Panel.setMaximumSize(new Dimension(contentPaneDimension.width,group2PanelHeigh));
	 group2Panel.setLayout(new GridBagLayout());
	 
	 group2ScrollPane = new JScrollPane(group2Panel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	 group2ScrollPane.setBackground(bgColor);
	 group2ScrollPane.setBorder(BorderFactory.createTitledBorder("Entire Sequence information"));
	 group2ScrollPane.setPreferredSize(new Dimension(contentPaneDimension.width,group2ScrollPaneHeigh));		  
	 group2ScrollPane.setMaximumSize(new Dimension(contentPaneDimension.width,group2ScrollPaneHeigh));
	 group2ScrollPane.setMinimumSize(new Dimension(contentPaneDimension.width,group2ScrollPaneHeigh));
	 
	 
	 //group 2
	 //components construction
	 gcEntireSequenceNameLabel = new JLabel("GC% : ");		  
	 entireSequenceLenghtNameLabel = new JLabel("Sequence Lenght : ");
	 nonATCGNucleotideEntireSequenceNameLabel = new JLabel("Non A,T,C,G : ");
	 intergenicEntireSequenceNameLabel = new JLabel("Intergenic% : ");
	 
	 gcEntireSequenceNameLabel.setFont(textFieldFont);
	 entireSequenceLenghtNameLabel.setFont(textFieldFont);
	 nonATCGNucleotideEntireSequenceNameLabel.setFont(textFieldFont);
	 intergenicEntireSequenceNameLabel.setFont(textFieldFont);
	 
	 gcEntireSequenceValueLabel = new JLabel("");		  	  
	 entireSequenceLenghtValueLabel = new JLabel("");		  
	 nonATCGNucleotideEntireSequenceValueLabel = new JLabel("");	
	 intergenicEntireSequenceValueLabel = new JLabel("");
	 
	 
	 //addind component to the grid
	 GridBagConstraints constraints = new GridBagConstraints();
	 constraints.fill = GridBagConstraints.NONE;
	 constraints.anchor = GridBagConstraints.WEST;
	 constraints.weightx = 0;
	 constraints.weighty = 0;
	 addToGridBag(gcEntireSequenceNameLabel,group2Panel,constraints,0,0,1, 1);
	 addToGridBag(nonATCGNucleotideEntireSequenceNameLabel,group2Panel,constraints,0,1,1, 1);
	 addToGridBag(entireSequenceLenghtNameLabel,group2Panel,constraints,3,0,1, 1);	
	 addToGridBag(intergenicEntireSequenceNameLabel,group2Panel,constraints,0,2,1, 1);
	 
	 constraints.weighty = 0;
	 constraints.weightx = 100;
	 constraints.anchor = GridBagConstraints.CENTER;
	 addToGridBag(gcEntireSequenceValueLabel,group2Panel,constraints,1,0,1, 1);
	 addToGridBag(nonATCGNucleotideEntireSequenceValueLabel,group2Panel,constraints,1,1,1, 1);
	 addToGridBag(entireSequenceLenghtValueLabel,group2Panel,constraints,4,0,1,1);
	 addToGridBag(intergenicEntireSequenceValueLabel,group2Panel,constraints,1,2,1, 1);
	   
	 //add all panel to the main container
	 contentPane.add(group1ScrollPane);
	 contentPane.add(Box.createVerticalGlue());
	 contentPane.add(group2ScrollPane);
	 contentPane.add(Box.createVerticalStrut(5));
	 
     }	
	
     
     /**
      * @return the conainerPane which contain all the information of the selected sequence
      */
     private Container getContentPane() {
	 return contentPane;
     }
     
     /**
      * Used to add a new JPanel (for a specified selection) in the left top container <br>
      * this JPanel contain all information to be displayed
      * @param selectionName the name of the selection which will be in the border title
      * @param gcValue the gc value
      * @param nonATCGValue the non ATCG value
      * @param intergenicValue the intergenic value
      */
     public void addNewSection(String selectionName,String gcValue,String nonATCGValue, String intergenicValue){
	 NewSectionDisplay newSection = new NewSectionDisplay(selectionName);
	 newSection.setGcValue(gcValue);
	 newSection.setNonATCGValue(nonATCGValue);
	 newSection.setIntergenicValueLabel(intergenicValue);
	 group1Panel.add(newSection);
     }
     
     
     /**
      * Used to refresh the display on the left Top panel
      */
     private void resetAllSection(){
	 group1Panel.removeAll();
     }
     
     /**
      * This method arange the display of the panel which contain the different selection panel
      * by modifying the size of the main JPanel in fonction of how many selection we have<br>
      * if there is too many selection the scrollBar appeared
      * @param featureCount the of actual feature in selection
      */
     private void manageSectionSize(int featureCount){
	 int totalHeigh = featureCount * sectionPanelHeigh;
	 if(totalHeigh == 0){
	     group1ScrollPane.setPreferredSize(new Dimension(0,0));
	     group1ScrollPane.setMaximumSize(new Dimension(0,0));			
	 } else if(totalHeigh<group1ScrollPaneHeigh){
	     group1ScrollPane.setPreferredSize(new Dimension(contentPaneDimension.width,totalHeigh+30));
	     group1ScrollPane.setMaximumSize(new Dimension(contentPaneDimension.width,totalHeigh+30));
	 } else {
	     group1ScrollPane.setPreferredSize(new Dimension(contentPaneDimension.width, group1ScrollPaneHeigh));
	     group1ScrollPane.setMaximumSize(new Dimension(contentPaneDimension.width, group1ScrollPaneHeigh));
	 }
     }
 }  

 
	
/**
 * This class allow to construct a JPanel for a selection
 * all information which can be displayed are present and there is only 
 * to complete them with the right values
 *
 */
private class NewSectionDisplay extends JPanel {
	  
    private JLabel gcNameLabel = new JLabel("GC% : ");
    private JLabel nonATCGNameLabel = new JLabel("Non A,T,C,G : ");
    private JLabel intergenicNameLabel = new JLabel("Intergenic% :");
    
    private JLabel gcValueLabel = new JLabel("");
    private JLabel nonATCGValueLabel = new JLabel("");
    private JLabel intergenicValueLabel = new JLabel("");
    
    
    /**
     * The constructor create the JPanel and set all detail like the layout, the size, the border..
     * 
     * @param selectionName the title which be in the border
     */
    public NewSectionDisplay(String selectionName) {
	super();
	setBackground(panelColor);
	setBorder(BorderFactory.createTitledBorder(selectionName));
	setPreferredSize(new Dimension(informationPanelDimension.width-5,sectionPanelHeigh));
	setMaximumSize(new Dimension(informationPanelDimension.width+10,sectionPanelHeigh));
	setLayout(new GridBagLayout());
	
	gcNameLabel.setFont(textFieldFont);
	nonATCGNameLabel.setFont(textFieldFont);
	intergenicNameLabel.setFont(textFieldFont);
	
	GridBagConstraints constraints = new GridBagConstraints();
	constraints.fill = GridBagConstraints.NONE;
	constraints.anchor = GridBagConstraints.WEST;
	constraints.weightx = 0;
	constraints.weighty = 0;
	addToGridBag(gcNameLabel,this,constraints,0,0,1, 1);
	addToGridBag(nonATCGNameLabel,this,constraints,0,1,1, 1);
	addToGridBag(intergenicNameLabel,this,constraints,0,2,1, 1);
	
	constraints.weighty = 0;
	constraints.weightx = 100;
	constraints.anchor = GridBagConstraints.CENTER;
	addToGridBag(gcValueLabel,this,constraints,1,0,1, 1);
	addToGridBag(nonATCGValueLabel,this,constraints,1,1,1, 1);
	addToGridBag(intergenicValueLabel,this,constraints,1,2,1, 1);
    }
    
    
    /**
     * @param gcValue the gcValue to set
     */
    public void setGcValue(String gcValue) {
	this.gcValueLabel.setText(gcValue);
    }
    
    
    /**
     * @param nonATCGValue the nonATCGValue to set
     */
    public void setNonATCGValue(String nonATCGValue) {
	this.nonATCGValueLabel.setText(nonATCGValue);
    }
    
    
    /**
     * @param intergenicValueLabel the intergenicValue to set
     */
    public void setIntergenicValueLabel(String intergenicValue) {
	this.intergenicValueLabel.setText(intergenicValue);
    }
    
}

}


