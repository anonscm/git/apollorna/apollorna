package apollo.gui;

import java.io.*;
import java.text.DecimalFormat;

import org.apache.log4j.*;

import apollo.datamodel.*;


/* Calculator to compute with a sliding window a score for each position of the entire sequence 
 */
public abstract class ComputeAllScoreCalculator extends  WindowScoreCalculator {

  protected final static Logger logger = LogManager.getLogger(ComputeAllScoreCalculator.class);

  public double [] scores;
  protected double score_min = 0;
  protected double score_max = 0;
  protected double score_mean = 0;
  protected CurationSet curation = null;
  protected int computedWinSize = 0;


  public ComputeAllScoreCalculator(int winSize, CurationSet c) {
    super(winSize);
    curation = c;
  }

  public int [] getXRange() {
    if (curation != null) {
      return new int [] {curation.getLow(), curation.getHigh()};
    } else {
      return new int [] {1,1};
    }
  }

  public int [] getYRange() {
      return new int [] {(int)score_min, (int)Math.ceil(score_max)};
  }

  public double  getMean() {
      return score_mean;
  }

  public double  getMin() {
      return score_min;
  }

  public double  getMax() {
      return score_max;
  }

  public double [] getScoresForPositions(int [] positions) {
      int l = positions.length;
      double [] s = new double [l];
      int low = curation.getLow();

      for (int i=0; i<l; i++)  
	  if (positions[i]<low) s[i]=getMean();  // it happens !?
	  else s[i]= scores[positions[i]-low];
      return s;
  }

    /* put to 0 the scores that are in the interval: mean +/- 2 times standard deviation */
  public void compute2SDcutoff(int l) {
      double cutoff_inf, cutoff_sup;
      double s=0;
      int i;

      for (i=0; i<l; i++) s+= Math.pow(scores[i]-score_mean,2);
      s = Math.sqrt(s/(double)l);
      cutoff_inf = score_mean - 2*s;
      cutoff_sup = score_mean + 2*s;
      score_mean=0;
      score_min=Double.POSITIVE_INFINITY;
      score_max=Double.NEGATIVE_INFINITY;
      for (i=0; i<l; i++) {
	  s=scores[i];
	  if (s>=cutoff_inf && s<=cutoff_sup) s=0;
	  scores[i]=s;
	  score_mean+=s;
	  score_min = Math.min(s, score_min);
	  score_max = Math.max(s, score_max);
      }
      score_mean = score_mean / (double)l;
  }

  /* update scores when window size is changed */
  public abstract void updateScores();
  
  /* for test, write scores in a file (a value per line) */
  public void writeFile (String file_name, int scores_length) {
      try {
         DecimalFormat df = new DecimalFormat();
	 df.setMaximumFractionDigits(2);
	 df.setMinimumFractionDigits(2);
	 FileWriter f = new FileWriter(file_name);
	 for (int i=0; i<scores_length; i++) f.write(df.format(scores[i])+"\n");
	 f.close();
      } catch(Exception e) {
	 logger.error("ERROR: Can not write scores in " + file_name);
      }
  }

}

