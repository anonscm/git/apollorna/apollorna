package apollo.gui.rna;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


/**
 * Class which represents a window to parameter the characters which links two
 * paired nucleotids in the visualization. If the user change one characters, it
 * will be use in all the new visualization of interactions.
 */
public class ConfigureVisuDialog extends JDialog
{
  private static final Color bgColor      = new Color(221, 238, 221);
  private static final Color buttonColor  = Color.white;
  private final Dimension    textfieldDim = new Dimension(35, 25);

  // graphical components
  JTextField auTextField;
  JTextField gcTextField;
  JTextField guTextField;
  JTextField gaTextField;
  JTextField otherTextField;

  /**
   * Initalize the JDialog
   * 
   * @param apolloFrame Parent frame
   */
  public ConfigureVisuDialog(JFrame apolloFrame)
  {
    super(apolloFrame, "Configure interaction visualization", true);

    initGUI(); // initialize graphic interface
    initializeTextField(); // initialize the content of textfields

    this.setVisible(true);
  }

  /**
   * initialize the user interface
   */
  public void initGUI()
  {
    // create the main panel
    JPanel mainPanel    = new JPanel();
    JPanel buttonPanel  = createButtonPanel();
    JPanel changePanel  = createChangePanel();
    JPanel commentPanel = new JPanel();
    JLabel commentLabel = new JLabel(
        "Characters to represent interactions between two nucleotides:");
    commentLabel.setBackground(bgColor);
    commentPanel.add(commentLabel);

    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(commentPanel);
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(changePanel);
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(buttonPanel);
    mainPanel.add(Box.createVerticalStrut(15));

    mainPanel.setBackground(bgColor);
    commentPanel.setBackground(bgColor);
    buttonPanel.setBackground(bgColor);
    changePanel.setBackground(bgColor);

    this.setContentPane(mainPanel);
    this.pack();
  }

  /**
   * Create and return a panel to enter characters which represent link between
   * two paired nucleotids
   * 
   * @return The panel
   */
  public JPanel createChangePanel()
  {
    auTextField     = new JTextField();
    gcTextField     = new JTextField();
    guTextField     = new JTextField();
    gaTextField     = new JTextField();
    otherTextField  = new JTextField();

    JPanel changePanel = new JPanel();
    changePanel.setBackground(bgColor);
    changePanel.setLayout(new GridLayout(5, 2, 10, 5));

    changePanel.add(createCharacterPanel("G-C"));
    changePanel.add(createTextFieldPanel(gcTextField));
    changePanel.add(createCharacterPanel("A-U"));
    changePanel.add(createTextFieldPanel(auTextField));
    changePanel.add(createCharacterPanel("G-U"));
    changePanel.add(createTextFieldPanel(guTextField));
    changePanel.add(createCharacterPanel("G-A"));
    changePanel.add(createTextFieldPanel(gaTextField));
    changePanel.add(createCharacterPanel("other"));
    changePanel.add(createTextFieldPanel(otherTextField));
    return changePanel;
  }

  /**
   * Create and return a panel containing a jlabel on the right side, whose text
   * is value of label
   * 
   * @param label String to display in the JLable
   * @return JPanel containing a jLabel on the right side
   */
  public JPanel createCharacterPanel(String label)
  {
    JPanel characterPanel = new JPanel();
    JLabel characterLabel = new JLabel(label);
    characterPanel.setLayout(new BoxLayout(characterPanel, BoxLayout.X_AXIS));
    characterPanel.add(Box.createHorizontalGlue());
    characterPanel.add(characterLabel);
    characterPanel.setBackground(bgColor);
    characterLabel.setBackground(bgColor);
    return characterPanel;
  }

  /**
   * Create and return a panel containing the jtextfield textField aligned on
   * the left of the window
   * 
   * @param textField The jtextfield included in the jpanel
   * @return JPanel containing the jtextField on the left
   */
  public JPanel createTextFieldPanel(JTextField textField)
  {
    JPanel textFieldPanel = new JPanel();

    textField.setDocument(new CharacterDocument());
    textField.setPreferredSize(textfieldDim);
    textField.setMaximumSize(textfieldDim);

    textFieldPanel.setLayout(new BoxLayout(textFieldPanel, BoxLayout.X_AXIS));
    textFieldPanel.add(textField);
    textFieldPanel.add(Box.createHorizontalGlue());
    textFieldPanel.setBackground(bgColor);

    return textFieldPanel;
  }

  /**
   * creates and returns a jpanel containing two buttons, Cancel and OK OK
   * button calls the save of the characters
   * 
   * @return the created jpanel
   */
  public JPanel createButtonPanel()
  {
    JPanel buttonPanel    = new JPanel();
    JButton cancelButton  = new JButton("Cancel");
    JButton changeButton  = new JButton("OK");

    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.add(cancelButton);
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.add(changeButton);
    buttonPanel.add(Box.createHorizontalStrut(15));

    buttonPanel.setBackground(bgColor);
    cancelButton.setBackground(buttonColor);
    changeButton.setBackground(buttonColor);

    changeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        // save characters
        changeCharacters();
        dispose();
      }
    });

    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    return buttonPanel;
  }

  /**
   * Initalize the textfields with the symbol containing in the class variables
   * of Target class
   */
  public void initializeTextField()
  {
    auTextField.setText(String.valueOf(Target.AU_SYMBOL));
    gcTextField.setText(String.valueOf(Target.GC_SYMBOL));
    guTextField.setText(String.valueOf(Target.GU_SYMBOL));
    gaTextField.setText(String.valueOf(Target.GA_SYMBOL));
    otherTextField.setText(String.valueOf(Target.OTHER_SYMBOL));
  }

  /**
   * class variables of Targets (x_SYMBOL) are symbols to display between 2
   * nucleotids This method changes their value
   */
  public void changeCharacters()
  {
    if (auTextField.getText().compareTo("") == 0) {
      Target.AU_SYMBOL = ' ';
    }
    else {
      Target.AU_SYMBOL = auTextField.getText().charAt(0);
    }

    if (gcTextField.getText().compareTo("") == 0) {
      Target.GC_SYMBOL = ' ';
    }
    else {
      Target.GC_SYMBOL = gcTextField.getText().charAt(0);
    }

    if (guTextField.getText().compareTo("") == 0) {
      Target.GU_SYMBOL = ' ';
    }
    else {
      Target.GU_SYMBOL = guTextField.getText().charAt(0);
    }

    if (gaTextField.getText().compareTo("") == 0) {
      Target.GA_SYMBOL = ' ';
    }
    else {
      Target.GA_SYMBOL = gaTextField.getText().charAt(0);
    }

    if (otherTextField.getText().compareTo("") == 0) {
      Target.OTHER_SYMBOL = ' ';
    }
    else {
      Target.OTHER_SYMBOL = otherTextField.getText().charAt(0);
    }
  }

  /**
   * Class which represents a document in which only one character is allowed
   */
  class CharacterDocument extends PlainDocument
  {
    public void insertString(int offs, String str, AttributeSet a)
        throws BadLocationException
    {
      if ((this.getLength() + str.length()) <= 1) {
        super.insertString(offs, str, a);
      }
    }
  }
}
