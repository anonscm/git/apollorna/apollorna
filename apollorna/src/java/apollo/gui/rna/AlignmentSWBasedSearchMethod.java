package apollo.gui.rna;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import apollo.datamodel.Range;

/**
 * Class that represents an interaction search method based on the alignment of
 * the two sequences, using the Smith and Waterman algorithm, adapted to search pairs 
 * Code source from JAligner [Ahmed Moustafa, JAligner: Open source Java implementation 
 * of Smith-Waterman, http://jaligner.sourceforge.net] is used.
 */
public class AlignmentSWBasedSearchMethod extends InteractionSearchMethod
{
  float[][] scoringMatrix;  // Scoring matrix defined bu the user
  float gapOpenPenality;    // penality of open gap
  float gapExtensionPenality; // penality of extend gap
  float[][] jalignerScoringMatrix; // Scoring matrix used by the method of JAligner
  public static final char[] alphabet = {'A', 'T', 'G', 'C'};
  
  public static final byte STOP     = 0; // Traceback direction stop
  public static final byte LEFT     = 1; // Traceback direction left
  public static final byte DIAGONAL = 2; // Traceback direction diagonal
  public static final byte UP       = 3; // Traceback direction up
  
  public static final char MARKUP_GAP     = ' ';
  public static final char ALIGNMENT_GAP  = '-';
  public static final char IDENTITY       = '|'; // Markup line identity character
  public static final char SIMILARITY     = ':'; // Markup line similarity character
  public static final char MISMATCH       = '.'; // Markup line mismatch character


  public AlignmentSWBasedSearchMethod(ArrayList param)
  {
    super("Alignment based search method");
    //  get the parameters of the method
    scoringMatrix         = (float [][]) param.get(0);
    gapOpenPenality       = ((Float) param.get(1)).floatValue();
    gapExtensionPenality  = ((Float) param.get(2)).floatValue();
    nCandidates           = ((Integer) param.get(3)).intValue();
    
    // create the JAligner matrix which get the score according to pairs
    jalignerScoringMatrix = getJAlignerMatrix();
    
  }

  /* Search the targets of seq in the list regions
   * @see apollo.gui.rna.InteractionSearchMethod#interactionSearch
   * (java.lang.String, java.util.ArrayList)
   */
  public LinkedList interactionSearch(String seq, ArrayList regions)
  {
    LinkedList  targets = null; // list of targets
    Target target;  // a target
    Range  aRegion; // one region
    float  thresholdScore; // lowest score of the found targets
    // reversed request sequence 
    String revSeq = String.valueOf(reverse(seq.toCharArray(), seq.length()));
    targets = new LinkedList(); 
      
    // for each region
    for (int i = 0; i < regions.size(); i++) {
      aRegion = (Range)regions.get(i);
      if (targets.size() > 0) {
        thresholdScore = ((Target) targets.getLast()).getMethodVariableValue();
      } 
      else {
        thresholdScore = -1;
      }
      // align the region and the reverse sequence
      target = align(aRegion, revSeq, targets.size(), thresholdScore);
      
      if (target != null) {
        // if the max number of targets is found, delete the last target
        if (targets.size() >= nCandidates) {
          targets.removeLast();
        }
        targets.add(target);
        // sort the list according to the score of the alignment
        Collections.sort(targets, new MethodVarTargetComparator());
      }
    }
    
    return targets;
  }
  
  /**
   * Align requestSequence and region and return the found target
   * if the nbFoundTargets is lower than nCandidates, and if nbFoundTargets
   * is equals to nCandidates, return the target if its score is higher 
   * than thresholdScore. Else, return an emply target
   * 
   * @param region          The region where searching a target
   * @param requestSequence The sequence request
   * @param nbFoundTargets  The number of found targets
   * @param thresholdScore  The reference score 
   * 
   * @return                The target of the requestSequence 
   *                        found in the region
   */
  private Target align(Range region, String requestSequence, 
      int nbFoundTargets, float thresholdScore) 
  {
    Target target = null;
    float score; // best score in the matrix
    int m = region.getResidues().length() + 1;
    int n = requestSequence.length() + 1;
    
    byte[] pointers = new byte[m * n];

    // Initializes the boundaries of the traceback matrix to STOP.
    for (int i = 0, k = 0; i < m; i++, k += n) {
      pointers[k] = STOP;
    }
    for (int j = 1; j < n; j++) {
      pointers[j] = STOP;
    }

    short[] sizesOfVerticalGaps   = new short[m * n];
    short[] sizesOfHorizontalGaps = new short[m * n];
    
    for (int i = 0, k = 0; i < m; i++, k += n) {
      for (int j = 0; j < n; j++) {
        sizesOfVerticalGaps[k + j] = sizesOfHorizontalGaps[k + j] = 1;
      }
    }
    
    // construct the matrix usefull to find the best alignment
    Cell cell = constructAlignment(region.getResidues(), requestSequence,
        pointers, sizesOfVerticalGaps, sizesOfHorizontalGaps);
    // get the best score of the matrix
    score = cell.getScore();
    
    // if the found target is interesting
    if ( (nbFoundTargets < nCandidates) || (score > thresholdScore)) {
      target = tracebackAlignment(region, requestSequence, pointers, cell, 
          sizesOfVerticalGaps, sizesOfHorizontalGaps);
    }
    
    return target;
  }

  
  /**
   * Construct matrix usefull to create alignement 
   * and find the cell with the best score
   * 
   * @param seq1                  First sequence to align
   * @param seq2                  Second sequence to align
   * @param pointers              Array of directions
   * @param sizesOfVerticalGaps   Array of sizes of vertical gaps
   * @param sizesOfHorizontalGaps Array of sizes of horizontal gaps
   * @return                      The cell of the matrix with the best score
   */
  private Cell constructAlignment(String seq1, String seq2, 
      byte[] pointers, short[] sizesOfVerticalGaps,
      short[] sizesOfHorizontalGaps)
  {
    char[] a1 = seq1.toCharArray();
    char[] a2 = seq2.toCharArray();
    
    int m = seq1.length() + 1;
    int n = seq2.length() + 1;

    float f; // score of alignment x1...xi to y1...yi if xi aligns to yi
    float[] g = new float[n]; // score if xi aligns to a gap after yi
    float h; // score if yi aligns to a gap after xi
    float[] v = new float[n]; // best score of alignment x1...xi to y1...yi
    float vDiagonal;

    g[0] = Float.NEGATIVE_INFINITY;
    h    = Float.NEGATIVE_INFINITY;
    v[0] = 0;

    for (int j = 1; j < n; j++) {
      g[j] = Float.NEGATIVE_INFINITY;
      v[j] = 0;
    }

    float similarityScore, g1, g2, h1, h2;

    Cell cell = new Cell();

    for (int i = 1, k = n; i < m; i++, k += n) {
      h = Float.NEGATIVE_INFINITY;
      vDiagonal = v[0];
      for (int j = 1, l = k + 1; j < n; j++, l++) {
        similarityScore = jalignerScoringMatrix[a1[i - 1]][a2[j - 1]];

        // Fill the matrices
        f = vDiagonal + similarityScore;

        g1 = g[j] + gapExtensionPenality;
        g2 = v[j] + gapOpenPenality;
        if (g1 > g2) {
          g[j] = g1;
          sizesOfVerticalGaps[l] = (short) (sizesOfVerticalGaps[l - n] + 1);
        } 
        else {
          g[j] = g2;
        }

        h1 = h + gapExtensionPenality;
        h2 = v[j - 1] + gapOpenPenality;
        if (h1 > h2) {
          h = h1;
          sizesOfHorizontalGaps[l] = (short) (sizesOfHorizontalGaps[l - 1] + 1);
        } 
        else {
          h = h2;
        }

        vDiagonal = v[j];
        v[j] = maximum(f, g[j], h, 0);

        // Determine the traceback direction
        if (v[j] == 0) {
          pointers[l] = STOP;
        } 
        else if (v[j] == f) {
          pointers[l] = DIAGONAL;
        } 
        else if (v[j] == g[j]) {
          pointers[l] = UP;
        } 
        else {
          pointers[l] = LEFT;
        }

        // Set the traceback start at the current cell i, j and score
        if (v[j] > cell.getScore()) {
          cell.set(i, j, v[j]);
        }
      }
    }
    
    return cell;
  }

  
  /**
   * Read the array representing the alignment and build the alignment and 
   * the corresponding target
   * 
   * @param region                Aligned region
   * @param requestSeq            Request sequence 
   * @param pointers              Array of directions
   * @param cell                  Cell of start of the alignment
   * @param sizesOfVerticalGaps   Array of sizes of vertical gaps
   * @param sizesOfHorizontalGaps Array of sizes of horizontal gaps
   * @return                      The target which was aligned with seq1
   */
  private Target tracebackAlignment(Range region, String requestSeq, 
      byte[] pointers, Cell cell, short[] sizesOfVerticalGaps,
      short[] sizesOfHorizontalGaps)
  {
    String regionSeq = region.getResidues();
    char[] a1 = regionSeq.toCharArray();
    char[] a2 = requestSeq.toCharArray();
   
    int n = requestSeq.length() + 1;
    int maxlen = regionSeq.length() + requestSeq.length() ; // maximum length after the
                        // aligned sequences

    char[] reversed1 = new char[maxlen]; // reversed sequence #1
    char[] reversed2 = new char[maxlen]; // reversed sequence #2
    char[] reversed3 = new char[maxlen]; // reversed markup

    int len1 = 0; // length of sequence #1 after alignment
    int len2 = 0; // length of sequence #2 after alignment
    int len3 = 0; // length of the markup line

    char c1, c2;

    int i = cell.getRow(); // traceback start row
    int j = cell.getCol(); // traceback start col
    int k = i * n;

    boolean stillGoing = true; // traceback flag: true -> continue & false
                   // -> stop

    while (stillGoing) {
      switch (pointers[k + j]) {
      case UP:
        for (int l = 0, len = sizesOfVerticalGaps[k + j]; l < len; l++) {
          reversed1[len1++] = a1[--i];
          reversed2[len2++] = ALIGNMENT_GAP;
          reversed3[len3++] = MARKUP_GAP;
          k -= n;
        }
        break;
      case DIAGONAL:
        c1 = a1[--i];
        c2 = a2[--j];
        k -= n;
        reversed1[len1++] = c1;
        reversed2[len2++] = c2;
        if (c1 == c2) {
          reversed3[len3++] = IDENTITY;

        } 
        else if (jalignerScoringMatrix[c1][c2] > 0) {
          reversed3[len3++] = SIMILARITY;

        } 
        else {
          reversed3[len3++] = MISMATCH;
        }
        break;
      case LEFT:
        for (int l = 0, len = sizesOfHorizontalGaps[k + j]; l < len; l++) {
          reversed1[len1++] = ALIGNMENT_GAP;
          reversed2[len2++] = a2[--j];
          reversed3[len3++] = MARKUP_GAP;
        }
        break;
      case STOP:
        stillGoing = false;
      }
    }
    
    char[] targetInChar   = reverse(reversed1, len1);
    char[] requestInChar  = reverse(reversed2, len2);
    char[] markupInChar   = reverse(reversed3, len3);
    int[] interaction     = extractInteraction(targetInChar, requestInChar, 
        markupInChar, (j + 1), requestSeq.length());
    // target strand
    byte targetStrand = (byte)region.getStrand();
    // target length
    int targetLength  = (String.copyValueOf(targetInChar)).replaceAll(
        String.valueOf(ALIGNMENT_GAP), "").length();
    // target genomic start position
    int targetGenomicStart;
    
    if (targetStrand == Target.STRAND_MINUS) {
      targetGenomicStart = region.getHigh() - (i + targetLength) + 1;
    }
    else {
      targetGenomicStart = region.getLow() + i;
    }
    // Create the target
    Target target = new Target(interaction, targetGenomicStart,
        targetLength, targetStrand, "Alignment score", cell.getScore()) ;
    
    return target;
  }
  
  /**
   * Extract from the result of Smith and Waterman algorithm the interactions 
   * between the two sequences. Return the integer array 'interaction'. Nucleotid i of 
   * the first sequence is paired with the nucleotids interaction[i] 
   * of the second sequence.
   * 
   * @param alignTarget           The target sequence in the alignment
   * @param alignRequest          The request sequence in the alignment
   * @param alignMarkup           The string containing the alignment symbols.
   * @param alignRequestStartPos  Position of the first aligned nucleotid 
   *                              of the request sequence.
   * @param requestLength         Length of the request sequence
   * 
   * @return                      An integer array
   */
  private int[] extractInteraction(char[] alignTarget, char[] alignRequest, 
      char[] alignMarkup, int alignRequestStartPos, int requestLength)
  {
    int indexRequest;
    int indexTarget;
    int[] interaction = new int[requestLength];
    
    // initialize interaction
    for (int i = 0; i < interaction.length; i++) {
      interaction[i] = -1;
    }
    indexRequest  = requestLength - alignRequestStartPos + 1;
    indexTarget   = -1;
    
    // move all along the strings
    for (int i = 0; i < alignTarget.length; i++) {
      if (alignTarget[i] != ALIGNMENT_GAP) {
        indexTarget ++;
      }
      if (alignRequest[i] != ALIGNMENT_GAP) {
        indexRequest --;
      }
      if (alignMarkup[i] == SIMILARITY || alignMarkup[i] == IDENTITY) {
        interaction[indexRequest] = indexTarget;
      }
    }
    
    return interaction;
  }
  
  
  /* Return a string descibing the method parameter
   * @see apollo.gui.rna.InteractionSearchMethod#getParameterDesc()
   */
  public String getParameterDesc()
  {
    String desc = new String();
    
    // write the matrix
    desc = desc.concat("Matrix: ");
    for (int i = 0 ; i < AlignmentSWBasedMethodPanel.MATRIX_DIM; i++) {
      if (i == 1) {
        desc = desc.concat("\n  ");
      }
      for (int j = 0; j < AlignmentSWBasedMethodPanel.MATRIX_DIM; j++) {
        if ( !(i == 0 && j == 0) ) {
          desc = desc.concat(" ");
        }
        desc = desc.concat(String.valueOf(alphabet[i]) + 
            String.valueOf(alphabet[j]) + ": " + 
            String.valueOf(scoringMatrix[i][j]));
      }
    }
    // write gap penalities
    desc = desc.concat(",\n   Gap opening penality: " + gapOpenPenality);
    desc = desc.concat(", Gap extension penality: " + gapExtensionPenality);
    // write maximum displayed targets
    desc = desc.concat(", Maximum displayed targets: " + nCandidates);
    return desc;
  }
  
  /**
   * Reverses an array of chars
   * 
   * @param a
   * @param len
   * @return the input array of char reserved
   */
  private static char[] reverse(char[] a, int len) 
  {
    char[] b = new char[len];
    for (int i = len - 1, j = 0; i >= 0; i--, j++) {
      b[j] = a[i];
    }
    return b;
  }
  
  /**
   * Returns the maximum of 4 float numbers.
   * 
   * @param a float #1
   * @param b float #2
   * @param c float #3
   * @param d float #4
   * @return  The maximum of a, b, c and d.
   */
  private static float maximum(float a, float b, float c, float d) 
  {
    if (a > b) {
      if (a > c) {
        return a > d ? a : d;
      } 
      else {
        return c > d ? c : d;
      }
    } 
    else if (b > c) {
      return b > d ? b : d;
    } 
    else {
      return c > d ? c : d;
    }
  }
  
  /**
   * Create a matrix which contains the scores of pairs.
   * matrix[i][j] contains the score for the alignment of the character i with j
   * 
   * @return Score matrix used by jAligner based method
   */
  float[][] getJAlignerMatrix()
  {
    final int SIZE = 127;
    float[][] jalignerMatrix = new float[SIZE][SIZE];

    // Initialize the jaligner matrix with the default mismatch value
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        jalignerMatrix[i][j] = AlignmentSWBasedMethodPanel.DEFAULT_MISMATCH_VALUE;
      }
    }
    // Put the value entered by the user
    for (int i = 0; i < AlignmentSWBasedMethodPanel.MATRIX_DIM; i++) {
      for (int j = 0; j < AlignmentSWBasedMethodPanel.MATRIX_DIM; j++) {
        jalignerMatrix[alphabet[i]][alphabet[j]] = scoringMatrix[i][j];
      }
    }

    return jalignerMatrix;
  }
  
  /**
   * A cell in a matrix, to hold row, column and score.
   * Class coming from JAligner
   */

  private class Cell 
  {
    private int   row;    // Row of the cell
    private int   col;    // Column of the cell
    private float score;  // Alignment score at this cell
    
    /**
     * Constructor
     */
    public Cell() 
    {
      super();
      this.row = 0;
      this.col = 0;
      this.score = Float.NEGATIVE_INFINITY;
    }
    /**
     * Return the col of the cell.
     * 
     * @return The col.
     */
    public int getCol() 
    {
      return this.col;
    }
    /**
     * Return the row od the cell.
     * 
     * @return The row.
     */
    public int getRow() 
    {
      return this.row;
    }

    /**
     * Return the score in the cell.
     * 
     * @return The score.
     */
    public float getScore() 
    {
      return this.score;
    }
    
    /**
     * Sets the row, column and score of the cell.
     * 
     * @param row   The row to set.
     * @param col   The col to set.
     * @param score The score to set.
     */
    public void set(int row, int col, float score) 
    {
      this.row    = row;
      this.col    = col;
      this.score  = score;
    }
  }

}
