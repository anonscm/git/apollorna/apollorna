package apollo.gui.rna;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Stack;

import apollo.datamodel.Range;


/**
 * Class that represents an interaction search method based on the folding of
 * the two sequences
 */
public class FoldBasedSearchMethod extends InteractionSearchMethod
{
  // length of the window, shift on regions
  int windowLength; 
  // step of the window between 2 shifts
  int step;
  // string to add between initial sequence and target sequence before 
  // computing RNAfold
  String linker;

  /**
   * Builder
   * 
   * @param param List of parameters: window length, step, linker and the number
   *              of wished targets
   */
  public FoldBasedSearchMethod(ArrayList param)
  {
    super("Fold based method");
    // get the parameters of the method
    windowLength  = ((Integer) param.get(0)).intValue();
    step          = ((Integer) param.get(1)).intValue();
    linker        = (String) param.get(2);
    nCandidates   = ((Integer) param.get(3)).intValue();
  }

  // Search targets which can interact with seq, and which are included in the
  // subsequences, contained in regions. candidateNumber best targets are 
  // return in a linked list.
  // Method:
  // A windows of length windowLength, is shift on each region. Betrween two
  // steps, the window is shifted of step nucleotids.At each step, the 
  // corresponding subsequence is read, and a string is built: 
  // seq+linker+read sequence. RNAfold is computed with this string. 
  // The found free energy is compared with the free energy of the worst
  // target, included in the list. If the new free energy is better, so the
  // old target is removed, and a new one is created and add in the list.
  public LinkedList interactionSearch(String seq, ArrayList regions)
  {
    LinkedList targets = new LinkedList();

    Range aRegion; // one region
    int lengthRegion; // length of a region
    int regionStart; // start position of the region
    int start, stop; // indexes to move about a region
    int regionStrand; // strand of the region
    String residues; // sequence of the region
    String foldInput; // input sequence of RNAfold
    String rnafoldOutput; // output of rnafold
    float rnaFoldfreeEnergy; // free energy of RNAfold
    float interactionFreeEnergy;// free energy of the interaction, computed with
                                // RNAeval
    int targetStart; // start position of the target on the strand +

    int seqLength = seq.length();
    String foldInputStart = seq.concat(linker); // start of the input RNAfold
                                                // sequence
    Comparator methodComp = new MethodVarTargetComparator();

    // for each selected region
    for (int i = 0; i < regions.size(); i++) {
      aRegion       = (Range) regions.get(i);
      residues      = aRegion.getResidues();
      lengthRegion  = residues.length();
      regionStrand  = aRegion.getStrand();
      regionStart   = aRegion.getStart(); // start position of the region.
                                        // WARNING: in the strand -,
      // start position is > to end position!
      // initialize indexes
      start = 0;
      stop  = windowLength;

      // while the window can be shift
      while (stop <= lengthRegion) {
        // call RNAfold
        foldInput     = foldInputStart.concat(residues.substring(start, stop));
        rnafoldOutput = computeRnafold(foldInput);

        // if there is an interaction
        if (existInteraction(rnafoldOutput, seqLength) == true) {
          // get an interaction array
          int[] interaction = extractInteraction(rnafoldOutput, seqLength,
              linker.length());
          // compute Interaction Free Energy with RNAeval
          interactionFreeEnergy = computeInterFreeEnergy(seq, residues
              .substring(start, stop), interaction);

          if ((targets.size() < nCandidates)
              || (interactionFreeEnergy < ((Target) targets.getLast())
                  .getMethodVariableValue())) {
            // if the max number of targets is found, delete the last target
            if (targets.size() >= nCandidates) {
              targets.removeLast();
            }
            // extract the free energy of RNAfold output
            rnaFoldfreeEnergy = extractFreeEnergy(rnafoldOutput);

            if (regionStrand == Target.STRAND_MINUS) {
              targetStart = regionStart - stop + 1;
            }
            else {
              targetStart = regionStart + start;
            }

            // create a new target, add it to the list and sort the list
            Target target = new Target(interaction, targetStart, windowLength,
                (byte) regionStrand, "Interaction free energy",
                interactionFreeEnergy);
            target.setRNAfoldFreeEnergy(rnaFoldfreeEnergy);

            targets.add(target);
            // sort the list according to free energy of the interaction
            Collections.sort(targets, methodComp);
          }
        }
        start = start + step;
        stop  = stop  + step;
      }
    }
    return targets;
  }

  /**
   * Compute RNAfold program with the sequence. Extract the last resulting line,
   * which contains pairs in the parenthesis format, and the free energy
   * 
   * @param sequence  Nucleotidic sequence
   * @return          Last resulting line produced by RNAfold
   */
  private String computeRnafold(String sequence)
  {
    String result = null;
    try {
      File tmpDirectory = new File(apollo.util.IOUtil
          .expandSquiggle("~/.apollo/") + "tmp/");

      // call RNAfold
      Process process = Runtime.getRuntime()
          .exec("RNAfold", null, tmpDirectory);
      OutputStream outputStream = process.getOutputStream();

      // write the sequence
      String command = sequence + "\n";
      outputStream.write(command.getBytes());
      outputStream.flush();

      // write the exit character
      command = "@\n";
      outputStream.write(command.getBytes());
      outputStream.flush();
      outputStream.close();

      result = extractLastLineFromOutput(process);

      // wait the end of the process
      process.waitFor();

    }
    catch (Exception ex) {
      System.err.println("Exception : " + ex.toString());
    }
    return result;
  }

  /**
   * Compute RNAeval and return the free energy
   * 
   * @param seq1        Selected sequence
   * @param seq2        Target sequence
   * @param interaction int array: specify the interactions between 
   *                    the two sequences
   * @return            the free energy of the interaction, computed 
   *                    by RNAeval>. Nucleotid i of the first sequence 
   *                    is paired with the nucleotids array[i] of
   *                    the second sequence.
   */
  public float computeInterFreeEnergy(String seq1, String seq2,
      int[] interaction)
  {
    //  concatenation of the two sequences
    String sequences = seq1 + "&" + seq2; 
    // array of char composed of ., ( and )
    char[] structure = new char[sequences.length()];
    // start position of the second sequence in the Strings
    int seq2Start = seq1.length() + 1; 
                                        
    // build the sequence entry of rnaeval
    for (int i = 0; i < sequences.length(); i++) {
      structure[i] = '.';
    }
    structure[seq2Start - 1] = '&';

    // if there is a pair, add a opened parenthesis and a closed parenthesis
    for (int i = 0; i < interaction.length; i++) {
      if (interaction[i] != -1) {
        structure[i] = '(';
        structure[seq2Start + interaction[i]] = ')';
      }
    }
    // compute the free energy with rnaeval
    float freeEnergy = rnaEvalCompute(sequences, String.copyValueOf(structure));

    return (freeEnergy);
  }

  /**
   * Compute RNAeval program with the sequence, and the associated structure.
   * Extract the free energy from the result
   * 
   * @param sequences String composed of two concatened sequences. Character &
   *                  separates the sequences. Example: AAGGG&CCCGGG
   * @param structure String representing pairs between the two sequences, with
   *                  parenthesis. Character & separates the sequences. Example:
   *                  ..(((.&.)))....
   * @return          Free energy
   */
  public float rnaEvalCompute(String sequences, String structure)
  {
    String lastLine = new String();
    try {
      // call RNAeval
      Process process = Runtime.getRuntime().exec(getRNAevalCommand());
      OutputStream outputStream = process.getOutputStream();

      // write the sequences
      String command = sequences + "\n";
      outputStream.write(command.getBytes());
      outputStream.flush();
      // write the structure
      command = structure + "\n";
      outputStream.write(command.getBytes());
      outputStream.flush();
      // write the exit character
      command = "@\n";
      outputStream.write(command.getBytes());
      outputStream.flush();
      outputStream.close();

      lastLine = extractLastLineFromOutput(process);

      // wait the end of the process
      process.waitFor();

    }
    catch (Exception ex) {
      System.err.println("Exception: " + ex.toString());
    }
    // extract the free energy value
    return (extractFreeEnergy(lastLine));
  }

  /**
   * Extract the last line containing in the process output.
   * 
   * @param process A process. Method read the output of it
   * @return        The last line containing in the output of the process
   */
  public String extractLastLineFromOutput(Process process)
  {
    String lastLine = null;
    try {
      InputStream inputStream = process.getInputStream();
      InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
      BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

      String line = bufferedReader.readLine();
      // extract the last line of the result text
      while (line != null) {
        lastLine  = line;
        line      = bufferedReader.readLine();
      }

      bufferedReader.close();
      inputStreamReader.close();
      inputStream.close();

    }
    catch (Exception ex) {
      System.err.println("Exception : " + ex.toString());
    }
    return lastLine;
  }

  /**
   * Return true if it exists an interaction in the rnafold output: if there are
   * pairs between first sequence and second sequence, so if some parenthesis of
   * the first part of the rnafold output are paired with parenthesis of the
   * second part.
   * 
   * @param rnafoldOutput   RNAfold output string, composed of parenthesis and
   *                        points. Example : ..((......)).
   * @param firstSeqLength  Length of the first sequence
   * @return                True if there is an interaction
   */
  private boolean existInteraction(String rnafoldOutput, int firstSeqLength)
  {
    boolean hasInteraction = false;

    String request = rnafoldOutput.trim().split(" ")[0];
    int nPairs = 0;

    for (int i = 0; i < firstSeqLength; i++) {
      if (request.charAt(i) == '(') 
        nPairs++;
      else if (request.charAt(i) == ')') {
        nPairs--;
      }
    }

    if (nPairs > 0) {
      hasInteraction = true;
    }
    return hasInteraction;
  }

  /**
   * Extract from line the value of the free energy, betwwen parenthesis
   * 
   * @param line  Result line of RNAfold, which contains the interaction between
   *              two sequences. Example: ((((.....)))) ( -4.40)
   * @return      The free energy
   */
  private float extractFreeEnergy(String line)
  {
    String[] words = line.trim().split(" ");
    String freeEnergy = words[words.length - 1];
    freeEnergy = freeEnergy.replaceAll("[()]", "");
    return (Float.parseFloat(freeEnergy));
  }

  /**
   * Build an integer array (interaction) to represent the interaction between
   * two sequences, according to a parenthesis representation of the interaction
   * (RNAfold output format) Nucleotid i of the first sequence is paired with
   * the nucleotids interaction[i] of the second sequence.
   * 
   * @param line          Result line of RNAfold, which contains the interaction 
   *                      between two sequences. Example: ((((.....)))) ( -4.40)
   * @param seq1Length    Length of the first sequence which composed the entry
   *                      sequence of RNAfold
   * @param linkerLength  Length of the linker sequence which composed the entry
   *                      sequence of RNAfold
   * @return              An integer array.
   */
  private int[] extractInteraction(String line, int seq1Length, int linkerLength)
  {
    int[] interaction = new int[seq1Length];
    String request = line.trim().split(" ")[0];
    Stack pairs = new Stack();
    char aChar;
    int firstIndex;

    // initialization of the array
    for (int i = 0; i < seq1Length; i++) {
      interaction[i] = -1;
    }

    // push the value of the nucleotids which are paired in the first sequence
    for (int i = 0; i < seq1Length; i++) {
      aChar = request.charAt(i);
      if (aChar == '(')
        pairs.push(new Integer(i));
      else if (aChar == ')')
        pairs.pop();
    }

    // create pairs
    for (int i = seq1Length + linkerLength; i < request.length(); i++) {
      aChar = request.charAt(i);
      if (aChar == '(') {
        pairs.push(new Integer(i));
      }
      else if (aChar == ')') {
        firstIndex = ((Integer) pairs.pop()).intValue();
        if (firstIndex < seq1Length) {
          interaction[firstIndex] = i - seq1Length - linkerLength;
        }
      }
    }
    return (interaction);
  }

  /**
   * Compute the number of windows which will be explored, according to the
   * serach regions, the window length and the shift step between two windows.
   * 
   * @param regions       Array of the search regions
   * @param windowLength  Length of the window
   * @param step          Shift step netween two step
   * @return              The number of windows which will be explored
   */
  public static int getWindowNumber(ArrayList regions, int windowLength, int step)
  {
    int stop; // index to count windows
    int lengthRegion; // length of a region
    int nWindows = 0; // number of windows
    Range aRegion;

    // for each regions
    for (int i = 0; i < regions.size(); i++) {
      stop = windowLength;
      aRegion = (Range) regions.get(i);
      lengthRegion = aRegion.getResidues().length();
      // while the window can be shift
      while (stop <= lengthRegion) {
        nWindows++;
        stop = stop + step;
      }
    }
    return nWindows;
  }

  public String getParameterDesc()
  {
    String desc = "Window size: " + windowLength + ", shift step: " + step
        + ", linker: " + linker + ", maximum displayed targets: " + nCandidates;
    return desc;
  }

  /**
   * @return command to execute RNAeval
   */
  public static String getRNAevalCommand()
  {
    return "RNAeval";
  }

}
