package apollo.gui.rna;

import java.util.ArrayList;
import java.util.LinkedList;


/**
 * Abstract class, representing a search method for possible interaction between
 * the selected sequence and subsequences in specified regions
 */
public abstract class InteractionSearchMethod
{
  private String methodName; // name of the method
  protected int nCandidates; // number of search best targets

  /**
   * @param name name of the method
   */
  public InteractionSearchMethod(String name)
  {
    methodName = name;
  }

  /**
   * In the subsequences containing in the array regions, search the targets
   * abled to interact with the sequence seq.
   * 
   * @param seq     The request sequence
   * @param regions List of subsequences in which targets are looked for
   * @return        List of targets: null if research wasn't executed, empty 
   *                if no target was found
   */
  abstract public LinkedList interactionSearch(String seq, ArrayList regions);

  /**
   * Return name of the method
   * 
   * @return Name of the method
   */
  public String getMethodName()
  {
    return methodName;
  }

  /**
   * Return description of the method
   * 
   * @return Description of the method
   */
  abstract public String getParameterDesc();
  
}
