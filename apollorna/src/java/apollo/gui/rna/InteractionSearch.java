package apollo.gui.rna;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import apollo.datamodel.CurationSet;
import apollo.datamodel.FeatureSet;
import apollo.datamodel.Range;
import apollo.datamodel.SeqFeature;
import apollo.datamodel.Sequence;
import apollo.datamodel.SequenceI;
import apollo.datamodel.TranslationI;
import apollo.gui.synteny.CurationManager;


/**
 * Class which represents the research manager. Find the search region and call
 * the research method.
 */
public class InteractionSearch
{
  CurationSet curationSet;
  Sequence requestSequence; // request sequence
  InteractionSearchMethod searchMethod; // method to search targets

  int start; // start position of the search regions
  int stop; // stop position of the search regions
  String definedRegions; // defined regions
  byte strand; // strand of the search regions
  int nStartBefore; // number of nucleotides before a start codon
  int nStartAfter; // number of nucleotides after a start codon
  int nStopBefore; // number of nucleotides before a stop codon
  int nStopAfter; // number of nucleotides after a stop codon
  boolean isIntergenic; // if search regions are in intergenic

  ArrayList geneSetForStrandMinus;
  ArrayList geneSetForStrandPlus;

  public InteractionSearch()
  {
    this.requestSequence = null;
    this.curationSet = CurationManager.getCurationManager().getActiveCurState()
        .getCurationSet();
  }

  /**
   * Search targets abled to interact with the request sequence, in the regions
   * specified by the parameters, with the specific method.
   * 
   * @param sequence      Request sequence
   * @param regionParams  List of parameters used to determine the regions where
   *                      search targets
   * @param nMethod       Integer, which specifies the used method
   * @param methodParams  List of parameters of the method
   * @return              List of targets. Empty if no target was found, 
   *                      null if research wasn't executed
   */
  public LinkedList search(Sequence sequence, ArrayList regionParams,
      int nMethod, ArrayList methodParams)
  {
    LinkedList targetList = null; // list of found targets
    ArrayList regionList  = new ArrayList(); // list of regions where search
                                            // targets
    this.requestSequence = sequence;

    // find regions where searching targets
    regionList = regionFind(regionParams);

    // use the appropriate method
    switch (nMethod) {
      case 0: {
        // create the fold based method
        searchMethod = new FoldBasedSearchMethod(methodParams);
        break;
      }
      case 1: {
        // create the alignment based method
        searchMethod = new AlignmentSWBasedSearchMethod(methodParams);
        break;
      }
      default: {
        searchMethod = new FoldBasedSearchMethod(methodParams);
        break;
      }
    }

    // search targets
    targetList = searchMethod.interactionSearch(requestSequence.getResidues(),
        regionList);

    // fill the orders attributes to the targets of the list
    if (targetList != null) {
      fillTargetData(targetList);
    }

    return targetList;
  }

  /**
   * Set the parameters of the research and search regions characterized by the
   * list of parameters on the strand + and/or the strand -.
   * 
   * @param regionParams  List of the parameters characterized the regions
   * @return              List of found regions
   */
  public ArrayList regionFind(ArrayList regionParams)
  {
    ArrayList regionList = new ArrayList();

    // initialize parameters
    initParameters(regionParams);

    // search regions of the strand +
    if ((strand == 0) || (strand == 1)) {
      regionList.addAll(regionFindbyStrand(Target.STRAND_PLUS));
    }

    // search regions of the strand -
    if ((strand == 0) || (strand == 2)) {
      regionList.addAll(regionFindbyStrand(Target.STRAND_MINUS));
    }

    SequenceI genomicSeq = curationSet.getRefSequence();

    // for each selected region, add the reference sequence
    for (int i = 0; i < regionList.size(); i++) {
      ((Range) regionList.get(i)).setRefSequence(genomicSeq);
    }

    return (regionList);
  }

  /**
   * Find regions on a specific strand. According to the parameters, search the
   * intergenic regions, the regions around start or stop codon, included
   * between two genomic positions.
   * 
   * @param currentStrand Strand where search regions
   * @return              List of Range objects.
   */
  public ArrayList regionFindbyStrand(int currentStrand)
  {
    // if isIntergenicRegion[i] == true, the nucleotid i belongs to an
    // intergenic region
    boolean[] isDefinedRegion     = new boolean[stop - start + 1];
    boolean[] isIntergenicRegion  = new boolean[stop - start + 1];
    boolean[] isAroundStartRegion = new boolean[stop - start + 1];
    boolean[] isAroundStopRegion  = new boolean[stop - start + 1];

    for (int i = 0; i < stop - start + 1; i++) {
      isDefinedRegion[i]      = true;
      isIntergenicRegion[i]   = true;
      isAroundStartRegion[i]  = true;
      isAroundStopRegion[i]   = true;
    }

    // extract the genes on this strand
    FeatureSet featSetForStrand = (FeatureSet) curationSet.getAnnots()
        .getFeatSetForStrand(currentStrand);
    if (currentStrand == Target.STRAND_PLUS) {
      geneSetForStrandPlus = getGenes(featSetForStrand);
    }
    else {
      geneSetForStrandMinus = getGenes(featSetForStrand);
    }

    // search the defined regions
    if (definedRegions.compareTo("") != 0) {
      isDefinedRegion = getDefinedRegion();
    }

    // search the intergenic region
    if (isIntergenic == true) {
      isIntergenicRegion = getIntergenic(currentStrand);
    }
    // search the region before or after a start codon
    if ((nStartBefore > 0) || (nStartAfter > 0)) {
      isAroundStartRegion = getRegionAroundCodon(currentStrand, "start");
    }

    // search the region before or after a stop codon
    if ((nStopBefore > 0) || (nStopAfter > 0)) {
      isAroundStopRegion = getRegionAroundCodon(currentStrand, "stop");
    }
    // merge the tables
    // if isSearchRegion[i] == true, the nucleotid i belongs to the searching
    // region
    boolean[] isSearchRegion = new boolean[stop - start + 1];
    for (int i = 0; i < stop - start + 1; i++) {
      isSearchRegion[i] = isDefinedRegion[i] & isIntergenicRegion[i]
          & isAroundStartRegion[i] & isAroundStopRegion[i];
    }

    // build the Range objects
    return (buildRegions(isSearchRegion, currentStrand));
  }

  /**
   * Build an arraylist of searching regions (Range objects). Extract it from
   * the boolean table isSearchRegion. For each continuation of value "true",
   * one region is built and adds to the resulting arrayList.
   * 
   * @param isSearchRegion Table of boolean. isSearchRegion[i] == true means
   *                       that the nucleotid i belongs to the search regions
   * @param currentStrand Strand
   * @return              List of Range objects.
   */
  public ArrayList buildRegions(boolean[] isSearchRegion, int currentStrand)
  {
    ArrayList results = new ArrayList();
    boolean isSearchingStart = true;
    int regionStart = 0;

    // for each nucleotid of the region determined by the genomic positions
    for (int i = 0; i < isSearchRegion.length; i++) {
      // if we are looking for the start of a searching region
      if (isSearchingStart == true) {
        if (isSearchRegion[i] == true) {
          regionStart = start + i;
          isSearchingStart = false;
        }
      }
      // if we're looking for the end of a searching region
      else if (isSearchRegion[i] == false) {
        if (currentStrand == Target.STRAND_PLUS) {
          results.add(new Range(regionStart, start + i - 1));
        }
        else {
          results.add(new Range(start + i - 1, regionStart));
        }
        isSearchingStart = true;
      }
    }

    if (isSearchingStart == false) {
      if (currentStrand == Target.STRAND_PLUS) {
        results.add(new Range(regionStart, start + isSearchRegion.length - 1));
      }
      else {
        results.add(new Range(start + isSearchRegion.length - 1, regionStart));
      }
    }
    return (results);
  }

  /**
   * Search in the list featSet, genes which are included between start and
   * stop, partially or completly Note: in circular genome, some genes starts
   * before the end of the genome and finish after the start of the genome. In
   * Apollo, these genes have the same positions that the genomic sequence so
   * there are not scan.
   * 
   * @param featSet Set of features
   * @return        List of genes which are included between start and stop, 
   *                partially or completly
   */
  public ArrayList getGenes(FeatureSet featSet)
  {
    SeqFeature feat;
    ArrayList geneSet = new ArrayList();
    // range of the genomic sequence
    Range genomicRange = (Range) curationSet.getRefSequence().getRange(); 

    // for each feature on the list
    for (int i = 0; i < featSet.size(); i++) {
      feat = (SeqFeature) featSet.getFeatureAt(i);

      // if the feature is a gene and that it's not a particular case
      if ((feat.getFeatureType().compareTo("gene") == 0)
          && (!((feat.getLow() == genomicRange.getLow()) && (feat.getHigh() == genomicRange
              .getHigh())))) {
        // if the feature is included in the region, add it to the list
        if (   ((feat.getLow() >= start)  && (feat.getLow() <= stop))
            || ((feat.getHigh() >= start) && (feat.getHigh() <= stop))
            || ((feat.getLow() <= start)  && (feat.getHigh() >= stop)) ) {
          geneSet.add(feat);
        }
      }
    }
    return geneSet;
  }

  /**
   * Search the user defined regions
   * 
   * @return An array of boolean, whom length is the size of the region. If
   *         true, the nucleotid at this position in the region belongs to the
   *         user defined region
   */
  public boolean[] getDefinedRegion()
  {
    String[] positions;
    boolean[] isDefinedRegion = new boolean[stop - start + 1];
    int highIndex; // high index in the array of a defined region included
                    // between start and stop
    int lowIndex; // low index in the array, of a defined region included
                  // between start and stop
    String[] regions = definedRegions.split(",");

    // initialize the table with false value
    for (int i = 0; i < isDefinedRegion.length; i++) {
      isDefinedRegion[i] = false;
    }

    // for each region
    for (int i = 0; i < regions.length; i++) {
      // get the positions of the region
      positions = regions[i].split("-");
      lowIndex  = Math.max(Integer.parseInt(positions[0]), start) - start;
      highIndex = Math.min(Integer.parseInt(positions[1]), stop) - start;

      // put true for the nucleotid included between lowIndex and highIndex
      for (int j = lowIndex; j <= highIndex; j++) {
        isDefinedRegion[j] = true;
      }
    }
    return isDefinedRegion;
  }

  /**
   * Search the intergenic regions between start and stop on the strand
   * currentStrand
   * 
   * @param currentStrand The strand where searching intergenic regions
   * @return              An array of boolean, whom length is the size of 
   *                      the region. If true, the nucleotid at this position 
   *                      in the region belongs to an intergenic region
   */
  public boolean[] getIntergenic(int currentStrand)
  {
    boolean[] isIntergenicRegion = new boolean[stop - start + 1];
    ArrayList geneList = getStrandGeneList(currentStrand);
    TranslationI translation;
    SeqFeature gene;
    int geneHigh; // high position of a gene
    int geneLow; // low position of a gene
    int highIndex; // high index in the array, of a region containing a gene
                    // and included between start and stop
    int lowIndex; // low index in the array, of a region containing a gene and
                  // included between start and stop

    for (int i = 0; i < isIntergenicRegion.length; i++) {
      isIntergenicRegion[i] = true;
    }

    // for each gene of the region
    for (int i = 0; i < geneList.size(); i++) {
      gene = (SeqFeature) geneList.get(i);

      // for each transcript of the gene
      for (int j = 0; j < gene.getFeatures().size(); j++) {
        if (((SeqFeature) gene.getFeatureAt(j)).getFeatureType().compareTo(
            "transcript") == 0) {
          translation = ((SeqFeature) gene.getFeatureAt(j)).getTranslation();

          if (currentStrand == Target.STRAND_PLUS) {
            geneLow  = translation.getTranslationStart();
            geneHigh = translation.getLastBaseOfStopCodon();
          }
          else {
            geneHigh = translation.getTranslationStart();
            geneLow  = translation.getLastBaseOfStopCodon();
          }
          lowIndex  = Math.max(geneLow, start) - start;
          highIndex = Math.min(geneHigh, stop) - start;

          // put false to the region between start and stop, AND included in a
          // gene
          for (int index = lowIndex; index <= highIndex; index++) {
            isIntergenicRegion[index] = false;
          }
        }
      }
    }
    return isIntergenicRegion;
  }

  /**
   * Search the regions around a start (stop) codon, between start and stop on
   * the strand currentStrand
   * 
   * @param currentStrand The strand where searching start(stop) codon
   * @param type          Start or stop
   * @return              an array of boolean, whom length is the size of 
   *                      the region. If true, the nucleotid at this position 
   *                      in the region is around a start(stop) codon
   */
  public boolean[] getRegionAroundCodon(int currentStrand, String type)
  {
    SeqFeature gene;
    TranslationI translation;
    boolean[] isAroundCodonRegion = new boolean[stop - start + 1];
    // array of two int, which contains the limits of the search region 
    // around a codon
    int[] searchLimits; 
    // high index in the array, of a region around codon and included between 
    // start and stop
    int highIndex; 
    // low index in the array, of a region around codon and included between 
    // start and stop
    int lowIndex; 
    int position;
    // genes of the strands
    ArrayList geneList = getStrandGeneList(currentStrand); 

    // for each gene of the region
    for (int g = 0; g < geneList.size(); g++) {
      gene = (SeqFeature) geneList.get(g);

      // for each transcript of the gene
      for (int j = 0; j < gene.getFeatures().size(); j++) {
        if (((SeqFeature) gene.getFeatureAt(j)).getFeatureType().compareTo(
            "transcript") == 0) {
          translation = ((SeqFeature) gene.getFeatureAt(j)).getTranslation();

          // start codon
          if (type == "start") {
            searchLimits = getRegionAroundCodonPosition(translation
                .getTranslationStart(), currentStrand, nStartBefore,
                nStartAfter);
          }
          // stop codon
          else {
            if (currentStrand == Target.STRAND_PLUS) {
              position = translation.getLastBaseOfStopCodon() - 2;
            }
            else {
              position = translation.getLastBaseOfStopCodon() + 2;
            }

            searchLimits = getRegionAroundCodonPosition(position,
                currentStrand, nStopBefore, nStopAfter);
          }
          // compute the low and high indexes of the search region
          lowIndex  = Math.max(start, searchLimits[0]) - start;
          highIndex = Math.min(stop, searchLimits[1])  - start;

          // fill the boolean array aroundCodonRegions
          for (int index = lowIndex; index <= highIndex; index++) {
            isAroundCodonRegion[index] = true;
          }
        }
      }
    }
    return (isAroundCodonRegion);
  }

  /**
   * Get the start and stop positions of a region around a codon, which starts
   * "before" nt before the codon, which stops "after" nt after the codon. The
   * codon starts at "position".
   * 
   * @param position      Start position of the codon
   * @param currentStrand Strand of the codon
   * @param nBefore       The number of nucleotids before the codon
   * @param nAfter        The number of nucleotids after the codon
   * @return              An array composed of two numbers: the start and stop 
   *                      positions of the region
   */
  public int[] getRegionAroundCodonPosition(int position, int currentStrand,
      int nBefore, int nAfter)
  {
    int searchStart, searchEnd; // start/end position of the region around the
                                // codon
    int[] limit = new int[2]; // start and stop positions of the region

    if (currentStrand == Target.STRAND_PLUS) {
      if (nBefore > 0) {
        searchStart = position - nBefore;
      }
      else {
        searchStart = position + 3;
      }
      if (nAfter > 0) {
        searchEnd = position + 2 + nAfter;
      }
      else {
        searchEnd = position - 1;
      }
    }
    // strand minus
    else {
      if (nAfter > 0) {
        searchStart = position - 2 - nAfter;
      }
      else {
        searchStart = position + 1;
      }
      if (nBefore > 0) {
        searchEnd = position + nBefore;
      }
      else {
        searchEnd = position - 3;
      }
    }

    // modify limits of the region, if they aren't included between start and
    // stop
    if (searchEnd > stop) {
      searchEnd = stop;
    }
    if (searchStart < start) {
      searchStart = start;
    }

    limit[0] = searchStart;
    limit[1] = searchEnd;
    return limit;
  }

  /**
   * Return the list of genes according to the strand.
   * 
   * @param strand Strand of the searched genes
   * @return       List of gene according to the strand
   */
  public ArrayList getStrandGeneList(int strand)
  {
    if (strand == Target.STRAND_PLUS) {
      return (geneSetForStrandPlus);
    }
    return (geneSetForStrandMinus);
  }

  /**
   * Initialize parameters, whose values are included in the array regionParams.
   * 
   * @param regionParams List of parameters
   */
  private void initParameters(ArrayList regionParams)
  {
    start           = ((Integer) regionParams.get(0)).intValue();
    stop            = ((Integer) regionParams.get(1)).intValue();
    definedRegions  = (String) regionParams.get(2);
    strand          = ((Byte) regionParams.get(3)).byteValue();
    nStartBefore    = ((Integer) regionParams.get(4)).intValue();
    nStartAfter     = ((Integer) regionParams.get(5)).intValue();
    nStopBefore     = ((Integer) regionParams.get(6)).intValue();
    nStopAfter      = ((Integer) regionParams.get(7)).intValue();
    isIntergenic    = ((Boolean) regionParams.get(8)).booleanValue();
  }

  /**
   * Fill the orders attributes to the targets of the list, and call the
   * research of the neighbor genes of the targets
   * 
   * @param targetList List of targets
   */
  public void fillTargetData(LinkedList targetList)
  {
    // sort the list according to the genomic position and set values of
    // GenomicStartOrder
    Collections.sort(targetList, new GenomicStartTargetComparator());
    for (int i = 0; i < targetList.size(); i++) {
      ((Target) targetList.get(i)).setGenomicStartOrder(i + 1);
    }
    // sort the list according to the method variable
    Collections.sort(targetList, new MethodVarTargetComparator());

    // set values of MethodVariableOrder
    for (int i = 0; i < targetList.size(); i++) {
      ((Target) targetList.get(i)).setMethodVariableOrder(i + 1);
    }

    // search neighbors of each target
    for (int i = 0; i < targetList.size(); i++) {
      ((Target) targetList.get(i)).neighborSearch();
    }
  }

  /**
   * return the minimum allowed size of the selected sequence
   * 
   * @return the minimum allowed size of the selected sequence
   */
  public static int getSizeMin()
  {
    return 5;
  }

  /**
   * return the maximum allowed size of the selected sequence.
   * 
   * @return the maximum allowed size of the selected sequence
   */
  public static int getSizeMax()
  {
    return 100;
  }

  /**
   * Return the request sequence
   * 
   * @return The request sequence
   */
  public Sequence getRequestSequence()
  {
    return requestSequence;
  }

  /**
   * Compute and return the description of the request sequence
   * 
   * @return The description of the request sequence
   */
  public String getRequestSequenceDescription()
  {
    String description = new String("");

    // write the name of the reference sequence
    if ((requestSequence.getName().compareTo("") != 0)
        && (requestSequence.getName().compareTo("no_name") != 0)) {
      description = description.concat(requestSequence.getName() + " ");
    }
    // write the range and the positions of the sequence
    if (requestSequence.getRange() != null) {
      if (requestSequence.getRange().getStrand() == Target.STRAND_MINUS) {
        description = description.concat("(reverse strand) ");
      }
      description = description.concat("from "
          + requestSequence.getRange().getLow() + " to "
          + requestSequence.getRange().getHigh());
    }
    if (description.compareTo("") == 0) {
      description = requestSequence.getResidues();
    }
    return description;
  }
}
