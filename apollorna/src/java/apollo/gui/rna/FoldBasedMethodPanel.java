package apollo.gui.rna;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import apollo.util.GuiUtil;


/**
 * Class which represents the user interface to enter the parameters of the fold
 * based method: the window size, the shift between two steps, the linker and
 * the number of resulting targets
 */
public class FoldBasedMethodPanel extends InteractionSearchMethodPanel
{
  int initSeqlength; // the length of the selected sequence

  // graphical components
  JTextField  winSizeTextField;
  JTextField  shiftTextField;
  JTextField  linkerTextField;
  JTextField  targetNbTextField;

  private static final int WIN_SIZE_MIN = 5;
  private static final int WIN_SIZE_MAX = 100;
  private static final int SHIFT_MIN = 1;
  private static final int SHIFT_MAX = 20;
  private static final int LINKER_SIZE_MIN = 3;
  private static final int LINKER_SIZE_MAX = 10;
  private static final String DEFAULT_LINKER = "NNNNN";
  private static final int WARNING_WINDOWS_NUMBER = 30000;
  private static final int DIFF_PERCENTAGE = 10;

  public FoldBasedMethodPanel(InteractionSearchSetDialog parent)
  {
    super(parent);
    // initialize the selected sequence length
    if (parentDialog.getSelectedSequence() != null) {
      this.initSeqlength = parentDialog.getSelectedSequence().getResidues()
          .length();
    }
    else {
      this.initSeqlength = 0;
    }
    initGUI();
  }

  public void initGUI()
  {
    JPanel paramPanel   = createParamPanel();
    JPanel actionPanel  = createButtonPanel();

    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    this.add(Box.createVerticalStrut(15));
    this.add(paramPanel);
    this.add(Box.createVerticalStrut(15));
    this.add(actionPanel);
    this.add(Box.createVerticalStrut(15));
    this.setBackground(bgColor);

    // Build the window displaying the details about the fold method
    buildInformationDialog(createInfoDocument(), "Info: fold based method");
  }

  /**
   * Create a panel containing parameters fields of fold based method
   * 
   * @return Panel containing parameters fields of fold based method
   */
  public JPanel createParamPanel()
  {
    JPanel paramPanel     = new JPanel();
    JLabel winSizeLabel   = new JLabel("Window size (in nucleotides)");
    JLabel winSizeComment = new JLabel("[" + WIN_SIZE_MIN + " ; " + WIN_SIZE_MAX
        + "]  " + "RECOMMENDED: equal to the request length ");
    JLabel shiftLabel     = new JLabel("Shift step (in nucleotides)");
    JLabel shiftComment   = new JLabel("[" + SHIFT_MIN + " ; " + SHIFT_MAX + "]");
    JLabel linkerLabel    = new JLabel("Linker");
    JLabel linkerComment  = new JLabel("Composed of A, U, T, G, C, N. Length ["
        + LINKER_SIZE_MIN + " ; " + LINKER_SIZE_MAX + "]");
    JLabel targetNbLabel  = new JLabel("Maximum number of predicted targets");
    JLabel targetNbComment = new JLabel("[" + TARGET_NB_MIN + " ; "
        + TARGET_NB_MAX + "]");
    Dimension textfieldDim        = new Dimension(70, 25);
    Dimension linkerTextfieldDim  = new Dimension(90, 25);
    winSizeTextField  = GuiUtil.makeNumericTextField();
    shiftTextField    = GuiUtil.makeNumericTextField();
    linkerTextField   = new JTextField();
    targetNbTextField = GuiUtil.makeNumericTextField();

    // set the size of the textfield
    winSizeTextField.setPreferredSize(textfieldDim);
    shiftTextField.setPreferredSize(textfieldDim);
    linkerTextField.setPreferredSize(linkerTextfieldDim);
    targetNbTextField.setPreferredSize(textfieldDim);
    winSizeTextField.setMaximumSize(textfieldDim);
    shiftTextField.setMaximumSize(textfieldDim);
    linkerTextField.setMaximumSize(linkerTextfieldDim);
    targetNbTextField.setMaximumSize(textfieldDim);

    JPanel sizePanel = new JPanel();
    sizePanel.setLayout(new BoxLayout(sizePanel, BoxLayout.X_AXIS));
    sizePanel.add(winSizeLabel);
    sizePanel.add(Box.createHorizontalStrut(10));
    sizePanel.add(winSizeTextField);
    sizePanel.add(Box.createHorizontalStrut(10));
    sizePanel.add(winSizeComment);

    JPanel shiftPanel = new JPanel();
    shiftPanel.setLayout(new BoxLayout(shiftPanel, BoxLayout.X_AXIS));
    shiftPanel.add(shiftLabel);
    shiftPanel.add(Box.createHorizontalStrut(10));
    shiftPanel.add(shiftTextField);
    shiftPanel.add(Box.createHorizontalStrut(10));
    shiftPanel.add(shiftComment);

    JPanel linkerPanel = new JPanel();
    linkerPanel.setLayout(new BoxLayout(linkerPanel, BoxLayout.X_AXIS));
    linkerPanel.add(linkerLabel);
    linkerPanel.add(Box.createHorizontalStrut(10));
    linkerPanel.add(linkerTextField);
    linkerPanel.add(Box.createHorizontalStrut(10));
    linkerPanel.add(linkerComment);

    JPanel targetNbPanel = new JPanel();
    targetNbPanel.setLayout(new BoxLayout(targetNbPanel, BoxLayout.X_AXIS));
    targetNbPanel.add(targetNbLabel);
    targetNbPanel.add(Box.createHorizontalStrut(10));
    targetNbPanel.add(targetNbTextField);
    targetNbPanel.add(Box.createHorizontalStrut(10));
    targetNbPanel.add(targetNbComment);

    paramPanel.setLayout(new GridLayout(4, 1, 10, 5));
    paramPanel.add(sizePanel);
    paramPanel.add(shiftPanel);
    paramPanel.add(linkerPanel);
    paramPanel.add(targetNbPanel);

    paramPanel.setBackground(bgColor);
    for (int i = 0; i < paramPanel.getComponentCount(); i++) {
      paramPanel.getComponent(i).setBackground(bgColor);
    }
    return paramPanel;
  }

  /**
   * Create a panel containing two buttons "More about fold method": open a
   * window displaying some explanations about the method "Compute number of
   * windows": compute the number of windows according to the parameters
   * 
   * @return A panel composed of the two buttons
   */
  public JPanel createButtonPanel()
  {
    JPanel buttonPanel = new JPanel();
    JButton moreButton = new JButton("Info: fold based method");
    JButton winNumberComputeButton = new JButton(
        "Info: compute number of windows");

    moreButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        aboutDialog.setVisible(true);
      }
    });

    winNumberComputeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        computeWindowNumber();
      }
    });

    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.add(moreButton);
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.add(winNumberComputeButton);
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.setBackground(bgColor);

    return buttonPanel;
  }

  /**
   * Initialize parameters of the method: window size, shift, linker, number of
   * targets
   */
  public void initParameters()
  {
    // window size is by default the length of the selected sequence
    // (or WIN_SIZE_MAX if the selected sequence has a size > WIN_SIZE_MAX)
    if (initSeqlength > WIN_SIZE_MAX) {
      winSizeTextField.setText(new Integer(WIN_SIZE_MAX).toString());
    }
    else if (initSeqlength < WIN_SIZE_MIN) {
      winSizeTextField.setText(new Integer(WIN_SIZE_MIN).toString());
    }
    else {
      winSizeTextField.setText(new Integer(initSeqlength).toString());
    }

    shiftTextField.setText(new Integer(SHIFT_MIN).toString());
    linkerTextField.setText(DEFAULT_LINKER);
    targetNbTextField.setText(DEFAULT_TARGET_NB);
  }

  public boolean checkParameters()
  {
    String message = "";
    boolean isCorrect = true;

    // check that all parameters have a value
    if (   (winSizeTextField.getText().compareTo("")  == 0)
        || (shiftTextField.getText().compareTo("")    == 0)
        || (linkerTextField.getText().compareTo("")   == 0)
        || (targetNbTextField.getText().compareTo("") == 0)) {
      message   = "All method parameters have to have a value.\n";
      isCorrect = false;
    }
    else {
      int windowSize = Integer.parseInt(winSizeTextField.getText());
      // check that window size is included between WIN_SIZE_MIN and
      // WIN_SIZE_MAX
      if ((windowSize < WIN_SIZE_MIN) || (windowSize > WIN_SIZE_MAX)) {
        message = message.concat("Window size has to be included between "
            + WIN_SIZE_MIN + " and " + WIN_SIZE_MAX + ".\n");
        isCorrect = false;
      }
      // check that shift is included between SHIFT_MIN and SHIFT_MAX
      int shift = Integer.parseInt(shiftTextField.getText());
      if ((shift < SHIFT_MIN) || (shift > SHIFT_MAX)) {
        message = message.concat("Shift step has to be included between "
            + SHIFT_MIN + " and " + SHIFT_MAX + ".\n");
        isCorrect = false;
      }
      // check that the number of wished targets is included between
      // TARGET_NB_MIN and TARGET_NB_MAX
      int targetNb = Integer.parseInt(targetNbTextField.getText());
      if ((targetNb < TARGET_NB_MIN) || (targetNb > TARGET_NB_MAX)) {
        message = message
            .concat("The number of targets has to be included between "
                + TARGET_NB_MIN + " and " + TARGET_NB_MAX + ".\n");
        isCorrect = false;
      }
      // check that linker size is included between LINKER_SIZE_MIN and
      // LINKER_SIZE_MAX
      String linker = linkerTextField.getText();
      if ((linker.length() < LINKER_SIZE_MIN)
          || (linker.length() > LINKER_SIZE_MAX)) {
        message = message
            .concat("The size of the linker has to be included between "
                + LINKER_SIZE_MIN + " and " + LINKER_SIZE_MAX + ".\n");
        isCorrect = false;
      }
      // check that linker is composed of letters N, A, T, G, C, U.
      if (linker.matches("[NAUGCT]*") == false) {
        message = message
            .concat("The linker is composed of A, T, U, G, C and N.");
        isCorrect = false;
      }
    }
    // if a parameter is incorrect, display the error
    if (isCorrect == false) {
      JOptionPane.showMessageDialog(this, message, "Error",
          JOptionPane.ERROR_MESSAGE);
    }
    return isCorrect;
  }

  public ArrayList getParameters()
  {
    ArrayList paramList = new ArrayList();
    // add parameters of the fold method
    paramList.add(new Integer(winSizeTextField.getText()));
    paramList.add(new Integer(shiftTextField.getText()));
    paramList.add(linkerTextField.getText());
    paramList.add(new Integer(targetNbTextField.getText()));

    return paramList;
  }

  // Get the number of windows which will be explored, and if this number
  // is taller than WARNING_WINDOWS_NUMBER, ask to the user to validate the
  // research.
  // Compare the window size and the request sequence length. If the difference
  // is upper
  // to DIFF_PERCENTAGE percent, ask confirmation to the user.
  // Return true if the research has to be launch
  // @see apollo.gui.rna.InteractionSearchMethodPanel#allowResearch()
  public boolean allowResearch()
  {
    boolean isAllowed = true;
    String  warning   = new String("");
    int     response;

    // length of the request sequence
    int requestSeqLength  = parentDialog.getRequestSequence().getLength();
    // window size
    int windowSize        = Integer.parseInt(winSizeTextField.getText());
    // check if the difference between the two lengths is upper to
    // DIFF_PERCENTAGE percents
    if (Math.abs(windowSize - requestSeqLength) > requestSeqLength * 1
        / DIFF_PERCENTAGE) {
      warning = "- The window size (" + windowSize
          + " nt) is different from the request sequence length ("
          + requestSeqLength + " nt).\n";
    }

    ArrayList regions = parentDialog.getInteractionSearch().regionFind(
        parentDialog.getRegionsParams());
    // compute the number of explored windows
    int nWindows = FoldBasedSearchMethod.getWindowNumber(regions, Integer
        .parseInt(winSizeTextField.getText()), Integer.parseInt(shiftTextField
        .getText()));
    // if the number of scanned windows will be upper to WARNING_WINDOWS_NUMBER
    if (nWindows > WARNING_WINDOWS_NUMBER) {
      warning = warning
          .concat("- The target research is going to take a long time ("
              + nWindows + " interactions to test).\n");
    }
    // if a warning was found
    if (warning.compareTo("") != 0) {
      response = JOptionPane.showConfirmDialog(null, warning
          + "Launch the search anyway?", "Confirmation",
          JOptionPane.YES_NO_OPTION);
      if (response == JOptionPane.NO_OPTION) {
        isAllowed = false;
      }
    }
    return isAllowed;
  }

  /**
   * Call the compute of the number of shifts of the window on the genome,
   * according to the parameters And displays it in a new widow
   */
  public void computeWindowNumber()
  {
    if (parentDialog.checkParameters() == true) {
      String    message = new String();
      ArrayList regions = parentDialog.getInteractionSearch().regionFind(
          parentDialog.getRegionsParams());

      // compute the number of explored windows
      int nWindows = FoldBasedSearchMethod.getWindowNumber(regions, Integer
          .parseInt(winSizeTextField.getText()), Integer
          .parseInt(shiftTextField.getText()));

      if (nWindows == 1) {
        message = "1 window will be shift.";
      }
      else if (nWindows > 1) {
        message = nWindows + " windows will be shift.";
      }
      else {
        message = "No window will be shift.";
      }
      JOptionPane.showMessageDialog(null, message);
    }
  }


  /* 
   * @see apollo.gui.rna.InteractionSearchMethodPanel#createInfoDocument()
   */
  public StyledDocument createInfoDocument()
  {
    String title    = "Search for RNA-RNA interaction method\n\n";
    String message  = "The general idea is to build a temporary sequence which appends the previously "
        + "selected sequence, a special sequence called linker and a potential target. \n"
        + "This temporary sequence is then folds with RNAfold (www.tbi.univie.ac.at/~ivo/RNA). The corresponding free energy is displayed in the Var2 column of the search result array.\n"
        + "The interactions between the two sequences are extracted. The free energy of the "
        + "interaction is computed with RNAeval (www.tbi.univie.ac.at/~ivo/RNA) \n"
        + "and the interaction is evaluated throught this value. This value of free energy is displayed  in the Var1 column of the search result array.\n\n"
        + "More precisely, the method explores regions of genome with a sliding window. "
        + "At each step, the method considers the value of the interaction as explained \n"
        + "above between the previously selected sequence and the one corresponding to the sliding window. "
        + "Only the better potential targets are stored.\n\n"
        + "The parameters of the methods are:\n"
        + "* the size of the sliding window (between " + WIN_SIZE_MIN + " and " 
        + WIN_SIZE_MAX + " nucleotides)\n"
        + "* the shift of the sliding window at each step (between " + SHIFT_MIN 
        + " and " + SHIFT_MAX + " nucleotides)\n"
        + "* a sequence of nucleotides called the linker. This sequence has to be sufficiently "
        + "long to allow folding (between " + LINKER_SIZE_MIN + " and " + LINKER_SIZE_MAX + " nucleotides, \n"
        + "  and composed of nucleotides that will not be involved in interactions. "
        + "Nucleotide N is good but A, U, G, C, T are also allowed)\n"
        + "* the number of potential targets that are stored (between " 
        + TARGET_NB_MIN + " and " + TARGET_NB_MAX + ").\n\n"
        + "The variable of the method shown in the list of predicted targets is the interaction free energy computed by RNAeval.\n";

    // define styles 
    StyledDocument doc = new DefaultStyledDocument();

    Style defaultStyle = StyleContext.getDefaultStyleContext().getStyle(
        StyleContext.DEFAULT_STYLE);
    StyleConstants.setFontSize(defaultStyle, 13);
    Style titleStyle = doc.addStyle("standard", defaultStyle);
    StyleConstants.setBold(titleStyle, true);
    StyleConstants.setFontSize(titleStyle, 14);

    // write on the document
    try {
      doc.insertString(doc.getLength(), title, titleStyle);
      doc.insertString(doc.getLength(), message, defaultStyle);

    }
    catch (Exception e) {
      System.err.println(e);
    }
    return doc;
  }
}
