package apollo.gui.rna;

import java.util.Comparator;


/**
 * Class which implements Comparator interface Compare two targets, according to
 * the genomic start position.
 */
public class GenomicStartTargetComparator implements Comparator
{
  public int compare(Object o1, Object o2)
  {
    int compareValue = 0;
    int genomicStart1 = ((Target) o1).getGenomicStart();
    int genomicStart2 = ((Target) o2).getGenomicStart();

    if (genomicStart1 > genomicStart2) {
      compareValue = 1;
    }
    else if (genomicStart1 < genomicStart2) {
      compareValue = -1;
    }
    return compareValue;
  }
}
