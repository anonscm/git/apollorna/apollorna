package apollo.gui.rna;

import java.util.Comparator;


/**
 * Class which implements Comparator interface. Compare two targets, according
 * to the value of MethodVariable
 */
public class MethodVarTargetComparator implements Comparator
{
  public int compare(Object o1, Object o2)
  {
    int compareValue = 0;
    float methodeVar1 = Math.abs(((Target) o1).getMethodVariableValue());
    float methodeVar2 = Math.abs(((Target) o2).getMethodVariableValue());
    if (methodeVar1 > methodeVar2) {
      compareValue = -1;
    }
    else if (methodeVar1 < methodeVar2) {
      compareValue = 1;
    }
    return compareValue;
  }
}