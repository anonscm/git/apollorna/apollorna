package apollo.gui.rna;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;


/**
 * Abstract class, representing a panel, displaying fields to enter parameters
 * of a searching interaction method.
 */
public abstract class InteractionSearchMethodPanel extends JPanel
{
  InteractionSearchSetDialog parentDialog; // parent dialog
  JDialog aboutDialog; // window which display the research method principle
  
  protected static final int TARGET_NB_MIN        = 5;
  protected static final int TARGET_NB_MAX        = 1000;
  protected static final String DEFAULT_TARGET_NB = "50";
  protected static final Color bgColor = InteractionSearchSetDialog.panelColor;

  public InteractionSearchMethodPanel(InteractionSearchSetDialog parent)
  {
    this.parentDialog = parent;
  }

  /**
   * initialize the user interface
   */
  abstract public void initGUI();

  /**
   * Initialize parameters
   */
  abstract public void initParameters();

  /**
   * Check that the parameters follow their contraints
   * 
   * @return True if the parameters follow their contraints
   */
  abstract public boolean checkParameters();

  /**
   * Return the list of the parameters
   * 
   * @return the list of the parameters
   */
  abstract public ArrayList getParameters();

  /**
   * Check that constraints are respected, and allow or not the start of the
   * research
   * 
   * @return true if the research is allowed
   */
  public boolean allowResearch()
  {
    return true;
  }
  
  /**
   * Build a document which describes the search interaction method
   * 
   * @return Styled document containing tje description of the search 
   * interaction method
   */
  abstract public StyledDocument createInfoDocument();
  
  /**
   * Build the JDialog which displays the text containing in the doc.
   * 
   * @param doc Document to display in the jdialog
   * @param title Title of the dialog window
   */
  public void buildInformationDialog(StyledDocument doc, String title)
  {
    JPanel    groupPanel  = new JPanel(new BorderLayout());
    JPanel    buttonPanel = new JPanel(true);
    JButton   button      = new JButton("OK");
    JTextPane messagePane = new JTextPane();
    messagePane.setEditable(false);
    messagePane.setBackground(Color.white);
    messagePane.setStyledDocument(doc);

    button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        aboutDialog.setVisible(false);
      }
    });
    buttonPanel.add(button);
    buttonPanel.setBackground(bgColor);
    button.setBackground(InteractionSearchSetDialog.buttonColor);

    groupPanel.add(messagePane, BorderLayout.CENTER);
    groupPanel.add(buttonPanel, BorderLayout.SOUTH);
    groupPanel.setBackground(bgColor);
    aboutDialog = new JDialog(this.parentDialog, title,
        false);
    aboutDialog.getContentPane().add(groupPanel, BorderLayout.CENTER);
    aboutDialog.pack();
  }

}
