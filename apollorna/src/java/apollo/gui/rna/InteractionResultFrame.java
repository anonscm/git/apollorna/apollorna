package apollo.gui.rna;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import apollo.gui.synteny.CurationManager;
import apollo.util.IOUtil;


/**
 * Class which represents a frame displaying the predicted targets.
 */
public class InteractionResultFrame extends JFrame
{
  LinkedList targets; // list of targets
  // manager of the interaction research
  InteractionSearch interactionSearch; 
  //array of InteractionViewFrame objects which are created from this window
  ArrayList views; 
  // table displaying the result targets
  JTable targetTable;
  // dialog which shows explanations about the title of the columns
  InfoDialog columnDocDialog;
  // dialog which shows explanations about the method
  InfoDialog methodDocDialog; 

  private static final Color bgColor      = new Color(221, 238, 221);
  private static final Color buttonColor  = Color.white;
  private static final Color cellColor    = new Color(221, 238, 230);
  // number of the interaction column in the table
  private final int INTERACTION_COLUMN = 2;
  // true if the last opened visualization window is hidden and has to be
  // displayed
  private boolean isHidden = false;

  private String requestSeqText;
  private String searchRegionText;
  private String searchMethodText;

  /**
   * Builder
   * 
   * @param targetList List of targets
   * @param search     Manager of the interaction research
   */
  public InteractionResultFrame(LinkedList targetList, InteractionSearch search)
  {
    super("Predicted targets to the previously selected sequence");
    targets = targetList;
    interactionSearch = search;
    views = new ArrayList();

    requestSeqText    = "Request sequence";
    searchRegionText  = "Regions of search";
    searchMethodText  = "Search method";

    initGUI();
  }

  /**
   * Initialize the user interface
   */
  public void initGUI()
  {
    // create the result table
    createResultTable();
    // create the three panels
    JPanel parameterPanel = createParamPanel();
    JPanel buttonPanel    = createButtonPanel();
    JPanel resultPanel    = createResultPanel();
    // create the main panel
    JPanel mainPanel = new JPanel();

    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    mainPanel.add(parameterPanel);
    mainPanel.add(resultPanel);
    mainPanel.add(buttonPanel);

    this.setContentPane(mainPanel);

    // color all panels
    mainPanel.setBackground(bgColor);
    for (int i = 0; i < mainPanel.getComponentCount(); i++) {
      mainPanel.getComponent(i).setBackground(bgColor);
    }

    // create the documentation window
    columnDocDialog = new InfoDialog(this, "More about column titles",
        createDocument());
    methodDocDialog = null;
    

    addWindowListener(new WindowAdapter() {
      public void windowActivated(WindowEvent e)
      {
        if ((IOUtil.isWindows() == true) && (isHidden == true)) {
          // set visible the last opened visualization window
          ((InteractionViewFrame) views.get(views.size() - 1)).setVisible(true);
          isHidden = false;
        }
      }
    });

    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this.setBackground(bgColor);

    this.pack();
  }

  /**
   * Create and return a panel, containing some information about parameters of
   * the searched interactions
   * 
   * @return A panel containing JLabel, displaying some parameters of the
   *         research
   */
  public JPanel createParamPanel()
  {
    JPanel paramPanel = new JPanel();
    TitledBorder paramBorder = BorderFactory
        .createTitledBorder("Search parameters");

    String seqParam = "   " + requestSeqText + ": "
        + interactionSearch.getRequestSequenceDescription();
    String regionParam = "   " + searchRegionText + ": " + getRegionParam();
    String methodParam = "   " + searchMethodText + ": "
        + interactionSearch.searchMethod.getMethodName() + " ("
        + interactionSearch.searchMethod.getParameterDesc() + ")   ";

    JLabel sequenceLabel  = new JLabel(seqParam);
    JLabel regionLabel    = new JLabel(regionParam);
    
    // create a jtextpane to write the method parameters
    JTextPane methodTextPane = new JTextPane();
    methodTextPane.setEditable(false);
    methodTextPane.setBackground(bgColor);
    
    StyledDocument document = methodTextPane.getStyledDocument();
    Style          newStyle = document.addStyle("courier", 
        StyleContext.getDefaultStyleContext().getStyle(
            StyleContext.DEFAULT_STYLE));
    StyleConstants.setFontSize(newStyle, 12);
    StyleConstants.setBold(newStyle, true);
    try {
      document.insertString(document.getLength(), 
          methodParam, newStyle);
    }
    catch (BadLocationException e1) {
      e1.printStackTrace();
    }
    
    JPanel sequencePanel  = new JPanel();
    JPanel regionPanel    = new JPanel();
    JPanel methodPanel    = new JPanel();
    
    JButton methodInfoButton = new JButton("Info: method");
    methodInfoButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        if (methodDocDialog != null) {
          methodDocDialog.setVisible(true);
        }
      }
    });
    sequencePanel.setLayout(new BoxLayout(sequencePanel, BoxLayout.X_AXIS));
    regionPanel.setLayout(new BoxLayout(regionPanel, BoxLayout.X_AXIS));
    methodPanel.setLayout(new BoxLayout(methodPanel, BoxLayout.X_AXIS));
    // add labels to panels
    sequencePanel.add(sequenceLabel);
    sequencePanel.add(Box.createHorizontalGlue());
    regionPanel.add(regionLabel);
    regionPanel.add(Box.createHorizontalGlue());
    methodPanel.add(methodTextPane);
    methodPanel.add(methodInfoButton);
    methodPanel.add(Box.createHorizontalGlue());

    // set color background
    sequenceLabel.setBackground(bgColor);
    regionLabel.setBackground(bgColor);
    sequencePanel.setBackground(bgColor);
    regionPanel.setBackground(bgColor);
    methodPanel.setBackground(bgColor);

    paramPanel.setBorder(paramBorder);
    paramPanel.setLayout(new BoxLayout(paramPanel, BoxLayout.Y_AXIS));
    // add components to the panel
    paramPanel.add(Box.createVerticalStrut(2));
    paramPanel.add(sequencePanel);
    paramPanel.add(Box.createVerticalStrut(2));
    paramPanel.add(regionPanel);
    paramPanel.add(methodPanel);
    return paramPanel;
  }

  /**
   * Create and return a panel, displaying a button to close the window.
   * 
   * @return A panel, displaying a button to close the window
   */
  public JPanel createButtonPanel()
  {
    JPanel buttonPanel = new JPanel();
    JButton parameterChangeButton = new JButton("Configure visualization");
    JButton saveTextButton        = new JButton("Save to text");
    JButton saveFastaButton       = new JButton("Save to fasta");
    JButton saveGFFButton         = new JButton("Save to GFF");
    JButton closeButton           = new JButton("Close");

    closeButton.setBackground(buttonColor);
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    saveTextButton.setBackground(buttonColor);
    saveTextButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        // save the found interactions in a text file
        saveInFile(getInteractionInText());
      }
    });

    saveFastaButton.setBackground(buttonColor);
    saveFastaButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        // save the found interactions in a fasta file
        saveInFile(getInteractionInFasta());
      }
    });

    saveGFFButton.setBackground(buttonColor);
    saveGFFButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        // save the found interactions in a GFF file
        saveInFile(getInteractionInGFF());
      }
    });

    parameterChangeButton.setBackground(buttonColor);
    parameterChangeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        parameterVisualization();
      }
    });

    buttonPanel.add(parameterChangeButton);
    buttonPanel.add(Box.createHorizontalStrut(4));
    buttonPanel.add(saveTextButton);
    buttonPanel.add(saveFastaButton);
    buttonPanel.add(saveGFFButton);
    buttonPanel.add(Box.createHorizontalStrut(4));
    buttonPanel.add(closeButton);

    return buttonPanel;
  }

  /**
   * Build the jtable, and fill it with the target data
   */
  public void createResultTable()
  {
    targetTable = new JTable(new TargetTableModel());

    // renderer to color the line of the jtable
    for (int i = 0; i < targetTable.getColumnCount(); i++) {
      if (i != INTERACTION_COLUMN) {
        targetTable.getColumnModel().getColumn(i).setCellRenderer(
            new ColoredCellRenderer());
      }
    }
    // put a renderer to the interaction column to display jtextareas in
    // this column
    targetTable.getColumn("Interaction").setCellRenderer(
        new InteractionViewRenderer());

    // put a cell editor to the interaction column to allow user to click on
    // the column
    targetTable.getColumn("Interaction").setCellEditor(
        new InteractionViewEditor());
    // allow the selection of cells
    targetTable.setCellSelectionEnabled(true);
    // apply width and height to the columns of the table
    parameterColumnDimension();
  }

  /**
   * Create the result panel which contains the jtable.
   * 
   * @return Result panel
   */
  public JPanel createResultPanel()
  {
    JPanel resultPanel = new JPanel();
    // add the jtable in a jscrollpane, and the jscrollpane in the result
    // panel
    JScrollPane targetScrollPane = new JScrollPane(targetTable);

    JPanel tablePanel = new JPanel();
    tablePanel.setLayout(new BorderLayout());
    tablePanel.add(targetScrollPane, BorderLayout.CENTER);
    tablePanel.setBackground(bgColor);
    tablePanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

    JPanel buttonPanel = new JPanel();
    JButton docButton = new JButton("Info: columns");

    // add listener about buttons
    docButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        columnDocDialog.setVisible(true);
      }
    });
    buttonPanel.setBackground(bgColor);
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.add(Box.createHorizontalGlue());
    buttonPanel.add(docButton);

    TitledBorder paramBorder = BorderFactory
        .createTitledBorder("Search result: " + targetTable.getRowCount()
            + " found targets");
    resultPanel.setBorder(paramBorder);
    resultPanel.setLayout(new BoxLayout(resultPanel, BoxLayout.Y_AXIS));
    resultPanel.add(buttonPanel);
    resultPanel.add(Box.createVerticalStrut(5));
    resultPanel.add(tablePanel);
    return resultPanel;
  }

  /**
   * Return a string which describes the region parameters.
   * 
   * @return A string which describes the region parameters
   */
  public String getRegionParam()
  {
    String regionParam = "from " + interactionSearch.start + " to "
        + interactionSearch.stop;
    if (interactionSearch.definedRegions.compareTo("") != 0) {
      regionParam = regionParam.concat(" (" + interactionSearch.definedRegions
          + ")");
    }
    regionParam = regionParam.concat(", strand "
        + InteractionSearchSetDialog.strandData[interactionSearch.strand]);
    if (interactionSearch.nStartBefore > 0) {
      regionParam = regionParam.concat(", " + interactionSearch.nStartBefore
          + " nt before start codons");
    }
    if (interactionSearch.nStartAfter > 0) {
      regionParam = regionParam.concat(", " + interactionSearch.nStartAfter
          + " nt after start codons");
    }
    if (interactionSearch.nStopBefore > 0) {
      regionParam = regionParam.concat(", " + interactionSearch.nStopBefore
          + " nt before stop codons");
    }
    if (interactionSearch.nStopAfter > 0) {
      regionParam = regionParam.concat(", " + interactionSearch.nStopAfter
          + " nt after stop codons");
    }
    if (interactionSearch.isIntergenic == true) {
      regionParam = regionParam.concat(", in intergenic regions");
    }
    return regionParam;
  }

  /**
   * Apply specific width and height to the columns of the jtable
   */
  public void parameterColumnDimension()
  {
    TableColumnModel model = targetTable.getColumnModel();
    int[] widths = {70, 30,
        Math.max(getPreferredColumnWidth(INTERACTION_COLUMN), 70), 75, 70, 75,
        90, 75, 55, 40, 40, 40, 40, 40}; // width of the columns

    // set preferred width of the columns
    for (int i = 0; i < targetTable.getColumnCount(); i++) {
      model.getColumn(i).setPreferredWidth(widths[i]);
    }

    // intialize the height of each row of the table
    for (int r = 0; r < targetTable.getRowCount(); r++) {
      // Get the preferred height
      targetTable.setRowHeight(r, Math.min(getPreferredRowHeight(r), 100));
    }
  }

  /**
   * Determine largest cell width in the column colIndex
   * 
   * @param colIndex  Index od the column
   * @return          Largest cell width in the column colIndex
   */
  public int getPreferredColumnWidth(int colIndex)
  {
    int maxWidth = 0;
    int currentWidth;
    TableCellRenderer renderer;
    Component comp;

    targetTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    // Determine largest cell in the column
    for (int r = 0; r < targetTable.getRowCount(); r++) {
      renderer      = targetTable.getCellRenderer(r, colIndex);
      comp          = targetTable.prepareRenderer(renderer, r, colIndex);
      currentWidth  = comp.getPreferredSize().width;
      maxWidth      = Math.max(maxWidth, currentWidth);
    }

    // width of the title of the column
    TableCellRenderer headerRenderer = targetTable.getTableHeader()
        .getDefaultRenderer();
    comp = headerRenderer.getTableCellRendererComponent(null, targetTable
        .getColumnModel().getColumn(colIndex).getHeaderValue(), false, false,
        0, 0);
    maxWidth = Math.max(comp.getPreferredSize().width, maxWidth);

    return maxWidth + 2;
  }

  /**
   * Determine highest cell height in the row rowIndex.
   * 
   * @param rowIndex  Index of the row
   * @return          Highest cell height in the row rowIndex
   */
  public int getPreferredRowHeight(int rowIndex)
  {
    // Get the current default height for all rows
    int rowHeight = targetTable.getRowHeight();
    int currentHeight;
    TableCellRenderer renderer;
    Component comp;

    // Determine highest cell in the row
    for (int c = 0; c < targetTable.getColumnCount(); c++) {
      renderer      = targetTable.getCellRenderer(rowIndex, c);
      comp          = targetTable.prepareRenderer(renderer, rowIndex, c);
      currentHeight = comp.getPreferredSize().height;
      rowHeight     = Math.max(rowHeight, currentHeight);
    }

    return rowHeight;
  }

  /**
   * Ask to the user to choose a file, and save the data in the selected file.
   * 
   * @param data Data to write in the file
   */
  public void saveInFile(String data)
  {
    JFileChooser chooser = new JFileChooser();
    int returnVal = chooser.showSaveDialog(null);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File selectedFile = chooser.getSelectedFile();
      if (selectedFile == null) {
        JOptionPane.showMessageDialog(null, "No file selected");
      }
      else {
        try {
          // write the data in the file
          BufferedWriter writer = new BufferedWriter(new FileWriter(
              selectedFile));
          writer.write(data);
          writer.close();
        }
        catch (IOException ex) {
          JOptionPane.showMessageDialog(null, "Couldn't write file to "
              + selectedFile);
        }

      }
    }
  }

  /**
   * Return a string containing the results of the interaction research in a
   * text format The string contains the research parameters and all the
   * interactions.
   * 
   * @return A string containing the results of the interaction research in a
   *         text format
   */
  public String getInteractionInText()
  {
    Target target; // A target
    String[] views; // String which represents the interaction visualization
    String text = new String();

    String[][] columns = ((TargetTableModel) targetTable.getModel())
        .getColumns();
    int endIndex = columns.length - 2;

    text = "##############################################################"
        + "#################\n"
        + "Data generated by ApolloRNA - Research of RNA/RNA interactions\n\n"
        + requestSeqText + ": "
        + interactionSearch.getRequestSequenceDescription() + "\n"
        + searchRegionText + ": " + getRegionParam() + "\n" + searchMethodText
        + ": " + interactionSearch.searchMethod.getMethodName() + "\n("
        + interactionSearch.searchMethod.getParameterDesc() + ")\n\n"
        + "The headline of an interaction is composed of " + endIndex
        + " fields:\n";

    // display the description of each column
    text = text.concat("Name: name of the target\n");
    for (int i = 0; i < endIndex; i++) {
      if (i != INTERACTION_COLUMN) {
        text = text.concat(columns[i][0].replaceAll("\u00b0", "") + ": "
            + columns[i][1] + "\n");
      }
    }
    text = text.concat("###########################################" +
            "####################################\n\n");

    // display each row of the table
    for (int i = 0; i < targetTable.getRowCount(); i++) {
      target = (Target) targets.get(i);

      text = text.concat("Target_" + target.getMethodVariableOrder() + "\t");
      for (int j = 0; j < endIndex; j++) {
        if (j != INTERACTION_COLUMN) {
          if ((targetTable.getValueAt(i, j) != null) && 
              (targetTable.getValueAt(i, j).toString().compareTo("") != 0) ) {
            text = text.concat(targetTable.getValueAt(i, j).toString() + "\t");
          }
          else {
            text = text.concat(".\t");
          }
        }
      }
      text = text.concat("\n");
      // write the interaction visualization
      views = target.getInteractionView(interactionSearch.getRequestSequence()
          .getResidues());
      text = text.concat(views[0]);
      text = text.concat(views[1]);
      text = text.concat(views[2]);
      text = text.concat("\n\n");
    }
    return text;
  }

  /**
   * Return a string containing the predicted target sequences in the multifasta
   * format
   * 
   * @return A string containing the predicted target in multifasta format
   */
  public String getInteractionInFasta()
  {
    String fastaData = new String();
    Target target; // A target
    String refSeqName = CurationManager.getCurationManager()
        .getActiveCurState().getCurationSet().getRefSequence().getName(); 
    refSeqName = refSeqName.replaceAll(" ", "");
    String selectSeqName = interactionSearch.getRequestSequenceDescription();
    selectSeqName = selectSeqName.replaceAll(" ", "_");

    // for each target
    for (int i = 0; i < targets.size(); i++) {
      target = (Target) targets.get(i);
      fastaData = fastaData.concat(">" + refSeqName + "_");

      if (target.getStrand() == 1) {
        fastaData = fastaData.concat(target.getGenomicStart() + "_"
            + target.getGenomicStop());
      }
      else {
        fastaData = fastaData.concat(target.getGenomicStop() + "_"
            + target.getGenomicStart());
      }

      fastaData = fastaData.concat(" Target" + target.getMethodVariableOrder()
          + " " + selectSeqName + "\n" + target.getSequence() + "\n");
    }
    return fastaData;
  }

  /**
   * Return a string containing the predicted target sequences in the GFF format
   * 
   * @return A string containing the predicted targets in GFF format
   */
  public String getInteractionInGFF()
  {
    String gffData = new String();
    Target target; // A target

    // for each target
    for (int i = 0; i < targets.size(); i++) {
      target = (Target) targets.get(i);
      gffData = gffData.concat("Target_" + target.getMethodVariableOrder()
          + "\t" + "ApolloRNA\tmisc_feature\t" + target.getGenomicStart()
          + "\t" + target.getGenomicStop() + "\t0\t" + target.drawStrand()
          + "\t.\n");
    }
    return gffData;
  }

  /**
   * Open a new window which displays the interaction with the target
   * 
   * @param target Target of the interaction
   */
  public void viewInteraction(Target target)
  {
    // open a new window which displays the interaction in the selected row
    InteractionViewFrame viewFrame = new InteractionViewFrame(interactionSearch
        .getRequestSequence().getResidues(), interactionSearch
        .getRequestSequenceDescription(), target.getSequence(), target
        .getName(), target.getInteraction());
    views.add(viewFrame);
    isHidden = true;
  }

  /**
   * Open a new window which is used to change characters which represents pairs
   * between paired nucleotids Update all views of interaction in the jtable
   */
  public void parameterVisualization()
  {
    // open the new window to change characters representing pairs
    new ConfigureVisuDialog(this);
    // update visualization changes
    ((TargetTableModel) targetTable.getModel()).updateVisualization();
  }

  // Method to close the window. close all the interaction views,
  // created from this window
  public void dispose()
  {
    // close all the views of interactions, created from this window
    for (int i = 0; i < views.size(); i++) {
      ((InteractionViewFrame) views.get(i)).dispose();
    }
    super.dispose();
  }

  /**
   * Build a document which describes the columns of the table
   * 
   * @return styled document containing description of columns
   */
  public StyledDocument createDocument()
  {
    String title = "List of predicted targets with the previously selected sequence\n\n";
    String introduction = "The array presents one target per line with the following "
        + "description of each column:\n";
    String notes = "\n\nNotes:\n- In the visualization, the target is painting in red and the selected "
        + "sequence in blue,\n- Clicking on the picture opens a new window to better "
        + "see the interaction,\n- See the description of the search method clicking on "
        + "the 'Info: method' button.\n";
    String[][] columns = ((TargetTableModel) targetTable.getModel())
        .getColumns();

    // define styles
    StyledDocument doc = new DefaultStyledDocument();
    Style defaultStyle = StyleContext.getDefaultStyleContext().getStyle(
        StyleContext.DEFAULT_STYLE);
    Style commonStyle = doc.addStyle("commonStyle", defaultStyle);
    StyleConstants.setFontSize(commonStyle, 13);
    Style boldStyle = doc.addStyle("boldStyle", commonStyle);
    StyleConstants.setBold(boldStyle, true);
    Style titleStyle = doc.addStyle("standard", boldStyle);
    StyleConstants.setFontSize(titleStyle, 14);

    // write on the document
    try {
      doc.insertString(doc.getLength(), title, titleStyle);
      doc.insertString(doc.getLength(), introduction, commonStyle);
      // write the description of each column
      for (int i = 0; i < targetTable.getColumnCount(); i++) {
        doc.insertString(doc.getLength(), columns[i][0], boldStyle);
        doc.insertString(doc.getLength(), ": " + columns[i][1] + "\n",
            commonStyle);
      }
      // write some notes
      doc.insertString(doc.getLength(), notes, commonStyle);
    }
    catch (Exception e) {
      System.out.println(e);
    }
    return doc;
  }

  /**
   * Model of the JTable. This class is used to fill the jtable
   */
  private class TargetTableModel extends AbstractTableModel
  {
    private Object[][] targetData;
    private String initialSeq;

    private String[][] columns = {
        {"Position", "Low genomic position"},
        {"St", "Strand"},
        {"Interaction", "Interaction visualization"},
        {"Prev", "Preceeding gene and its distance with overlapping gene(s)"},
        {"Gene Pos", "Low genomic position of overlapping gene(s)"},
        {"Gene", "Overlapping gene(s)"},
        {"Gene Desc", "Description of overlapping gene(s) product"},
        {"Next", "Following gene and its distance with overlapping gene(s)"},
        {"Strands",
            "Strands of the previous gene, overlapping gene(s) and the next gene"},
        {"L", "Length of the interaction"},
        {"Var1", "Main variable of the search method"},
        {"Var2", "Secondary variable of the search method"},
        {"N\u00b0", "Order number according to the search method"},
        {"Pos N\u00b0", "Order number according to genomic position"}};

    // builder
    public TargetTableModel()
    {
      initialSeq = interactionSearch.getRequestSequence().getResidues();
      targetData = new Object[targets.size()][];
      fillTargetData();
    }

    /**
     * Fill the table with target data
     */
    public void fillTargetData()
    {
      Target target;
      JTextPane view;
      String previousGene;
      String nextGene;
      String var2;

      // fill the row of the table
      for (int i = 0; i < targetData.length; i++) {
        target = (Target) targets.get(i);
        // Create a jtextpane representing the interaction
        view = createView(target.getInteractionView(initialSeq));

        // Create the previous gene text: name of the gene and its distance with
        // overlapping genes
        if (target.getPreviousGene() != null) {
          previousGene = target.getPreviousGene().getName();
          if (target.getPreviousDistance() > 0) {
            previousGene = previousGene.concat(" (-"
                + target.getPreviousDistance() + ")");
          }
          else if (target.getPreviousDistance() == 0) {
            previousGene = previousGene.concat(" (0)");
          }
        }
        else {
          previousGene = "";
        }

        // Create the next gene text: name of the gene and its distance with
        // overlapping genes
        if (target.getNextGene() != null) {
          nextGene = target.getNextGene().getName();
          // if there is a gene and a next gene, write the distance
          if (target.getNextDistance() > 0) {
            nextGene = nextGene.concat(" (+" + target.getNextDistance() + ")");
          }
          else if (target.getNextDistance() == 0) {
            nextGene = nextGene.concat(" (0)");
          }
        }
        else {
          nextGene = "";
        }
        // Create the Var2 string: empty if its value is negative infinity
        if (target.getRNAfoldFreeEnergy() == Float.NEGATIVE_INFINITY) {
          var2 = null;
        } 
        else {
          var2 = String.valueOf(target.getRNAfoldFreeEnergy());
        }

        // add a row in the table
        Object[] row = {String.valueOf(target.getGenomicStart()),
            target.drawStrand(), view, previousGene,
            target.getGenesPosition(), target.getGenesName(),
            target.getGeneTranscripts(), nextGene,
            target.getSymbols(), 
            String.valueOf(target.getInteractionLength()),
            String.valueOf(target.getMethodVariableValue()),
            var2,
            String.valueOf(target.getMethodVariableOrder()),
            String.valueOf(target.getGenomicStartOrder()),
        };
        targetData[i] = row;
      }
    }

    /**
     * Call the computing of the visualization of all the interactions and
     * update the column "interaction" of the jtable
     */
    public void updateVisualization()
    {
      for (int i = 0; i < targetData.length; i++) {
        Target target = (Target) targets.get(i);
        // create a jtextpane representing the interaction
        JTextPane view = createView(target.getInteractionView(initialSeq));
        // change the contain of the cell containing the visualization
        // of the interaction
        targetData[i][INTERACTION_COLUMN] = view;
        this.fireTableCellUpdated(i, INTERACTION_COLUMN);
      }
    }

    public int getColumnCount()
    {
      return columns.length;
    }

    public int getRowCount()
    {
      return targetData.length;
    }

    public String getColumnName(int col)
    {
      return columns[col][0];
    }

    public Object getValueAt(int rowIndex, int columnIndex)
    {
      return targetData[rowIndex][columnIndex];
    }

    public boolean isCellEditable(int row, int col)
    {
      boolean isEditable = false;
      if (col == INTERACTION_COLUMN) {
        isEditable = true;
      }
      return isEditable;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
      targetData[rowIndex][columnIndex] = aValue;
      this.fireTableCellUpdated(rowIndex, INTERACTION_COLUMN);
    }

    /**
     * Return 2 dimensions array of the columns
     * 
     * @return 2 dimensions array of the columns
     */
    public String[][] getColumns()
    {
      return columns;
    }

    /**
     * create a jtextpane which contains the interaction view. Apply some styles
     * to the string
     * 
     * @param views Array of 3 string which are the string to display in the
     *              jtextpane
     * @return      A jtextpane which contains the interaction view
     */
    public JTextPane createView(String[] views)
    {
      JTextPane interactionTextPane = new JTextPane();
      interactionTextPane.setEditable(false);

      // use to center the text on the jtextpane
      SimpleAttributeSet set = new SimpleAttributeSet();
      StyleConstants.setAlignment(set, StyleConstants.ALIGN_CENTER);
      interactionTextPane.setParagraphAttributes(set, true);

      // styles which will be applicated on the text
      StyledDocument styledDocument = interactionTextPane.getStyledDocument();
      Style defaultStyle = StyleContext.getDefaultStyleContext().getStyle(
          StyleContext.DEFAULT_STYLE);
      Style regular = styledDocument.addStyle("courier", defaultStyle);

      StyleConstants.setFontSize(regular, 10);
      StyleConstants.setFontFamily(regular, "Courier");

      Style s = styledDocument.addStyle("seq1_courier", regular);
      StyleConstants.setForeground(s, Color.red);
      s = styledDocument.addStyle("seq2_courier", regular);
      StyleConstants.setForeground(s, Color.blue);

      // add the three styled string
      try {
        styledDocument.insertString(styledDocument.getLength(), views[0],
            styledDocument.getStyle("seq1_courier"));
        styledDocument.insertString(styledDocument.getLength(), views[1],
            styledDocument.getStyle("courier"));
        styledDocument.insertString(styledDocument.getLength(), views[2],
            styledDocument.getStyle("seq2_courier"));
      }
      catch (BadLocationException ble) {
        System.err.println("Couldn't insert initial text into text pane.");
      }

      return (interactionTextPane);
    }
  }

  /**
   * Class use to draw the cells of the jtable foreground of the cell is white
   * or blue, according to the row of the cell
   */
  private class ColoredCellRenderer extends DefaultTableCellRenderer
  {
    public ColoredCellRenderer()
    {
      super();
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
        boolean isSelected, boolean hasFocus, int row, int column)
    {
      setHorizontalAlignment(CENTER);
      JLabel cellLabel = (JLabel) super.getTableCellRendererComponent(table,
          value, isSelected, hasFocus, row, column);

      // color the cell according to its row
      if (isSelected == false) {
        if ((row % 2) == 0) {
          cellLabel.setBackground(Color.WHITE);
        }
        else {
          cellLabel.setBackground(cellColor);
        }
      }
      return cellLabel;
    }
  }

  /**
   * Class used to draw a JTextPane in the column interaction
   */
  private class InteractionViewRenderer extends JTextPane
      implements TableCellRenderer
  {
    public InteractionViewRenderer()
    {
      super();
    }

    public Component getTableCellRendererComponent(JTable table,
        Object textPane, boolean isSelected, boolean hasFocus, int row,
        int column)
    {
      return (JTextPane) textPane;
    }
  }

  /**
   * Class used to create an event when the user acts on the interaction column
   */
  private class InteractionViewEditor implements TableCellEditor
  {
    public Component getTableCellEditorComponent(JTable table, Object value,
        boolean isSelected, int row, int column)
    {
      // get the target corresponding to the row
      Target target = (Target) targets.get(row);
      // call the view of te interaction
      viewInteraction(target);

      return null;
    }

    public void cancelCellEditing()
    {
    }

    public boolean stopCellEditing()
    {
      return true;
    }

    public Object getCellEditorValue()
    {
      return null;
    }

    public boolean isCellEditable(EventObject anEvent)
    {
      return true;
    }

    public boolean shouldSelectCell(EventObject anEvent)
    {
      return true;
    }

    public void addCellEditorListener(CellEditorListener l)
    {
    }

    public void removeCellEditorListener(CellEditorListener l)
    {
    }
  }

  /**
   * Internal class used to create a new jdialog displaying information
   */
  class InfoDialog extends JDialog
  {
    JPanel groupPanel;
    JPanel buttonPanel;
    JButton button;
    JTextPane messagePane;

    /**
     * @param parent 	Parent frame
     * @param title 	Title of the window
     * @param doc 		The containing to insert in the window
     */
    public InfoDialog(JFrame parent, String title, StyledDocument doc)
    {
      super(parent, title, false);
      buttonPanel = new JPanel(true);
      button = new JButton("OK");
      messagePane = new JTextPane();

      messagePane.setEditable(false);
      messagePane.setBackground(Color.white);
      messagePane.setStyledDocument(doc);

      button.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e)
        {
          setVisible(false);
        }
      });

      buttonPanel.add(button);
      buttonPanel.setBackground(bgColor);
      button.setBackground(InteractionSearchSetDialog.buttonColor);

      groupPanel = new JPanel(new BorderLayout());
      groupPanel.add(messagePane, BorderLayout.CENTER);
      groupPanel.add(buttonPanel, BorderLayout.SOUTH);
      groupPanel.setBackground(bgColor);

      this.getContentPane().add(groupPanel, BorderLayout.CENTER);
      this.pack();
    }
  }
  
  /**
   * Create the information dialog and put the infoDocument. 
   * 
   * @param infoDocument Document to dispaly in the information jdialog
   */
  public void createInfoDialog(StyledDocument infoDocument) 
  {
    methodDocDialog = new InfoDialog(this, "More about method",
    infoDocument);
  }

}
