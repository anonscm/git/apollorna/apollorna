package apollo.gui.rna;

import apollo.config.Config;
import apollo.dataadapter.DataLoadEvent;
import apollo.dataadapter.DataLoadListener;
import apollo.datamodel.RangeI;
import apollo.datamodel.SeqFeatureI;
import apollo.datamodel.SequenceI;
import apollo.editor.AnnotationChangeEvent;
import apollo.editor.AnnotationChangeListener;
import apollo.gui.ControlledObjectI;
import apollo.gui.Controller;
import apollo.gui.Selection;
import apollo.gui.event.FeatureSelectionEvent;
import apollo.gui.event.FeatureSelectionListener;
import apollo.seq.io.FastaFile;
import apollo.util.FeatureList;
import apollo.util.NumericKeyFilter;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


/**
 * This class is a window to display predicted secondary structure of a selected
 * sequence, the associated free energy and the sequence. The secondary
 * structure is predicted with RNAfold program, which generates a ps file which
 * contains the image of the structure. Convert program is called to convert the
 * ps file in a PNG file. These files are saved in the temporary directory
 * .apollo/tmp/
 */
public class SecondaryStructureView extends JFrame 
  implements DataLoadListener
{
  private FeatureList features;

  // graphic components
  private JScrollPane   seqScrollPane;
  private JTextArea     seqTextArea;
  private JRadioButton  currentlySelectedButton;
  private JRadioButton  genomicButton;
  private JRadioButton  genomicPlusMinusButton;
  private JTextField    genomicPlusMinusField;
  private JTextField    informationTextField;
  private JTextField    freeEnergyTextField;

  private DrawImage imagePanel;
  private String    structureImageWay;
  private String    rnafoldImageWay;
  private JCheckBox followSelectionCheckBox;

  // dynamic objects of the window
  private Selection selection;
  private Controller controller;

  // static variables about the graphic window apparence
  private static final int defaultGenomicPlusMinus = 20;
  private static final Color bgColor = new Color(221, 238, 221);
  private static final Font seqFont = new Font("Courier", 0, 12);
  private static final Font textFieldFont = new Font("Serif", Font.BOLD, 12);

  private static int RNAFOLD_SEQ_MAX_LENGTH = 1500;

  // global variable used to number the window drawing secondary structure
  private static int lastWindowNo = 0;

  // value of the shift applied on the sequence
  private int genomicPlusMinus = defaultGenomicPlusMinus;
  // to organize different window location
  private int offset = 0;
  // number of the current window: need it to create the name of the secondary
  // structure image
  private int currentWindowNo;

  /**
   * Builder : post-processes the selection and call window intialization and
   * RNAfold
   * 
   * @param selection   sequence selection
   * @param controller  controller of the main frame
   */
  public SecondaryStructureView(Selection selection, Controller controller)
  {
    this(selection, controller, 0);
  }

  /**
   * Builder : post-processes the selection and call window intialization and
   * RNAfold
   * 
   * @param selection   sequence selection
   * @param controller  controller of the main frame
   * @param offset      shift to display this window
   */
  public SecondaryStructureView(Selection selection, Controller controller,
      int offset)
  {
    this.selection = selection;
    this.controller = controller;

    // Offset is for horizontally offsetting new sequence windows so they're
    // not all smack on top of each other.
    this.offset = offset;
    // creation of the temporary directory to save images
    temporyDirCreate();

    // If all exons in transcript present, it replaces exons w/ trans
    // so user sees transcript seq not all the exons seq
    FeatureList consolidatedFeatures = selection.getConsolidatedFeatures();
    init(consolidatedFeatures, controller);
  }

  /**
   * Builder: call window intialization and RNAfold
   * 
   * @param feat        a sequence feature
   * @param controller  controller of the main frame
   */
  public SecondaryStructureView(SeqFeatureI feat, Controller controller)
  {
    FeatureList f = new FeatureList(1);
    f.addFeature(feat); // Only a single feature - no need to consolidate
    init(f, controller);
  }

  /**
   * intialize GUI, process features
   * 
   * @param features selected features
   * @param controller controller of the main frame
   */
  private void init(FeatureList features, Controller controller)
  {
    // If the selected sequence has a size longer than the max size, 
    // display an error message
    if ( (features.size() > 0) && 
        (features.getFeature(0).getResidues().length() 
            >= RNAFOLD_SEQ_MAX_LENGTH) ) {
      String message = 
        "The secondary structure isn't computed because the length " +
        "of the selected sequence is higher than " + RNAFOLD_SEQ_MAX_LENGTH + ".";
      JOptionPane.showMessageDialog(null, message, "Error",
          JOptionPane.ERROR_MESSAGE);
      this.dispose();
    }
    else {
      // initialization of the number of the current window
      incLastWindowNo();
      this.currentWindowNo = getLastWindowNo();

      // initialization of the way of the images of this window
      this.structureImageWay  = getStructureImageWay();
      this.rnafoldImageWay    = getRNAfoldFileWay();

      initGui();
      setLocation(offset, 0);
      processFeatures(features);
      // inner class that deals with selection and window control
      new SecStructureViewFeatureListener(controller);

      this.pack();
      setVisible(true);
    }
  }

  /**
   * go through all_features feat list and add features to feature list, if gene
   * is found, add its transcripts not the gene. if a feature has result
   * sequence enable result button
   * 
   * @param all_features selected features
   */
  private void processFeatures(FeatureList all_features)
  {
    // use to catch an error
    // when the selected sequence results from a selection
    // with the middle button of the mouse (doesn't result of a clic about a
    // feature)
    // Reference sequence is equal to the selected sequence (Incoherent).
    // So, it's impossible to extend the sequence with the checkbutton
    // A solution is in this case to unabled this checkbutton
    // seqAndRefAreEqual is true if Reference sequence is equal to the selected
    // sequence
    boolean seqAndRefAreEqual = false;
    this.features = new FeatureList();

    for (int i = 0; i < all_features.size(); i++) {
      SeqFeatureI feat = all_features.getFeature(i);
      if ( feat.getRefSequence() != null && 
           feat.getRefSequence().getResidues().compareTo(feat.getResidues()) == 0) {
        seqAndRefAreEqual = true;
      }

      if (feat.isAnnotTop() && feat.hasKids()) {
        Vector transcripts = feat.getFeatures();
        features.addAll(transcripts);

      }
      else {
        /*
         * whatever it is, add it and deal with it (as long as it has some
         * residues. This is to keep 0-residue start/stop codons from showing
         * up)
         */
        if (feat.getResidues().length() > 0)
          features.addFeature(feat);
      }
    }
    currentlySelectedButton.setSelected(true);

    if (seqAndRefAreEqual == true) {
      // unable the genomic plus minus checkbutton
      genomicPlusMinusButton.setEnabled(false);
      genomicPlusMinusField.setEnabled(false);
    }
    else {
      genomicPlusMinusButton.setEnabled(true);
      genomicPlusMinusField.setEnabled(true);
    }

    displaySequences();
  }

  /**
   * Method getTitle of the JFrame String that appears in window menus list:
   * name of the sequence if there is only one sequence else the number of
   * sequences
   * 
   * @return name of the sequence or the nb of sequences (if the number is >1)
   */
  public String getTitle()
  {
    if (features.size() == 1) {
      SeqFeatureI feature = features.getFeature(0);
      return Config.getDisplayPrefs().getHeader(feature);
    }
    else
      return features.size() + " sequences";
  }

  /**
   * set up graphical components
   */
  private void initGui()
  {
    int leftWidth = 470; // width of the left part in the window
    int rightWidth = 400; // width of the RIGHT part in the window
    Dimension textDimension       = new Dimension(rightWidth, 30);
    Dimension buttonBoxDimension  = new Dimension(rightWidth, 40);
    Dimension imagePanelDimension = new Dimension(leftWidth, leftWidth);

    // panel for image
    imagePanel = new DrawImage();
    imagePanel.setPreferredSize(imagePanelDimension);
    imagePanel.setMinimumSize(imagePanelDimension);

    // sequence
    Dimension scrollpaneDimension = new Dimension(rightWidth, 200);

    seqTextArea = new JTextArea();
    seqTextArea.setFont(seqFont);
    seqTextArea.setEditable(false);
    seqScrollPane = new JScrollPane(seqTextArea);
    seqScrollPane.setMinimumSize(scrollpaneDimension);
    seqScrollPane.setPreferredSize(scrollpaneDimension);
    seqScrollPane.setMaximumSize(scrollpaneDimension);

    // free energy text field
    freeEnergyTextField = new JTextField(" ");
    freeEnergyTextField.setBackground(bgColor);
    freeEnergyTextField.setFont(textFieldFont);
    freeEnergyTextField.setEditable(false);
    freeEnergyTextField.setBorder(null);
    freeEnergyTextField.setMinimumSize(textDimension);
    freeEnergyTextField.setMaximumSize(textDimension);
    freeEnergyTextField.setPreferredSize(textDimension);

    // information text field
    informationTextField = new JTextField(" ");
    informationTextField.setBackground(bgColor);
    informationTextField.setFont(textFieldFont);
    informationTextField.setForeground(Color.RED);
    informationTextField.setEditable(false);
    informationTextField.setBorder(null);
    informationTextField.setMinimumSize(textDimension);
    informationTextField.setMaximumSize(textDimension);
    informationTextField.setPreferredSize(textDimension);

    // radio button to select initial sequence
    genomicButton = new JRadioButton("Corresponding genomic sequence");
    genomicButton.setBackground(bgColor);
    genomicButton.setAlignmentX(0.0f);
    genomicButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        displayGenomicSequence();
        currentlySelectedButton = genomicButton;
      }
    });
    // Box which contains genomic radio button
    Box genomicBox = new Box(BoxLayout.X_AXIS);
    genomicBox.add(genomicButton);
    genomicBox.add(Box.createHorizontalGlue());
    genomicBox.setPreferredSize(textDimension);
    genomicBox.setMinimumSize(textDimension);
    genomicBox.setMaximumSize(textDimension);

    // radio button to select initial sequence +/- x bases
    genomicPlusMinusButton = new JRadioButton(
        "Corresponding genomic sequence +/-");
    genomicPlusMinusButton.setBackground(bgColor);
    GenomicPlusMinusListener plusMinusListener = new GenomicPlusMinusListener();
    genomicPlusMinusButton.addActionListener(plusMinusListener);

    // group of the radio buttons to select sequence
    ButtonGroup radioButtonGroup = new ButtonGroup();
    radioButtonGroup.add(genomicButton);
    radioButtonGroup.add(genomicPlusMinusButton);

    // Adds the possibility to choose the genomic range to export
    Dimension fieldDim = new Dimension(5, 20);
    genomicPlusMinusField = new JTextField(Integer.toString(genomicPlusMinus),
        5);
    genomicPlusMinusField.setMinimumSize(fieldDim);
    genomicPlusMinusField.setPreferredSize(fieldDim);
    genomicPlusMinusField.setMaximumSize(fieldDim);
    genomicPlusMinusField.addKeyListener(NumericKeyFilter.getFilter());
    genomicPlusMinusField.addActionListener(plusMinusListener);

    JLabel bases = new JLabel("bases");

    Box genomicPlusMinusBox = new Box(BoxLayout.X_AXIS);
    genomicPlusMinusBox.add(genomicPlusMinusButton);
    genomicPlusMinusBox.add(genomicPlusMinusField);
    genomicPlusMinusBox.add(bases);
    genomicPlusMinusBox.add(Box.createHorizontalGlue());
    genomicPlusMinusBox.setPreferredSize(textDimension);
    genomicPlusMinusBox.setMinimumSize(textDimension);
    genomicPlusMinusBox.setMaximumSize(textDimension);
    // Color of the buttons
    final Color backgroundButtonColor = Color.white;

    // sequence save button
    JButton seqSaveButton = new JButton("Save sequence as...");
    seqSaveButton.setBackground(backgroundButtonColor);
    seqSaveButton.addActionListener(new SeqSaveListener());

    // structure save button
    JButton structSaveButton = new JButton("Save structure as...");
    structSaveButton.setBackground(backgroundButtonColor);
    structSaveButton.addActionListener(new StructSaveListener());

    // box for save buttons
    Box saveBox = new Box(BoxLayout.X_AXIS);
    saveBox.add(seqSaveButton);
    saveBox.add(Box.createHorizontalStrut(10));
    saveBox.add(structSaveButton);
    saveBox.add(Box.createHorizontalGlue());
    saveBox.setPreferredSize(buttonBoxDimension);
    saveBox.setMinimumSize(buttonBoxDimension);
    saveBox.setMaximumSize(buttonBoxDimension);

    // close button
    JButton closeButton = new JButton("Close");
    closeButton.setBackground(backgroundButtonColor);
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    // new sequence window button
    JButton newSeqWindow = new JButton("New structure window...");
    newSeqWindow.setBackground(backgroundButtonColor);
    newSeqWindow.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        new SecondaryStructureView(selection, controller, offset + 20);
      }
    });
    // flip image button
    JButton flipButton = new JButton("Flip image");
    flipButton.setBackground(backgroundButtonColor);
    flipButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        // flip the image and repaint the imagePanel
        if( new File(structureImageWay).exists() == true) {
          convert(structureImageWay, structureImageWay, "-flip");
          imagePanel.replaceImage(structureImageWay);
          imagePanel.repaint();
        }
      }
    });
    // box for close button, new sequence window button and flip image button
    Box closeSeqBox = new Box(BoxLayout.X_AXIS);
    closeSeqBox.add(closeButton);
    closeSeqBox.add(Box.createHorizontalStrut(10));
    closeSeqBox.add(newSeqWindow);
    closeSeqBox.add(Box.createHorizontalStrut(10));
    closeSeqBox.add(flipButton);
    closeSeqBox.add(Box.createHorizontalGlue());
    closeSeqBox.setPreferredSize(buttonBoxDimension);
    closeSeqBox.setMinimumSize(buttonBoxDimension);
    closeSeqBox.setMaximumSize(buttonBoxDimension);

    // follow selection checkbox
    followSelectionCheckBox = new JCheckBox("Follow external selection", false);
    followSelectionCheckBox.setBackground(bgColor);

    // box which contains the checkbox
    Box followHorizontalBox = new Box(BoxLayout.X_AXIS);
    followHorizontalBox.add(Box.createHorizontalGlue());
    followHorizontalBox.add(followSelectionCheckBox);
    followHorizontalBox.add(Box.createHorizontalStrut(30));
    followHorizontalBox.setPreferredSize(buttonBoxDimension);
    followHorizontalBox.setMinimumSize(buttonBoxDimension);
    followHorizontalBox.setMaximumSize(buttonBoxDimension);

    // right part of the window
    Container rightContainer = new Container();
    rightContainer.setLayout(new BoxLayout(rightContainer, BoxLayout.Y_AXIS));
    rightContainer.add(seqScrollPane);
    rightContainer.add(freeEnergyTextField);
    rightContainer.add(informationTextField);
    rightContainer.add(genomicBox);
    rightContainer.add(genomicPlusMinusBox);
    rightContainer.add(saveBox);
    rightContainer.add(closeSeqBox);
    rightContainer.add(followHorizontalBox);

    // add image (to the left) and right part to the main container
    Container content = this.getContentPane();
    content.setBackground(bgColor);
    content.setLayout(new BoxLayout(content, BoxLayout.X_AXIS));
    content.add(imagePanel);
    content.add(rightContainer);

    currentlySelectedButton = genomicButton;

    // add listeners when the current window is closed
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e)
      {
        deleteStructureFile();
      }
    });
    addWindowListener(new WindowAdapter() {
      public void windowClosed(WindowEvent e)
      {
        deleteStructureFile();
      }
    });
  }

  /**
   * call the appropriated method to display sequence
   */
  private void displaySequences()
  {
    currentlySelectedButton.doClick();
    setTitle(getTitle());
  }

  /**
   * display selected sequence and secondary structure of the sequence
   */
  private void displayGenomicSequence()
  {
    SeqFeatureI feat;
    String fastas         = "";
    String firstSequence  = "";
    int feat_count = features.size();

    // compute the selected sequence(s) in the fasta format
    for (int i = 0; i < feat_count; i++) {
      feat = features.getFeature(i);
      fastas += featureFastaString(feat.getResidues(), feat, "genomic");

      // get the first sequence of the selection
      if (i == 0) {
        firstSequence = feat.getResidues();
      }
    }
    // display fasta sequence(s) in the textarea
    seqTextArea.setText(fastas);
    seqTextArea.setCaretPosition(0);

    // call rnafold and display the secondary structure of the first sequence
    displaySecondaryStructure(firstSequence, feat_count);
  }

  /**
   * display selected sequence +/- x nucleotids and secondary structure of the
   * corresponding sequence
   */
  private void displayGenomicPlusMinus()
  {
    SeqFeatureI feat;
    SequenceI refSeq;
    int minus;
    int plus;
    String seq;
    String fastas         = "";
    String firstSequence  = "";
    int feat_count = features.size();

    // compute the selected sequence(s) in the fasta format
    for (int i = 0; i < feat_count; i++) {
      feat    = features.getFeature(i);
      refSeq  = feat.getRefSequence();
      if (feat == null || refSeq == null)
        continue;
      minus = genomicPlusMinus * feat.getStrand(); // hard wired for now
      plus  = genomicPlusMinus * feat.getStrand();
      seq   = refSeq.getResidues(feat.getStart() - minus, feat.getEnd() + plus);
      fastas += featureFastaString(seq, feat, "genomic +/-" + genomicPlusMinus
          + " bases");

      // get the first sequence of the selection
      if (i == 0) {
        firstSequence = refSeq.getResidues(feat.getStart() - minus, feat
            .getEnd() + plus);
      }
    }

    // display fasta sequence(s) in the textarea
    seqTextArea.setText(fastas);
    seqTextArea.setCaretPosition(0);

    // call rnafold and display the secondary structure of the first sequence
    displaySecondaryStructure(firstSequence, feat_count);
  }

  /**
   * call RNAfold program and display the predicted secondary structure
   * 
   * @param sequence      nucleotidic sequence
   * @param featureNumber number of features of the original selection
   */
  private void displaySecondaryStructure(String sequence, int featureNumber)
  {
    boolean hasFailed = false;
    
    informationTextField.setText("");
    freeEnergyTextField.setText("");
    if (featureNumber > 0) {
      if (sequence.length() <= RNAFOLD_SEQ_MAX_LENGTH) {

        boolean rnafoldExeOk = callRNAfold(sequence);
        if (rnafoldExeOk == true) {

          // convert the ps generated image in PNG
          boolean convertExeOk = callConvert();
          if (convertExeOk == true) {
            imagePanel.replaceImage(structureImageWay);
            imagePanel.repaint();

            if (featureNumber > 1) {
              informationTextField
                  .setText("WARNING: secondary structure of the first sequence!");
            }
          }
          else {
            informationTextField.setText("ERROR during convert computing!");
            hasFailed = true;
          }
        }
        else {
          informationTextField.setText("ERROR during RNAfold computing!");
          hasFailed = true;
        }
      }
      // sequence too long
      else {
        informationTextField.setText("Structure isn't computed: " +
            "selected sequence too long.");
        hasFailed = true;
      }
    }
    // no selected sequence
    else {
      informationTextField.setText("WARNING: no selected sequence!");
      hasFailed = true;
    }
    
    if (hasFailed == true) {
      imagePanel.replaceImage("");
      // If a rnafold image exists, delete it
      File structureFile = new File(structureImageWay);
      if (structureFile.exists() == true) {
        structureFile.delete();
      }
    }
    
    this.repaint();
  }

  /**
   * call RNAfold program which predicts a secondary structure
   * 
   * @param sequence  nucleotidic sequence
   * @return          true if the computing of RNAfold is ok
   */
  private boolean callRNAfold(String sequence)
  {
    String        command = "";
    File          imageDirectory = new File(getTmpDir());
    Process       process;
    OutputStream  outputStream;
    String        freeEnergy;

    try {
      // call RNAfold
      process = Runtime.getRuntime().exec(getRNAfoldCommand(), null, imageDirectory);
      outputStream = process.getOutputStream();

      // write the header of the sequence
      command = ">" + getFileHead() + "\n";
      outputStream.write(command.getBytes());
      outputStream.flush();

      // write the sequence
      command = sequence + "\n";
      outputStream.write(command.getBytes());
      outputStream.flush();

      // write the exit character
      command = "@\n";
      outputStream.write(command.getBytes());
      outputStream.flush();
      outputStream.close();

      // get the free energy
      InputStream       inputStream = process.getInputStream();
      InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
      BufferedReader    bufferedReader = new BufferedReader(inputStreamReader);
      String lastLine = new String();
      String line = bufferedReader.readLine();
      while (line != null) {
        lastLine  = line;
        line      = bufferedReader.readLine();
      }
      freeEnergy = extractFreeEnergy(lastLine);
      bufferedReader.close();
      inputStreamReader.close();
      inputStream.close();

      // wait the end of the process
      process.waitFor();
      freeEnergyTextField.setText("Free energy: " + freeEnergy);

    }
    catch (Exception ex) {
      System.out.println("Exception : " + ex.toString());
      return false;
    }
    return true;

  }

  /**
   * @param line  last line of the output of rnafold. Example: ((..)) (-12.50)
   * @return      free energy ; value contained in the parenthesis
   */
  private String extractFreeEnergy(String line)
  {
    String[] words    = line.trim().split(" ");
    String freeEnergy = words[words.length - 1];
    
    freeEnergy = freeEnergy.replaceAll("[()]", "");
    return freeEnergy;
  }

  /**
   * Convert the generated image by RNAfold in PNG with convert of ImageMagick
   * 
   * @return true if the convertion is ok
   */
  private boolean callConvert()
  {
    boolean isConverted = true;
    File rnafoldGeneratedFile = new File(rnafoldImageWay);

    if (rnafoldGeneratedFile.exists()) {
      isConverted = convert(rnafoldImageWay, structureImageWay, "");
      // delete postscript file generated by rnafold
      rnafoldGeneratedFile.delete();
    }
    else {
      System.out.println("File to convert doesn't exist!!");
      isConverted = false;
    }
    return isConverted;
  }

  /**
   * Execute the convert command between the two files with the specified
   * options
   * 
   * @param file1   Name of the file to convert
   * @param file2   Name of the resulting file
   * @param options Options of the convert command
   * @return        true if the convertion is ok
   */
  public static boolean convert(String file1, String file2, String options)
  {
    String  command;
    Runtime runtime;
    Process process;
    boolean isConverted = true;

    if (apollo.util.IOUtil.isWindows()) 
      command = getConvertCommand() + " " + options + " \"" + file1 + "\" \"" + file2 + "\"";
    else 
      command = getConvertCommand() + " " + options + " " + file1 + " " + file2;

    runtime = Runtime.getRuntime();
    // convert the source file into target file
    try {
      process = runtime.exec(command);
      process.waitFor();
      process.destroy();
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
      isConverted = false;
    }
    return isConverted;

  }

  /**
   * Generate the text contained in the fasta file of the sequence
   * 
   * @param seq   nucleotidic sequence
   * @param feat  feature
   * @param type  type of the sequence
   * @return      text of the fasta file
   */
  private String featureFastaString(String seq, RangeI feat, String type)
  {
    String header = (">" + Config.getDisplayPrefs().getHeader(feat) + " ("
        + type + " sequence): " + seq.length() + " residues");
    header = header + "\n";
    return FastaFile.format(header, seq, 50);
  }

  /**
   * Listens for changed in the genomic plus minus range used if the radio
   * button is clicked and when the an action is performed on the text field
   */
  private class GenomicPlusMinusListener 
    implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      String txt = genomicPlusMinusField.getText();
      try {
        genomicPlusMinus = Integer.parseInt(txt);
      }
      catch (NumberFormatException nfe) { // Mostly in case of overflow
        genomicPlusMinus = defaultGenomicPlusMinus;
        genomicPlusMinusField
            .setText(Integer.toString(defaultGenomicPlusMinus));
        String msg = "Could not convert " + txt + "\nto an integer, using "
            + defaultGenomicPlusMinus + " instead";
        JOptionPane.showMessageDialog(SecondaryStructureView.this, msg,
            "Error", JOptionPane.ERROR_MESSAGE);
      }
      if (currentlySelectedButton != genomicPlusMinusButton) {
        currentlySelectedButton = genomicPlusMinusButton;
        genomicPlusMinusButton.setSelected(true);
      }
      displayGenomicPlusMinus();
    }
  }

  /**
   * Listens to sequence save button, brings up save file chooser, and writes
   * file
   */
  private class SeqSaveListener 
    implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      if (features.size() > 0) {
        JFileChooser chooser = new JFileChooser();
        int returnVal = chooser.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File file = chooser.getSelectedFile();
          if (file == null) { // file might be null
            // is this message overkill - should it just exit?
            JOptionPane.showMessageDialog(null, "No file selected");
            return;
          }
          try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(seqTextArea.getText());
            writer.close();
          }
          catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Couldn't write file to " + file);
          }
        }
      }
    }
  }

  /**
   * Listens to structure save button, brings up save file chooser, and writes
   * file
   */
  private class StructSaveListener 
    implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      if (new File(structureImageWay).exists() == true) {
        JFileChooser chooser = new JFileChooser();
        int returnVal = chooser.showSaveDialog(null);
        
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File selectedFile = chooser.getSelectedFile();
          if (selectedFile == null) { // file might be null
            JOptionPane.showMessageDialog(null, "No file selected");
            return;
          }
          copyFile(structureImageWay, selectedFile.getPath() + ".png");
        }
      }
    }
  }

  /**
   * Deals with selection listening and being a controlled object to get in
   * controllers window list implements ControlledObjectI so it can be added to
   * Controller, so it show up in Windows menu
   */
  private class SecStructureViewFeatureListener
      implements FeatureSelectionListener, AnnotationChangeListener,
      ControlledObjectI
  {
    private Controller controller;

    private SecStructureViewFeatureListener(Controller controller)
    {
      setController(controller);
      controller.addListener(this); // adds this inner class as listener
    }

    /**
     * Sets the Controller for the object (ControlledObjectI)
     */
    public void setController(Controller controller)
    {
      this.controller = controller;
      // this is to get the window to show up in the windows menu
    }

    /**
     * Gets the Controller for the object(ControlledObjectI)
     */
    public Controller getController()
    {
      return controller;
    }

    public Object getControllerWindow()
    {
      return SecondaryStructureView.this;
    }

    /**
     * Whether controller should remove as listener on window closing
     */
    public boolean needsAutoRemoval()
    {
      return true;
    }

    /**
     * FeatureSelectionListener - handle selection. Only deals with selection if
     * this is the most recent instance
     */
    public boolean handleFeatureSelectionEvent(FeatureSelectionEvent e)
    {
      // if followSelectionCheckBox not checked dont handle
      if (!followSelectionCheckBox.isSelected() && !e.forceSelection())
        return false;

      // if all exons of trans present, deletes exons adds trans
      FeatureList consolidatedFeatures = e.getSelection()
          .getConsolidatedFeatures();
      processFeatures(consolidatedFeatures);
      return true;
    }

    public boolean handleAnnotationChangeEvent(AnnotationChangeEvent evt)
    {
      if (evt.isEndOfEditSession()) {
        /*
         * Perhaps it would be better to check the vector and only process if
         * the changed features is something that is visible, but this is much
         * simpler. What really is wrong here is that deletes and additions to
         * the feature is not really dealt with and that should be fixed
         */
        processFeatures(features);
      }
      else if (evt.isDelete()) {
        SeqFeatureI gone = evt.getChangedFeature();
        boolean handled = false;
        for (int i = features.size() - 1; i >= 0 && !handled; i--) {
          handled = features.remove(gone);
        }
      }
      return true;
    }
  }

  /**
   * Region changing means a new data set is being loaded. Get rid of this
   * window. (Not working yet--doesn't seem to get called when region changes.
   * Probably need to add window listener when new SeqWindow is created.) Do we
   * want this? I think sima likes that the seq window lingers.
   */
  public boolean handleDataLoadEvent(DataLoadEvent e)
  {
    this.hide();
    this.dispose();
    controller.removeListener(this);
    return true;
  }

  /**
   * @return start of the rna file name
   */
  private String getFileHead()
  {
    String name = "rna" + getCurrentWindowNo();
    return name;
  }

  /**
   * @return: way of the postscript file generated by RNAfold software
   */
  private String getRNAfoldFileWay()
  {
    String name = getTmpDir() + getFileHead();
    name += "_ss.ps";
    return name;
  }

  /**
   * @return way of the image displaying the secondary structure of the
   *         current sequence
   */
  private String getStructureImageWay()
  {
    String way = getTmpDir() + getFileHead() + ".png";
    return way;
  }

  /**
   * create the directory tmp which will contain images of secondary structure
   */
  public void temporyDirCreate()
  {
    File tmpDir = new File(getTmpDir());
    if (!tmpDir.isDirectory()) {
      tmpDir.mkdir();
    }
  }

  /**
   * delete the image file of the secondary structure
   * 
   * @return true if the file is deleted
   */
  public boolean deleteStructureFile()
  {
    File structureFile = new File(getStructureImageWay());
    if (structureFile.exists() == true) {
      structureFile.delete();
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * copy sourcefileway to outfileway
   * 
   * @param sourceFileWay url of the file to copy
   * @param outFileWay    url of the fiel to create
   * @return              true if the copy succeeded
   */
  private boolean copyFile(String sourceFileWay, String outFileWay)
  {
    try {
      // Get secondary structure image file
      File sourceFile = new File(sourceFileWay);
      FileInputStream fis = new FileInputStream(sourceFile);
      BufferedInputStream bis = new BufferedInputStream(fis);
      long l = sourceFile.length();

      // Prepare output flux
      File outFile = new File(outFileWay);
      FileOutputStream fos = new FileOutputStream(outFile);
      BufferedOutputStream bos = new BufferedOutputStream(fos);

      // Copy
      for (long i = 0; i < l; i++) {
        bos.write(bis.read());
      }

      // close flux
      bos.flush();
      bos.close();
      bis.close();
      return true;
    }
    catch (Exception ex) {
      System.err.println("File access error !");
      ex.printStackTrace();
      return false;
    }
  }

  /**
   * get the value of the global variable lastWindowNo (the last number of a
   * window)
   * 
   * @return the value of the global variable lastWindowNo
   */
  public static int getLastWindowNo()
  {
    return lastWindowNo;
  }

  /**
   * increment the global variable lastWindowNo
   */
  public static void incLastWindowNo()
  {
    lastWindowNo = lastWindowNo + 1;
  }

  /**
   * get the number of the current window
   * 
   * @return the number of the current window
   */
  public int getCurrentWindowNo()
  {
    return currentWindowNo;
  }

  /**
   * set the number of the current window
   * 
   * @param currentWindowNo number if the current window
   */
  public void setCurrentWindowNo(int currentWindowNo)
  {
    this.currentWindowNo = currentWindowNo;
  }

  /**
   * @return command to execute convert
   */
  public static String getConvertCommand()
  {
    String convertCommand;
    if (apollo.util.IOUtil.isWindows()) 
      convertCommand = "cmd.exe /C convert";
    else 
      convertCommand = "convert";
    return convertCommand;
  }

  /**
   * @return command to execute RNAfold
   */
  public static String getRNAfoldCommand()
  {
    return "RNAfold";
  }

  /**
   * @return name of the temporary directory of the user
   */
  public static String getTmpDir()
  {
    String tmpDirName = apollo.util.IOUtil.expandSquiggle("~/.apollo/") + "tmp/";
    return tmpDirName;
  }

  /** 
   * internal class use to draw an image 
   */
  protected class DrawImage extends JPanel
  {
    BufferedImage bufferedImage;
    int imageWidth;
    int imageHeight;

    public DrawImage()
    {
      bufferedImage = null;
      imageWidth = 0;
      imageHeight = 0;
      this.setBackground(bgColor);
    }

    /**
     * @param url url of the image
     */
    public DrawImage(String url)
    {
      replaceImage(url);
      this.setBackground(bgColor);
    }

    /**
     * replace old image by the image url 
     * @param url url of the new image
     */
    public void replaceImage(String url)
    {
      if (url.compareTo("") == 0)
        bufferedImage = null;
      else {
        try {
          bufferedImage = ImageIO.read(new File(url));
          imageWidth = bufferedImage.getWidth();
          imageHeight = bufferedImage.getHeight();
        }
        catch (IOException exception) {
          System.err.println("IO exception: " + exception);
          exception.printStackTrace();
        }
      }
    }

    public void paintComponent(Graphics g)
    {
      super.paintComponent(g);
      if (bufferedImage != null) {
        int imageX = getSize().width / 2 - imageWidth / 2;
        int imageY = getSize().height / 2 - imageHeight / 2;
        g.drawImage(bufferedImage, imageX, imageY, null);

      } else
        g.dispose();
    }
  }

}
