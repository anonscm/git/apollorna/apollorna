package apollo.gui.rna;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;


/**
 * Class which represents a frame displaying an interaction, and use to save an
 * interaction.
 */
public class InteractionViewFrame extends JFrame
{
  String seq1; // first sequence in the interaction
  String seq2; // second sequence in the interaction
  String seq1Name; // text to display about the sequence1
  String seq2Name; // text to display about the sequence2

  int[] pairs; // nucleotid i of seq1 is paired with the nucleotid
  // pairs[i] of the seq2
  JTextPane viewTextPane; // textPane displaying the interaction

  private static final Color bgColor = new Color(221, 238, 221);
  private static final Color buttonColor = Color.white;

  /**
   * Builder
   * 
   * @param seq1        First sequence in the interaction
   * @param seq1Name    Name of the first sequence in the interaction
   * @param seq2        Second sequence in the interaction
   * @param seq2Name    Name of the second sequence in the interaction
   * @param interaction Interaction between seq1 and seq2
   */
  public InteractionViewFrame(String seq1, String seq1Name, String seq2,
      String seq2Name, int[] interaction)
  {
    super("Interaction visualization");
    this.seq1     = seq1;
    this.seq1Name = seq1Name;
    this.seq2     = seq2;
    this.seq2Name = seq2Name;
    this.pairs    = interaction;

    // initialize graphic interface
    initGUI();
    this.show();
    this.setVisible(true);
  }

  /**
   * Initialize the user interface
   */
  public void initGUI()
  {
    String seq1Text = "Blue sequence: " + seq1Name + "  ";
    String seq2Text = "Red sequence: " + seq2Name + "  ";

    JLabel seq1Label = new JLabel(seq1Text, SwingConstants.LEFT);
    JLabel seq2Label = new JLabel(seq2Text, SwingConstants.LEFT);
    JPanel seq1Panel = new JPanel();
    JPanel seq2Panel = new JPanel();
    JPanel viewPanel = new JPanel();
    JPanel buttonPanel = new JPanel();
    JPanel mainPanel = new JPanel();

    JButton visualizeButton = new JButton("Configure visualization");
    JButton saveButton = new JButton("Save image as...");
    JButton closeButton = new JButton("Close");

    visualizeButton.setBackground(buttonColor);
    saveButton.setBackground(buttonColor);
    closeButton.setBackground(buttonColor);
    seq1Label.setBackground(bgColor);
    seq2Label.setBackground(bgColor);

    // draw the interaction on the jtextPane
    viewTextPane = new JTextPane();
    createView();

    buttonPanel.setBackground(bgColor);
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.add(visualizeButton);
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.add(saveButton);
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.add(closeButton);
    buttonPanel.add(Box.createHorizontalStrut(15));

    visualizeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        parameterVisualization();
      }
    });

    saveButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        JFileChooser chooser = new JFileChooser();
        int returnVal = chooser.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File file = chooser.getSelectedFile();
          if (file == null) {
            JOptionPane.showMessageDialog(null, "No file selected");
          }
          else {
            imageExport(file);
          }
        }
      }
    });

    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });

    seq1Panel.setLayout(new BoxLayout(seq1Panel, BoxLayout.X_AXIS));
    seq2Panel.setLayout(new BoxLayout(seq2Panel, BoxLayout.X_AXIS));
    seq1Panel.add(seq1Label);
    seq2Panel.add(seq2Label);
    seq1Panel.add(Box.createHorizontalGlue());
    seq2Panel.add(Box.createHorizontalGlue());
    seq1Panel.setBackground(bgColor);
    seq2Panel.setBackground(bgColor);

    viewPanel.setLayout(new BoxLayout(viewPanel, BoxLayout.X_AXIS));
    viewPanel.add(Box.createHorizontalGlue());
    viewPanel.add(viewTextPane);

    mainPanel.setBackground(bgColor);
    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(seq2Panel);
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(seq1Panel);
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(viewTextPane);
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(buttonPanel);
    mainPanel.add(Box.createVerticalStrut(15));

    this.setContentPane(mainPanel);
    this.setBackground(bgColor);
    this.pack();
  }

  /**
   * Write on the jtextpane the 3 strings which represent the interaction with
   * some special styles
   */
  private void createView()
  {
    Target target = new Target();
    // 3 String which represent the view of theinteraction
    String[] viewStrings; 
    // viewStrings[0] is the first sequence with gap
    // viewStrings [2] is the second sequence with gap
    // viewStrings [1] is the symbols to represent pairs

    // Compute the view of the interaction
    viewStrings = target.getInteractionView(this.seq1, this.seq2, this.pairs);

    viewTextPane.setEditable(true);
    viewTextPane.setText("");

    // To center text in the jtextPane
    SimpleAttributeSet attributeSet = new SimpleAttributeSet();
    StyleConstants.setAlignment(attributeSet, StyleConstants.ALIGN_CENTER);
    viewTextPane.setParagraphAttributes(attributeSet, true);

    // create style to display interaction
    StyledDocument styledDocument = viewTextPane.getStyledDocument();
    Style defaultStyle = StyleContext.getDefaultStyleContext().getStyle(
        StyleContext.DEFAULT_STYLE);
    Style regularStyle = styledDocument.addStyle("courier", defaultStyle);

    StyleConstants.setFontSize(regularStyle, 15);
    StyleConstants.setFontFamily(regularStyle, "Courier");

    Style style = styledDocument.addStyle("seq1_courier", regularStyle);
    StyleConstants.setForeground(style, Color.red);
    style = styledDocument.addStyle("seq2_courier", regularStyle);
    StyleConstants.setForeground(style, Color.blue);

    // add the three styled string int he jtextPane
    try {
      styledDocument.insertString(styledDocument.getLength(), viewStrings[0],
          styledDocument.getStyle("seq1_courier"));
      styledDocument.insertString(styledDocument.getLength(), viewStrings[1],
          styledDocument.getStyle("courier"));
      styledDocument.insertString(styledDocument.getLength(), viewStrings[2],
          styledDocument.getStyle("seq2_courier"));
    }
    catch (BadLocationException ble) {
      System.err.println("Couldn't insert initial text into text pane.");
    }
    viewTextPane.setEditable(false);
  }

  /**
   * Open a new window which is used to change characters which represent the
   * pairs symbols between paired nucleotids. Redraw the jtextpane to dispaly
   * the interaction with the new sympbols
   */
  public void parameterVisualization()
  {
    ConfigureVisuDialog visuDialog = new ConfigureVisuDialog(this);
    createView();
  }

  /**
   * Export the view of the interaction in an image
   * 
   * @param selectedFile Selected file
   */
  public void imageExport(File selectedFile)
  {
    int textPaneWidth = viewTextPane.getWidth();
    int textPaneHeight = viewTextPane.getHeight();
    BufferedImage image = new BufferedImage(textPaneWidth, textPaneHeight,
        BufferedImage.TYPE_INT_RGB);
    Graphics2D graph = image.createGraphics();

    // paint the textpane in the image
    viewTextPane.paintAll(graph);
    graph.dispose();

    try {
      // draw the graphics in a png file
      ImageIO.write(image, "png", new File(selectedFile.getAbsolutePath()
          + ".png"));
    }
    catch (Exception exception) {
      System.err.println("Image write: " + exception.getMessage());
      JOptionPane.showMessageDialog(this, "Error during image export. "
          + "Check that you're allowed writing in the selected directory.",
          "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
}
