package apollo.gui.rna;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import apollo.config.Config;
import apollo.datamodel.Range;
import apollo.util.GuiUtil;

/**
 * Class which represents the user interface to enter the parameters 
 * of the alignment based method: the scoring matrix, the opening gap penality
 * the extension gap penality and the number of resulting targets
 */
public class AlignmentSWBasedMethodPanel extends InteractionSearchMethodPanel
{
  // Variables used to save the parameters values
  private Properties historyProperties;
  private File historyFile = new File(Config.getAdapterHistoryFile());
  private static String HISTORY_KEY = "alignmentMethodProperties";
  
  // Graphical components
  // list of JTextField to represent the matrix
  private JTextField[][]  scoringMatrixTextfields;
  private JTextField      openPenalityTextField;
  private JTextField      extendPenalityTextField;
  private JTextField      targetNbTextField;

  public  static int              MATRIX_DIM = 4;
  private static final float      MATRIX_VALUE_MIN = -50;
  private static final float      MATRIX_VALUE_MAX = 50;
  private static final float      GAP_PENALITY_MIN = -50;
  private static final float      GAP_PENALITY_MAX = 0;
  private static final String     DEFAULT_OPEN_GAP_PENALITY   = "-10";
  private static final String     DEFAULT_EXTEND_GAP_PENALITY = "-2";
  public  static final float      DEFAULT_MISMATCH_VALUE = -4;
  private static final int        REGION_LENGTH_MAX = 524288;
 
  private static final float[][] DEFAULT_MATRIX_VALUES    = 
    {{DEFAULT_MISMATCH_VALUE, 5, DEFAULT_MISMATCH_VALUE, DEFAULT_MISMATCH_VALUE},
     {5, DEFAULT_MISMATCH_VALUE, 0, DEFAULT_MISMATCH_VALUE},
     {DEFAULT_MISMATCH_VALUE, 0, DEFAULT_MISMATCH_VALUE, 5},
     {DEFAULT_MISMATCH_VALUE, DEFAULT_MISMATCH_VALUE, 5, DEFAULT_MISMATCH_VALUE}};

  private final Dimension textfieldDim = new Dimension(45, 25);
  
  
  public AlignmentSWBasedMethodPanel(InteractionSearchSetDialog parent)
  {
    super(parent);
    historyProperties = new Properties();
    initGUI();
  }

  public void initGUI()
  {
    JPanel paramPanel   = createParamPanel();
    JPanel actionPanel  = createButtonPanel();

    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    this.add(Box.createVerticalStrut(5));
    this.add(paramPanel);
    this.add(Box.createVerticalStrut(5));
    this.add(actionPanel);
    this.add(Box.createVerticalStrut(5));
    this.setBackground(bgColor);
    
    // Build the window displaying the details about the fold method
    buildInformationDialog(createInfoDocument(), "Info: alignment based method");
  }
  
  /**
   * Create a panel containing parameters fields of alignment based method
   * 
   * @return Panel containing parameters fields of alignment based method
   */
  public JPanel createParamPanel()
  {
    // creation of the four panels composing the paramPanel
    JPanel scorePanel     = createScorePanel();
    JPanel openGapPanel   = createOpenGapPanel();
    JPanel extendGapPanel = createExtendGapPanel();
    JPanel targetNbPanel  = createTargetNbPanel();

    // creation of the global panel
    JPanel paramPanel = new JPanel();
    paramPanel.setLayout(new BoxLayout(paramPanel, BoxLayout.Y_AXIS));
    paramPanel.add(scorePanel);
    paramPanel.add(Box.createVerticalStrut(5));
    paramPanel.add(openGapPanel);
    paramPanel.add(Box.createVerticalStrut(5));
    paramPanel.add(extendGapPanel);
    paramPanel.add(Box.createVerticalStrut(5));
    paramPanel.add(targetNbPanel);
    
    // set the color background
    paramPanel.setBackground(bgColor);
    for (int i = 0; i < paramPanel.getComponentCount(); i++) {
      paramPanel.getComponent(i).setBackground(bgColor);
    }
    return paramPanel;
  }
  
  
  /**
   * Create the score panel, composed of the scoring matrix.
   * 
   * @return The score panel, composed of the scoring matrix 
   */
  private JPanel createScorePanel()
  {
    // Creation of the component scoreMatrixTitlePanel
    JLabel scoreMatrixLabel = new JLabel("Score matrix (values: [" + 
        MATRIX_VALUE_MIN + " ; " + MATRIX_VALUE_MAX + "])");
    JPanel scoreMatrixTitlePanel = new JPanel();
    scoreMatrixTitlePanel.setBackground(bgColor);
    scoreMatrixTitlePanel.setLayout(new BoxLayout(scoreMatrixTitlePanel, 
        BoxLayout.Y_AXIS));
    scoreMatrixTitlePanel.add(scoreMatrixLabel);
    scoreMatrixTitlePanel.add(Box.createVerticalGlue());
   
    // Creation of the component matrixPanel
    JPanel matrixPanel = new JPanel();
    matrixPanel.setLayout(new GridLayout(5, 5, 5, 5));
    matrixPanel.setBackground(bgColor);
    matrixPanel.add(new JLabel(""));
    matrixPanel.add(new JLabel("A", SwingConstants.CENTER));
    matrixPanel.add(new JLabel("T", SwingConstants.CENTER));
    matrixPanel.add(new JLabel("G", SwingConstants.CENTER));
    matrixPanel.add(new JLabel("C", SwingConstants.CENTER));
    
    scoringMatrixTextfields = new JTextField[MATRIX_DIM][MATRIX_DIM];
    for (int i= 0; i < MATRIX_DIM; i++) {
      if (i == 0) {
        matrixPanel.add(new JLabel("A", SwingConstants.RIGHT));
      }
      else if (i == 1) {
        matrixPanel.add(new JLabel("T", SwingConstants.RIGHT));
      }
      else if (i == 2) {
        matrixPanel.add(new JLabel("G", SwingConstants.RIGHT));
      }
      else {
        matrixPanel.add(new JLabel("C", SwingConstants.RIGHT));
      }
      
      for (int j = 0; j < MATRIX_DIM; j++) {
        scoringMatrixTextfields[i][j] = new JTextField();
        matrixPanel.add(scoringMatrixTextfields[i][j]);
        scoringMatrixTextfields[i][j].setHorizontalAlignment(JTextField.CENTER);
      }
    }
   
    //  creation of scorePanel 
    JPanel scorePanel = new JPanel();
    scorePanel.setLayout(new BoxLayout(scorePanel, BoxLayout.X_AXIS));
    scorePanel.add(scoreMatrixTitlePanel);
    scorePanel.add(matrixPanel);
    scorePanel.add(Box.createHorizontalGlue());
    
    return scorePanel;
  }
  
  /**
   * Create the open gap panel
   * 
   * @return The open gap panel
   */
  private JPanel createOpenGapPanel()
  {
  // creation of openGapPanel components
  openPenalityTextField = new JTextField();
  openPenalityTextField.setPreferredSize(textfieldDim);
  openPenalityTextField.setMaximumSize(textfieldDim);
  JLabel openPenalityLabel    = new JLabel("Gap opening penality");
  JLabel openPenalityComment  = new JLabel("[" + GAP_PENALITY_MIN + " ; "
      + GAP_PENALITY_MAX + "]");
  
  // creation of openGapPanel
  JPanel openGapPanel = new JPanel();
  openGapPanel.setLayout(new BoxLayout(openGapPanel, BoxLayout.X_AXIS));
  openGapPanel.add(openPenalityLabel);
  openGapPanel.add(Box.createHorizontalStrut(10));
  openGapPanel.add(openPenalityTextField);
  openGapPanel.add(Box.createHorizontalStrut(10));
  openGapPanel.add(openPenalityComment);
  openGapPanel.add(Box.createHorizontalGlue());
  
  return openGapPanel;
  }
  
  /**
   * Create the extend gap panel
   * 
   * @return The extend gap panel
   */
  private JPanel createExtendGapPanel()
  {
    //  creation of extendGapPanel components
    extendPenalityTextField = new JTextField();
    extendPenalityTextField.setPreferredSize(textfieldDim);
    extendPenalityTextField.setMaximumSize(textfieldDim);
    JLabel extendPenalityLabel    = new JLabel("Gap extension penality");
    JLabel extendPenalityComment  = new JLabel("[" + GAP_PENALITY_MIN + " ; "
        + GAP_PENALITY_MAX + "]");
    
    // creation of extendGapPanel
    JPanel extendGapPanel = new JPanel();
    extendGapPanel.setLayout(new BoxLayout(extendGapPanel, BoxLayout.X_AXIS));
    extendGapPanel.add(extendPenalityLabel);
    extendGapPanel.add(Box.createHorizontalStrut(10));
    extendGapPanel.add(extendPenalityTextField);
    extendGapPanel.add(Box.createHorizontalStrut(10));
    extendGapPanel.add(extendPenalityComment);
    extendGapPanel.add(Box.createHorizontalGlue());
    
    return extendGapPanel;
  }
  
  /**
   * Cretae the target number panel
   * 
   * @return The target number panel
   */
  private JPanel createTargetNbPanel()
  {
    // creation of targetNbPanel components
    targetNbTextField = GuiUtil.makeNumericTextField();
    targetNbTextField.setPreferredSize(textfieldDim);
    targetNbTextField.setMaximumSize(textfieldDim);
    JLabel targetNbLabel   = new JLabel("Maximum number of predicted targets");
    JLabel targetNbComment = new JLabel("[" + TARGET_NB_MIN + " ; "
        + TARGET_NB_MAX + "]");
    
    // creation of targetNbPanel 
    JPanel targetNbPanel = new JPanel();
    targetNbPanel.setLayout(new BoxLayout(targetNbPanel, BoxLayout.X_AXIS));
    targetNbPanel.add(targetNbLabel);
    targetNbPanel.add(Box.createHorizontalStrut(10));
    targetNbPanel.add(targetNbTextField);
    targetNbPanel.add(Box.createHorizontalStrut(10));
    targetNbPanel.add(targetNbComment);
    targetNbPanel.add(Box.createHorizontalGlue());
    
    return targetNbPanel;
  }
  
  /**
   * Create a panel containing the button "Info: alignment based method"
   * and the button "Set default value"
   * 
   * @return A panel composed of an information button
   */
  public JPanel createButtonPanel()
  {
    JPanel buttonPanel = new JPanel();
    buttonPanel.setBackground(bgColor);
    
    JButton moreButton            = new JButton("Info: alignment based method");
    JButton setDefaultValueButton = new JButton("Reset default value parameters");
    JButton saveParametersButton  = new JButton("Save parameters");
    
    moreButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        aboutDialog.setVisible(true);
      }
    });
    setDefaultValueButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        setDefautValueParameters();
      }
    });
    saveParametersButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        saveParameters();
      }
    });
    
    // add buttons to the panel
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.add(Box.createHorizontalStrut(10));
    buttonPanel.add(moreButton);
    buttonPanel.add(Box.createHorizontalStrut(10));
    buttonPanel.add(setDefaultValueButton);
    buttonPanel.add(Box.createHorizontalStrut(10));
    buttonPanel.add(saveParametersButton);
    buttonPanel.add(Box.createHorizontalStrut(10));
    
    return buttonPanel;
  }

  /**
   * Initialize parameters of the method: window size, shift, linker, number of
   * targets. Load the saved values or if no value was saved, initialize the parameters
   * with default values.
   */
  public void initParameters()
  {
    // Initialize the scoring matrix
    try {
      historyProperties.load(new FileInputStream(historyFile));
    }
    catch(IOException ie) {
      System.out.println(ie.getMessage());
    }
    // Check that some values were saved
    String savedValues = historyProperties.getProperty(HISTORY_KEY);
    
    if (savedValues != null) {
      String[] values = savedValues.split("%");
      for (int i = 0; i < MATRIX_DIM; i++) {
        for (int j = 0; j < MATRIX_DIM; j++) {
          scoringMatrixTextfields[i][j].setText(values[i * MATRIX_DIM + j]);
        }
      }
      openPenalityTextField.setText(values[values.length - 3]);
      extendPenalityTextField.setText(values[values.length - 2]);
      targetNbTextField.setText(values[values.length - 1]);
    } 
    else { 
      setDefautValueParameters();
    }
  }

  /**
   * Initialize parameters of the method: window size, shift, linker, number of
   * targets with default values
   */
  private void setDefautValueParameters()
  {
    for (int i = 0; i < MATRIX_DIM; i++) {
      for (int j = 0; j < MATRIX_DIM; j++) {
        scoringMatrixTextfields[i][j].setText(
          Float.toString(DEFAULT_MATRIX_VALUES[i][j]));
      }
    }
    // Initialize the penality of open gap
    openPenalityTextField.setText(DEFAULT_OPEN_GAP_PENALITY);
    // Initialize the penality of extend gap
    extendPenalityTextField.setText(DEFAULT_EXTEND_GAP_PENALITY);
    // Initialize the number of search target
    targetNbTextField.setText(DEFAULT_TARGET_NB);
    
  }
  
  /* Check that the parameter fields value are correct according to the specification
   * @see apollo.gui.rna.InteractionSearchMethodPanel#checkParameters()
   */
  public boolean checkParameters()
  {
    String message = "";
    String score;
    boolean isCorrect = true;


    // check that the matrix is full and that its values are corrects
    for (int i = 0; i < MATRIX_DIM;  i++) {
      for (int j = 0; j < MATRIX_DIM; j++) {
        score = scoringMatrixTextfields[i][j].getText();
        if ( (score.compareTo("") == 0) || 
             (Float.parseFloat(score) < MATRIX_VALUE_MIN) ||
             (Float.parseFloat(score) > MATRIX_VALUE_MAX) ) {
          message   = "Not allowed value in the scoring matrix.\n";
          isCorrect = false;
        }
      } 
    } 
    
    // check that all the parameters have a value 
    if ( (openPenalityTextField.getText().compareTo("")   == 0) || 
         (extendPenalityTextField.getText().compareTo("") == 0) ||
         (targetNbTextField.getText().compareTo("")       == 0) ) {
      message   = message.concat("All method parameters have to have a value.\n");
      isCorrect = false;
    }
    else {
      // Check that the open gap penality value is correct 
      float openValue = Float.parseFloat(openPenalityTextField.getText());
      if ((openValue < GAP_PENALITY_MIN) || (openValue > GAP_PENALITY_MAX)) {
        message = message
            .concat("Gap opening penality has to be included between "
                + GAP_PENALITY_MIN + " and " + GAP_PENALITY_MAX + ".\n");
        isCorrect = false;
      }
      // Check that the extend gap penality value is correct 
      float extendValue = Float.parseFloat(extendPenalityTextField.getText());
      if ((extendValue < GAP_PENALITY_MIN) || (extendValue > GAP_PENALITY_MAX)) {
        message = message
            .concat("Gap extension penality has to be included between "
                + GAP_PENALITY_MIN + " and " + GAP_PENALITY_MAX + ".\n");
        isCorrect = false;
      }
      
      // Check that the number of wished targets is correct
      int targetNb = Integer.parseInt(targetNbTextField.getText());
      if ((targetNb < TARGET_NB_MIN) || (targetNb > TARGET_NB_MAX)) {
        message = message
            .concat("The number of targets has to be included between "
                + TARGET_NB_MIN + " and " + TARGET_NB_MAX + ".\n");
        isCorrect = false;
      }
    }
    //  if a parameter is incorrect, display the error
    if (isCorrect == false) {
      JOptionPane.showMessageDialog(this, message, "Error",
          JOptionPane.ERROR_MESSAGE);
    }
    
    return isCorrect;
  }
  
  /* Check that the search regions have size lower than the max allow size
   * Return false if not
   * @see apollo.gui.rna.InteractionSearchMethodPanel#allowResearch()
   */
  public boolean allowResearch()
  {
    boolean isAllowed = true;
    ArrayList regions = parentDialog.getInteractionSearch().regionFind(
        parentDialog.getRegionsParams());
    
    // check that the size of search regions are not too long
    if (hasLongRegion(regions) == true) {
      JOptionPane.showMessageDialog(this, 
          "A search region is too long to compute Smith & Waterman.", "Error",
          JOptionPane.ERROR_MESSAGE);
      isAllowed = false;
    } 
    
    return isAllowed;
  }
   
  /**
   * Return true if a region of the list has a size higher than REGION_LENGTH_MAX 
   * 
   * @param regions List of regions
   * @return        True if one region has a size higher than REGION_LENGTH_MAX 
   */
  private boolean hasLongRegion(ArrayList regions)
  {
    Range   aRegion;
    boolean isFinish      = false; // true to go out the loop
    boolean hasLongRegion = false; // true if there is a too long region
    int     indexRegion   = 0; 
    
    while (isFinish == false) {
      // if all the region were scanned
      if (indexRegion >= regions.size() -1) {
        isFinish = true;
      }
      else {
        aRegion = (Range)regions.get(indexRegion);
        // if the region length is longer than the max
        if (aRegion.getResidues().length() >  REGION_LENGTH_MAX) {
          hasLongRegion = true;
          isFinish      = true;
        }
      }
      indexRegion++;
    }
    
    return hasLongRegion;
  }
  
  /* 
   * Create and return an ordering list composed of the method parameters: 
   * the scoring matrix, the open gap penality, the extend gap penality 
   * and the target number.
   * 
   * @see apollo.gui.rna.InteractionSearchMethodPanel#getParameters()
   */
  public ArrayList getParameters()
  {
    ArrayList paramList = new ArrayList();
    float[][] matrix = new float[MATRIX_DIM][MATRIX_DIM];
    
    // add the scoring matrix to the list
    for (int i = 0; i < MATRIX_DIM; i++) {
      for (int j = 0; j < MATRIX_DIM; j++) {
        matrix[i][j] = Float.parseFloat(scoringMatrixTextfields[i][j].getText());
      }
    }
    paramList.add(matrix);
    paramList.add(new Float(openPenalityTextField.getText()));
    paramList.add(new Float(extendPenalityTextField.getText()));
    paramList.add(new Integer(targetNbTextField.getText()));

    return paramList;
  }

  /* 
   * @see apollo.gui.rna.InteractionSearchMethodPanel#createInfoDocument()
   */
  public StyledDocument createInfoDocument()
  {
    String title    = "Info: alignment based method\n\n";
    String message  = "The request sequence is aligned with each defined " +
        "region using the Smith and Waterman alignment algorithm " +
        "adapted from JAligner\n[Ahmed Moustafa, JAligner: " +
        "Open source Java implementation of Smith-Waterman, " +
        "http://jaligner.sourceforge.net].\nThe default scoring " +
        "matrix is defined to look for an interaction between the " +
        "two sequences instead of an alignment.\n" +
        "Thus the A-T, G-C and G-T matchs have positive default values.\n\n" +
        "The method extracts one potential target per region. " +
        "Targets which have the best alignment score are stored.\n\n" +
        "The parameters of the methods are:\n" +
        "* the scoring matrix (values between " + MATRIX_VALUE_MIN + 
        " and " + MATRIX_VALUE_MAX + ")\n" +
        "* the open gap penality (between " + GAP_PENALITY_MIN + 
        " and " + GAP_PENALITY_MAX + ")\n" +
        "* the extend gap penality (between " + GAP_PENALITY_MIN + 
        " and " + GAP_PENALITY_MAX + ")\n" +
        "* the maximum number of potential targets that are stored " +
        "(between "+ TARGET_NB_MIN + " and " + TARGET_NB_MAX + ").\n\n" +
        "The variable of the method shown in the result list of predicted " +
        "targets is the score of the alignment.";
    
    // define styles 
    StyledDocument doc = new DefaultStyledDocument();

    Style defaultStyle = StyleContext.getDefaultStyleContext().getStyle(
        StyleContext.DEFAULT_STYLE);
    StyleConstants.setFontSize(defaultStyle, 13);
    
    Style titleStyle = doc.addStyle("standard", defaultStyle);
    StyleConstants.setBold(titleStyle, true);
    StyleConstants.setFontSize(titleStyle, 14);

    // write on the document
    try {
      doc.insertString(doc.getLength(), title, titleStyle);
      doc.insertString(doc.getLength(), message, defaultStyle);

    }
    catch (Exception e) {
      System.err.println(e);
    }
    
    return doc;
  }
  
  /**
   * Save the current method parameter values. 
   */
  private void saveParameters()
  {
    // if the parameters values are corrects 
    if (checkParameters() == true) {
      String alignParameters = new String();
      // save the matrix values
      for (int i = 0; i < MATRIX_DIM; i++) {
        for (int j = 0; j < MATRIX_DIM; j++) {
          alignParameters = alignParameters.concat(
              scoringMatrixTextfields[i][j].getText() + "%"); 
        }
      }
      alignParameters = alignParameters.concat(
          openPenalityTextField.getText() 
          + "%" + extendPenalityTextField.getText() 
          + "%" + targetNbTextField.getText());

      // Save the current parameters in the history file
      try {
        historyProperties.setProperty(HISTORY_KEY, alignParameters);
        historyProperties.store(new FileOutputStream(historyFile), 
            "DataAdapterChooser properties file");
      } 
      catch(IOException ie) {
        System.out.println(ie.getMessage());
      }
    }
  }
  
}
