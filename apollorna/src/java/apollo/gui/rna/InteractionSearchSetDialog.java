package apollo.gui.rna;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import apollo.datamodel.FeatureSet;
import apollo.datamodel.Range;
import apollo.datamodel.SeqFeature;
import apollo.datamodel.Sequence;
import apollo.datamodel.SequenceI;
import apollo.gui.ApolloFrame;
import apollo.gui.ThreadWait;
import apollo.gui.synteny.GuiCurationState;
import apollo.util.GuiUtil;


/**
 * Class which represents a window to parameter the search of targets which can
 * interact with the selected sequence. User enters parameters about the genomic
 * regions where look for the targets parameters af a method to look for
 * interactions, At this moment, only one method is proposed We built this
 * window in the way to propose in the future new search interaction methods.
 */
public class InteractionSearchSetDialog extends JDialog
{
  private int genomicStart; // start position of the genomic sequence
  private int genomicEnd; // stop position of the genomic sequence
  private SequenceI genomicSequence; // genomic sequence
  private SeqFeature selectedSequence; // selected sequence
  private Sequence requestSequence; // request sequence
  private InteractionSearch interactionSearch; // manager of the interaction
                                                // research
  private GuiCurationState curationState;
  // graphic components
  private JRadioButton selectedRadioButton;
  private JRadioButton positionRadioButton;
  private JRadioButton sequenceRadioButton;
  private JCheckBox positionCheckBox;
  private JCheckBox definedRegionCheckBox;
  private JCheckBox startCheckBox;
  private JCheckBox stopCheckBox;
  private JCheckBox strandCheckBox;
  private JCheckBox intergenicCheckBox;
  private JTextField sequenceTextField;
  private JTextField requestPositionTextField;
  private JTextField requestLengthTextField;
  private JTextField startPositionTextField;
  private JTextField stopPositionTextField;
  private JTextField definedRegionTextField;
  private JTextField startBeforeTextField;
  private JTextField startAfterTextField;
  private JTextField stopBeforeTextField;
  private JTextField stopAfterTextField;
  private JComboBox requestStrandComboBox;
  private JComboBox strandComboBox;
  private JTabbedPane methodPane;
  private JDialog informationDialog;

  // static variables about the graphic window apparence
  static final Color bgColor = new Color(221, 238, 221);
  static final Color panelColor = new Color(221, 238, 230);
  static final Color buttonColor = Color.white;
  static final Dimension textfieldDim = new Dimension(70, 25);
  static final Dimension comboBoxDim = new Dimension(70, 25);
  static final String[] strandData = {"+/-", "+", "-"};

  private final int MAX_AROUND_CODON = 300;

  /**
   * Builder
   * 
   * @param apolloFrame main frame of apollo
   * @param seq         selected sequence
   */
  public InteractionSearchSetDialog(ApolloFrame apolloFrame,
      GuiCurationState curationState)
  {
    super(apolloFrame, "Search RNA/RNA interactions", true);
    boolean isCorrect = true;
    this.curationState = curationState;
    this.requestSequence = null;

    // if a sequence is selected
    if (this.curationState.getSelectionManager().getSelection()
        .getConsolidatedFeatures().size() > 0) {
      selectedSequence = (SeqFeature) this.curationState.getSelectionManager()
          .getSelection().getConsolidatedFeatures().getFeature(0);

      // check that the selected sequence has a size included between
      // SEQ_SIZE_MIN and SEQ_SIZE_MAX
      int selectedSize = selectedSequence.getResidues().length();
      if (   (selectedSize < InteractionSearch.getSizeMin())
          || (selectedSize > InteractionSearch.getSizeMax())) {
        String message = "The sequence size has to be included between "
            + InteractionSearch.getSizeMin() + " and "
            + InteractionSearch.getSizeMax() + ".\n";
        JOptionPane.showMessageDialog(this, message, "Error",
            JOptionPane.ERROR_MESSAGE);
        this.dispose();
        isCorrect = false;
      }
    }
    else {
      selectedSequence = null;
    }

    if (isCorrect == true) {
      // create new manager for the research of interactions
      interactionSearch = new InteractionSearch();

      // get genomic sequence positions
      genomicSequence = this.curationState.getCurationSet().getRefSequence();
      genomicStart    = this.curationState.getCurationSet().getRefSequence()
          .getRange().getStart();
      genomicEnd      = this.curationState.getCurationSet().getRefSequence()
          .getRange().getEnd();

      // initialize graphic interface
      initGUI();

      // initialize parameters
      initParameters();

      this.setVisible(true);
    }
  }

  /**
   * Initialize the user interface
   */
  public void initGUI()
  {
    // create the three panels composing mainPanel
    JPanel sequencePanel      = createSequencePanel();
    JPanel searchRegionPanel  = createRegionPanel();
    JTabbedPane methodPane    = createMethodPane();
    JPanel buttonPanel        = createButtonPanel();

    // create the main panel
    JPanel mainPanel = new JPanel();
    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    mainPanel.add(Box.createVerticalStrut(5));
    mainPanel.add(sequencePanel);
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(searchRegionPanel);
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(methodPane);
    mainPanel.add(Box.createVerticalStrut(15));
    mainPanel.add(buttonPanel);
    mainPanel.add(Box.createVerticalStrut(5));
    mainPanel.setBackground(bgColor);
    this.setContentPane(mainPanel);

    // create the documentation dialog
    buildInformationDialog(createDocument());

    this.pack();
  }

  /**
   * Create and return the request sequence panel
   * 
   * @return The request sequence panel
   */
  private JPanel createSequencePanel()
  {
    String[] strand = {"+", "-"};
    String lengthDefinition = " [" + InteractionSearch.getSizeMin() + " ; "
        + InteractionSearch.getSizeMax() + "]";

    // create the selected panel
    JPanel selectedPanel = new JPanel();
    selectedRadioButton = new JRadioButton("Selected sequence");
    selectedRadioButton.setBackground(panelColor);
    selectedPanel.setLayout(new BoxLayout(selectedPanel, BoxLayout.X_AXIS));
    selectedPanel.setBackground(panelColor);
    selectedPanel.add(selectedRadioButton);
    selectedPanel.add(Box.createHorizontalGlue());

    // create the position panel
    JPanel positionPanel = new JPanel();
    positionRadioButton = new JRadioButton("");
    positionRadioButton.setBackground(panelColor);
    requestPositionTextField  = GuiUtil.makeNumericTextField();
    requestLengthTextField    = GuiUtil.makeNumericTextField();
    requestPositionTextField.setPreferredSize(textfieldDim);
    requestPositionTextField.setMaximumSize(textfieldDim);
    requestLengthTextField.setPreferredSize(textfieldDim);
    requestLengthTextField.setMaximumSize(textfieldDim);
    requestStrandComboBox = new JComboBox(strand);
    requestStrandComboBox.setPreferredSize(comboBoxDim);
    requestStrandComboBox.setMaximumSize(comboBoxDim);

    positionPanel.setLayout(new BoxLayout(positionPanel, BoxLayout.X_AXIS));
    positionPanel.setBackground(panelColor);
    positionPanel.add(positionRadioButton);
    positionPanel.add(new JLabel("Position  "));
    positionPanel.add(requestPositionTextField);
    positionPanel.add(new JLabel("     Length (in nucleotides) "));
    positionPanel.add(requestLengthTextField);
    positionPanel.add(new JLabel(lengthDefinition));
    positionPanel.add(new JLabel("     Strand "));
    positionPanel.add(requestStrandComboBox);
    positionPanel.add(Box.createHorizontalGlue());

    // create the sequence panel
    JPanel sequencePanel = new JPanel();
    sequenceRadioButton = new JRadioButton(
        "Nucleotidic sequence, composed of A, T, G, C. Length "
            + lengthDefinition);
    sequenceRadioButton.setBackground(panelColor);
    sequencePanel.setLayout(new BoxLayout(sequencePanel, BoxLayout.X_AXIS));
    sequencePanel.setBackground(panelColor);
    sequencePanel.add(sequenceRadioButton);
    sequencePanel.add(Box.createHorizontalGlue());

    // create the textField sequence panel
    JPanel textFieldSequencePanel = new JPanel();
    sequenceTextField = new JTextField();
    textFieldSequencePanel.setLayout(new BoxLayout(textFieldSequencePanel,
        BoxLayout.X_AXIS));
    textFieldSequencePanel.setBackground(panelColor);
    textFieldSequencePanel.add(sequenceTextField);

    // create the buttonGroup to group the three buttons
    ButtonGroup requestButtonGroup = new ButtonGroup();
    requestButtonGroup.add(selectedRadioButton);
    requestButtonGroup.add(positionRadioButton);
    requestButtonGroup.add(sequenceRadioButton);

    // create the request sequence panel
    JPanel requestPanel = new JPanel();
    TitledBorder seqBorder = BorderFactory
        .createTitledBorder("Specifications of the request sequence");
    requestPanel.setBorder(seqBorder);
    requestPanel.setLayout(new BoxLayout(requestPanel, BoxLayout.Y_AXIS));
    requestPanel.setBackground(panelColor);
    requestPanel.add(selectedPanel);
    requestPanel.add(Box.createVerticalStrut(5));
    requestPanel.add(positionPanel);
    requestPanel.add(Box.createVerticalStrut(5));
    requestPanel.add(sequencePanel);
    requestPanel.add(textFieldSequencePanel);
    requestPanel.add(Box.createVerticalStrut(5));

    return requestPanel;
  }

  /**
   * Create and return the region panel about specifications of search regions
   * 
   * @return Panel about specifications of search regions
   */
  private JPanel createRegionPanel()
  {
    // help panel
    JPanel helpPanel = new JPanel();
    JTextPane helpPane = new JTextPane();
    String helpMessage = "The explored regions have to be characterized by the below "
        + "properties.\n"
        + "Only the regions that satisfy ALL the selected properties are explored";

    StyledDocument document = helpPane.getStyledDocument();
    Style defaultStyle = StyleContext.getDefaultStyleContext().getStyle(
        StyleContext.DEFAULT_STYLE);
    Style newStyle = document.addStyle("courier", defaultStyle);
    StyleConstants.setFontSize(newStyle, 12);
    StyleConstants.setBold(newStyle, true);
    StyleConstants.setAlignment(newStyle, StyleConstants.ALIGN_LEFT);

    try {
      document.insertString(document.getLength(), helpMessage, newStyle);
    }
    catch (BadLocationException e1) {
      e1.printStackTrace();
    }
    helpPane.setEditable(false);
    helpPane.setBackground(panelColor);

    JButton helpButton = new JButton("Info: Search regions");
    helpButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        informationDialog.setVisible(true);
      }
    });

    helpPanel.setLayout(new BoxLayout(helpPanel, BoxLayout.X_AXIS));
    helpPanel.add(helpPane);
    helpPanel.add(helpButton);

    // genomic position panel
    JPanel positionPanel = new JPanel();
    JLabel toLabel = new JLabel("  to  ");
    positionCheckBox = new JCheckBox("Genomic positions ");
    startPositionTextField = GuiUtil.makeNumericTextField();
    stopPositionTextField = GuiUtil.makeNumericTextField();

    positionCheckBox.setBackground(panelColor);
    startPositionTextField.setPreferredSize(textfieldDim);
    startPositionTextField.setMaximumSize(textfieldDim);
    stopPositionTextField.setPreferredSize(textfieldDim);
    stopPositionTextField.setMaximumSize(textfieldDim);

    positionPanel.setLayout(new BoxLayout(positionPanel, BoxLayout.X_AXIS));
    positionPanel.add(positionCheckBox);
    positionPanel.add(startPositionTextField);
    positionPanel.add(toLabel);
    positionPanel.add(stopPositionTextField);
    positionPanel.add(Box.createHorizontalGlue());

    // defined region panel
    JPanel definedRegionPanel = new JPanel();
    definedRegionCheckBox = new JCheckBox("Defined regions ");
    definedRegionTextField = new JTextField();

    definedRegionCheckBox.setBackground(panelColor);
    definedRegionPanel.setLayout(new BoxLayout(definedRegionPanel,
        BoxLayout.X_AXIS));
    definedRegionPanel.add(definedRegionCheckBox);
    definedRegionPanel.add(definedRegionTextField);

    // start codon panel
    startCheckBox = new JCheckBox("Around start codon: ");
    startBeforeTextField = GuiUtil.makeNumericTextField();
    startAfterTextField   = GuiUtil.makeNumericTextField();
    JPanel startCodonPanel = createCodonPanel(startCheckBox,
        startBeforeTextField, startAfterTextField, textfieldDim);

    // stop codon panel
    stopCheckBox = new JCheckBox("Around stop codon: ");
    stopBeforeTextField = GuiUtil.makeNumericTextField();
    stopAfterTextField  = GuiUtil.makeNumericTextField();
    JPanel stopCodonPanel = createCodonPanel(stopCheckBox, stopBeforeTextField,
        stopAfterTextField, textfieldDim);

    // strand panel
    JPanel strandPanel = new JPanel();
    strandCheckBox = new JCheckBox("Strand");
    strandComboBox = new JComboBox(strandData);

    strandCheckBox.setBackground(panelColor);
    strandComboBox.setSelectedIndex(0);
    strandComboBox.setPreferredSize(comboBoxDim);
    strandComboBox.setMaximumSize(comboBoxDim);

    strandPanel.setLayout(new BoxLayout(strandPanel, BoxLayout.X_AXIS));
    strandPanel.add(strandCheckBox);
    strandPanel.add(strandComboBox);
    strandPanel.add(Box.createHorizontalGlue());

    // intergenic panel
    JPanel igPanel = new JPanel();
    intergenicCheckBox = new JCheckBox("Intergenic regions");
    intergenicCheckBox.setBackground(panelColor);

    igPanel.setLayout(new BoxLayout(igPanel, BoxLayout.X_AXIS));
    igPanel.add(intergenicCheckBox);
    igPanel.add(Box.createHorizontalGlue());

    // create the search region panel
    JPanel searchRegionPanel = new JPanel();
    TitledBorder regionBorder = BorderFactory
        .createTitledBorder("Specifications of search regions");

    // add components to the search region panel
    searchRegionPanel.setLayout(new BoxLayout(searchRegionPanel,
        BoxLayout.Y_AXIS));
    searchRegionPanel.setBorder(regionBorder);
    searchRegionPanel.add(helpPanel);
    searchRegionPanel.add(Box.createVerticalStrut(20));
    searchRegionPanel.add(positionPanel);
    searchRegionPanel.add(Box.createVerticalStrut(5));
    searchRegionPanel.add(definedRegionPanel);
    searchRegionPanel.add(Box.createVerticalStrut(5));
    searchRegionPanel.add(startCodonPanel);
    searchRegionPanel.add(Box.createVerticalStrut(5));
    searchRegionPanel.add(stopCodonPanel);
    searchRegionPanel.add(Box.createVerticalStrut(5));
    searchRegionPanel.add(strandPanel);
    searchRegionPanel.add(Box.createVerticalStrut(5));
    searchRegionPanel.add(igPanel);

    // color the background
    searchRegionPanel.setBackground(panelColor);
    for (int i = 0; i < searchRegionPanel.getComponentCount(); i++) {
      searchRegionPanel.getComponent(i).setBackground(panelColor);
    }

    // return search region panel
    return searchRegionPanel;
  }

  /**
   * Create and return a panel composed of a checkbox, and two textfields used
   * to enter the number of whished nucleotids before and after a codon All the
   * components are disposed on a same line
   * 
   * @param checkBox        The checkbox which is included at the begin of the line
   * @param beforeTextField First textfield
   * @param afterTextField  Second textfield
   * @param dim             Dimension of the textfields
   * @return                The built JPanel object
   */
  private JPanel createCodonPanel(JCheckBox checkBox,
      JTextField beforeTextField, JTextField afterTextField, Dimension dim)
  {
    JPanel codonPanel = new JPanel();
    JLabel minusLabel = new JLabel("   - ");
    JLabel plusLabel  = new JLabel("   + ");
    JLabel nt1Label   = new JLabel(" nt  ");
    JLabel nt2Label   = new JLabel(" nt  ");

    beforeTextField.setPreferredSize(dim);
    beforeTextField.setMaximumSize(dim);
    afterTextField.setPreferredSize(dim);
    afterTextField.setMaximumSize(dim);
    checkBox.setBackground(panelColor);

    codonPanel.setLayout(new BoxLayout(codonPanel, BoxLayout.X_AXIS));
    codonPanel.add(checkBox);
    codonPanel.add(minusLabel);
    codonPanel.add(beforeTextField);
    codonPanel.add(nt1Label);
    codonPanel.add(plusLabel);
    codonPanel.add(afterTextField);
    codonPanel.add(nt2Label);
    codonPanel.add(Box.createHorizontalGlue());

    return codonPanel;
  }

  /**
   * Create and return a pane composed of a set of panels, one per interaction
   * search method.
   * 
   * @return A tabbed pane composed of a set of panels, one per interaction
   *         search method
   */
  private JTabbedPane createMethodPane()
  {
    methodPane = new JTabbedPane();
    TitledBorder regionBorder = BorderFactory
        .createTitledBorder("Specifications of the search procedure");
    InteractionSearchMethodPanel method1Panel = new FoldBasedMethodPanel(this);
    InteractionSearchMethodPanel method2Panel = new AlignmentSWBasedMethodPanel(this);
    
    methodPane.setBackground(panelColor);
    methodPane.setBorder(regionBorder);
    methodPane.add("Fold based method", method1Panel);
    methodPane.add("Alignment based method", method2Panel);
    return methodPane;
  }

  /**
   * Create and return a panel composed of two buttons.
   * 
   * @return Panel composed of two buttons
   */
  private JPanel createButtonPanel()
  {
    JPanel buttonPanel    = new JPanel();
    JButton cancelButton  = new JButton("Cancel");
    JButton searchButton  = new JButton("Search");
    cancelButton.setBackground(buttonColor);
    searchButton.setBackground(buttonColor);

    // add listener about buttons
    searchButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        interactionSearchCall();
      }
    });

    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        windowExit();
      }
    });

    buttonPanel.setBackground(bgColor);
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.add(cancelButton);
    buttonPanel.add(Box.createHorizontalStrut(15));
    buttonPanel.add(searchButton);

    return buttonPanel;
  }

  /**
   * Initialize parameters to determine search regions
   */
  public void initParameters()
  {
    // default selected checkboxes
    positionCheckBox.setSelected(true);
    strandCheckBox.setSelected(true);
    // always selected
    positionCheckBox.setEnabled(false);
    strandCheckBox.setEnabled(false);

    // initialize the genomic positions
    startPositionTextField.setText(new Integer(genomicStart).toString());
    stopPositionTextField.setText(new Integer(genomicEnd).toString());

    // if no sequence was selected, disable the selected radio button
    if (selectedSequence == null) {
      selectedRadioButton.setSelected(false);
      selectedRadioButton.setEnabled(false);
      positionRadioButton.setSelected(true);
    }
    else {
      // select the selected radio button
      selectedRadioButton.setSelected(true);
      // fill the sequence field with the nucleotidic sequence
      sequenceTextField.setText(selectedSequence.getResidues());
      // fill the low position, the strnad and the length fields, according to
      // the selected sequence
      int[] positions = getSelectedSequencePositions();
      int length = Math.abs(positions[1] - positions[0]) + 1;
      if (positions[0] < positions[1]) {
        requestPositionTextField.setText(String.valueOf(positions[0]));
        requestStrandComboBox.setSelectedIndex(0);
      }
      else {
        requestPositionTextField.setText(String.valueOf(positions[1]));
        requestStrandComboBox.setSelectedIndex(1);
      }
      requestLengthTextField.setText(String.valueOf(length));
    }
    // disable the startCheckBox and stopCheckBox if there is no gene
    SeqFeature feature;
    boolean hasGenes = false;
    // all the features
    FeatureSet featSet = (FeatureSet) curationState.getCurationSet()
        .getAnnots();
    for (int i = 0; i < featSet.size(); i++) {
      feature = (SeqFeature) featSet.getFeatureAt(i);
      if (feature.getFeatureType().compareTo("gene") == 0) {
        hasGenes = true;
      }
    }
    if (hasGenes == false) {
      startCheckBox.setEnabled(false);
      stopCheckBox.setEnabled(false);
    }

    // Call initParameters of all the panel composed JTabbedPane
    for (int i = 0; i < methodPane.getTabCount(); i++) {
      ((InteractionSearchMethodPanel) methodPane.getComponentAt(i)).initParameters();
    }
  }

  /**
   * Check that all the parameters entered by the user follow contraints
   * 
   * @return true if all the constraints are respected
   */
  public boolean checkParameters()
  {
    boolean isCorrect = true;
    String message = "";

    // check that the request sequence is correct
    if (checkRequestSequence() == false) {
      message = message.concat("Bad definition of the request sequence.<br>");
      isCorrect = false;
    }
    // check that genomic positions have a value
    else if ((startPositionTextField.getText().compareTo("") == 0)
        || (stopPositionTextField.getText().compareTo("") == 0)) {
      message = message.concat("Enter genomic positions.<br>");
      isCorrect = false;
    }
    else {
      int start = Integer.parseInt(startPositionTextField.getText());
      int stop  = Integer.parseInt(stopPositionTextField.getText());

      // check that position are included in genomic positions
      if ((start < genomicStart) || (start > genomicEnd)
          || (stop < genomicStart) || (stop > genomicEnd)) {
        message = message
            .concat("Positions aren't included in genomic sequence.<br>");
        isCorrect = false;
      }
      // check that start is inferior to stop
      if (start > stop) {
        message = message
            .concat("Start position is superior to stop position.<br> ");
        isCorrect = false;
      }
      // check that the region definition has a correct syntax
      if (checkDefinedRegion() == false) {
        message = message
            .concat("Defined region expression has a bad syntax. " +
                "See the search region information.<br>");
        isCorrect = false;
      }
      // check that if a codon check box is selected, at least one of the
      // textfields has a value
      if (startCheckBox.isSelected() == true) {
        int nStartBef, nStartAft;
        if (startBeforeTextField.getText().compareTo("") == 0) {
          nStartBef = 0;
        }
        else {
          nStartBef = Integer.parseInt(startBeforeTextField.getText());
        }
        if (startAfterTextField.getText().compareTo("") == 0) {
          nStartAft = 0;
        }
        else {
          nStartAft = Integer.parseInt(startAfterTextField.getText());
        }
        if ((nStartBef == 0) && (nStartAft == 0)) {
          message = message
              .concat("Around start codon is checked but no value is entered.<br>");
          isCorrect = false;
        }
        if (   (nStartBef > MAX_AROUND_CODON) || (nStartBef < 0)
            || (nStartAft > MAX_AROUND_CODON) || (nStartAft < 0)) {
          message = message
              .concat("Value around start codon has to be inferior to "
                  + MAX_AROUND_CODON + ".<br>");
          isCorrect = false;
        }
      }

      if (stopCheckBox.isSelected() == true) {
        int nStopBef, nStopAft;
        if (stopBeforeTextField.getText().compareTo("") == 0) {
          nStopBef = 0;
        }
        else {
          nStopBef = Integer.parseInt(stopBeforeTextField.getText());
        }
        if (stopAfterTextField.getText().compareTo("") == 0) {
          nStopAft = 0;
        }
        else {
          nStopAft = Integer.parseInt(stopAfterTextField.getText());
        }
        if ((nStopBef == 0) && (nStopAft == 0)) {
          message = message
              .concat("Around stop codon is checked but no value is entered.<br>");
          isCorrect = false;
        }
        if (   (nStopBef > MAX_AROUND_CODON) || (nStopBef < 0)
            || (nStopAft > MAX_AROUND_CODON) || (nStopAft < 0)) {
          message = message
              .concat("Value around stop codon has to be inferior to "
                  + MAX_AROUND_CODON + ".<br>");
          isCorrect = false;
        }
      }
    }

    // if a parameter is incorrect, display the error
    if (isCorrect == false) {
      JOptionPane.showMessageDialog(this, "<html>" + message + "</html>",
          "Error", JOptionPane.ERROR_MESSAGE);
    }

    // check method parameters
    boolean isMethodParamCorrect = ((InteractionSearchMethodPanel) methodPane
        .getSelectedComponent()).checkParameters();
    if (isMethodParamCorrect == false) {
      isCorrect = false;
    }
    return isCorrect;
  }

  /**
   * Check that the parameters about request sequence are corrects
   * 
   * @return True if the request sequence parameters are correct
   */
  public boolean checkRequestSequence()
  {
    boolean isCorrect = true;

    // if the request sequence is described by a position, a length and a strand
    if (positionRadioButton.isSelected() == true) {
      // check that the fields Position and Length aren't empty
      if (   (requestPositionTextField.getText().compareTo("") == 0)
          || (requestLengthTextField.getText().compareTo("")   == 0)) {
        isCorrect = false;
      }
      else {
        // check that the positions of the sequence are included in the genomic
        // sequence positions and that the length sequence is correct
        int start   = Integer.parseInt(requestPositionTextField.getText());
        int length  = Integer.parseInt(requestLengthTextField.getText());
        int stop    = length + start;
        if (   (start < genomicStart) || (start  > genomicEnd)
            || (stop  > genomicEnd)   || (length < InteractionSearch.getSizeMin())
            || (length > InteractionSearch.getSizeMax())) {
          isCorrect = false;
        }
      }
    } // if the request sequence is directly written
    else if (sequenceRadioButton.isSelected() == true) {
      String sequenceText = sequenceTextField.getText();
      // check that the sequence field is not empty and is composed of letters
      // A, T, G and C
      if (   (sequenceText.compareTo("") == 0)
          || (sequenceText.toLowerCase().matches("(a|t|g|c)*") == false) ) {
        isCorrect = false;
      }
      // check that the length of the sequence is correct
      else if ((sequenceText.length() < InteractionSearch.getSizeMin())
          ||   (sequenceText.length() > InteractionSearch.getSizeMax())) {
        isCorrect = false;
      }
    }
    return isCorrect;
  }

  /**
   * Return true if the defined region expression is correct
   * 
   * @return True if the defined region expression is correct
   */
  public boolean checkDefinedRegion()
  {
    boolean isCorrect = true;

    if (definedRegionCheckBox.isSelected() == true) {
      String region = definedRegionTextField.getText();
      // check the syntax of the expression
      if (   (region.compareTo("") == 0)
          || (region.matches("(([0-9]+-[0-9]+,)*([0-9]+-[0-9]+))?") == false)) {
        isCorrect = false;
      }
      else {
        String[] splitResult;
        int start, stop; // region positions
        int index = 0;
        boolean isEnd = false;
        //      array of regions. A region is a string like 'start-stop'
        String[] regions = region.split(","); 

        while (isEnd == false) {
          // if all the regions was read
          if (index >= regions.length) {
            isEnd = true;
          }
          else {
            splitResult = regions[index].split("-");
            start       = Integer.parseInt(splitResult[0]);
            stop        = Integer.parseInt(splitResult[1]);
            // check that positions are included in genomic positions and
            // that start position is lower than stop position
            if ((  start < genomicStart) || (start > genomicEnd)
                || (stop < genomicStart) || (stop > genomicEnd)
                || (start > stop)) {
              isCorrect = false;
              isEnd     = true;
            }
          }
          index++;
        }
      }
    }
    return isCorrect;
  }

  /**
   * Compute and return the positions of the selected sequence
   * 
   * @return An array composed of the start position and the stop position. If
   *         start position is higher that the stop position, it means that the
   *         selected sequence is on the strand minus.
   */
  int[] getSelectedSequencePositions()
  {
    int[] positions = new int[2];
    // the sequence is a annotated feature
    if ((selectedSequence.getLow() > 0) && (selectedSequence.getHigh() > 0)) {
      if (selectedSequence.getStrand() == Target.STRAND_PLUS) {
        positions[0] = selectedSequence.getLow();
        positions[1] = selectedSequence.getHigh();
      }
      else {
        positions[0] = selectedSequence.getHigh();
        positions[1] = selectedSequence.getLow();
      }
    } // the sequence is a mouse selection
    else {
      // parse the sequence name to get the position
      String name = selectedSequence.getName();
      // true if the sequence is in the reverse strand
      boolean isReverse = name.matches("(.*reverse strand.*)");
      // delete the string "reverse strand" in the name
      name = name.replaceAll("( \\(reverse strand\\) )", " ");
      // split the name to get the positions
      String[] splitResult = name.split("( from )|( to )");
      if (isReverse == false) {
        positions[0] = Integer.parseInt(splitResult[1]);
        positions[1] = Integer.parseInt(splitResult[2]);
      }
      else {
        positions[0] = Integer.parseInt(splitResult[2]);
        positions[1] = Integer.parseInt(splitResult[1]);
      }
    }
    return positions;
  }

  /**
   * Return a sequence object representing the request sequence
   * 
   * @return A sequence object representing the request sequence
   */
  public Sequence getRequestSequence()
  {
    return requestSequence;
  }

  /**
   * Create a sequence object, representing the request sequence
   */
  public void createRequestSequence()
  {
    // "selected sequence" radio button is selected
    if (selectedRadioButton.isSelected() == true) {
      requestSequence = new Sequence("", selectedSequence.getResidues());
      // get the positions of the selected sequence
      int[] positions = getSelectedSequencePositions();
      requestSequence.setRange(new Range(positions[0], positions[1]));

      // the selected sequence is a annotated feature so get the reference
      // sequence name
      if ((selectedSequence.getLow() > 0) && (selectedSequence.getHigh() > 0)) {
        requestSequence.setName(selectedSequence.getRefSequence().getName());
      }
      // the selected sequence is a mouse selection. Get its name parsing the
      // name attribute
      else {
        String[] splitResult = selectedSequence.getName().split(" ");
        // save the name of the reference sequence
        requestSequence.setName(splitResult[0]); 
      }
    } // "position" radio button is selected
    else if (positionRadioButton.isSelected() == true) {
      int requestHighPos = Integer.parseInt(requestPositionTextField.getText());
      int stop = requestHighPos
          + Integer.parseInt(requestLengthTextField.getText()) - 1;
      // case strand plus
      if (requestStrandComboBox.getSelectedIndex() == 0) {
        requestSequence = new Sequence("", genomicSequence.getResidues(
            requestHighPos, stop));
        requestSequence.setRange(new Range(requestHighPos, stop));
      }
      else { // case strand minus
        requestSequence = new Sequence("", genomicSequence.getResidues(stop,
            requestHighPos));
        requestSequence.setRange(new Range(stop, requestHighPos));
      }
      // save the name of the genomic sequence
      requestSequence.setName(genomicSequence.getName());
    } // "nucleotidic sequence" radio button is selected
    else {
      requestSequence = new Sequence("", sequenceTextField.getText());
    }
  }

  /**
   * @return List composed of the parameters entered by the user
   */
  public ArrayList getRegionsParams()
  {
    ArrayList regionParams = new ArrayList();
    // region parameters
    String definedRegion;
    int nStartBefore;
    int nStartAfter;
    int nStopBefore;
    int nStopAfter;

    // get the value of the defined region string
    if (definedRegionCheckBox.isSelected() == true) {
      definedRegion = definedRegionTextField.getText();
    }
    else {
      definedRegion = "";
    }
    // get the value of the start before parameter
    if (   (startBeforeTextField.getText().compareTo("") == 0)
        || (startCheckBox.isSelected() == false)) {
      nStartBefore = 0;
    }
    else {
      nStartBefore = Integer.parseInt(startBeforeTextField.getText());
    }
    // get the value of the stop before parameter
    if (   (stopBeforeTextField.getText().compareTo("") == 0)
        || (stopCheckBox.isSelected() == false)) {
      nStopBefore = 0;
    }
    else {
      nStopBefore = Integer.parseInt(stopBeforeTextField.getText());
    }
    // get the value of the start after parameter
    if (   (startAfterTextField.getText().compareTo("") == 0)
        || (startCheckBox.isSelected() == false)) {
      nStartAfter = 0;
    }
    else {
      nStartAfter = Integer.parseInt(startAfterTextField.getText());
    }
    // get the value of the stop aftere parameter
    if (   (stopAfterTextField.getText().compareTo("") == 0)
        || (stopCheckBox.isSelected() == false)) {
      nStopAfter = 0;
    }
    else {
      nStopAfter = Integer.parseInt(stopAfterTextField.getText());
    }

    // fill the list with the parameters
    // add the positions start and stop
    regionParams.add(new Integer(startPositionTextField.getText()));
    regionParams.add(new Integer(stopPositionTextField.getText()));
    // add the defined regions
    regionParams.add(definedRegion);
    // add the strand
    regionParams.add(new Byte(new Integer(strandComboBox.getSelectedIndex())
        .byteValue()));
    // add startbefore, startafter, stopbefore, stopafter
    regionParams.add(new Integer(nStartBefore));
    regionParams.add(new Integer(nStartAfter));
    regionParams.add(new Integer(nStopBefore));
    regionParams.add(new Integer(nStopAfter));
    // add the boolean intergenic
    regionParams.add(new Boolean(intergenicCheckBox.isSelected()));

    return regionParams;
  }

  /**
   * If parameters are ok, call the research of the interactions if targets are
   * found, create a new window to display the results and close itself
   */
  public void interactionSearchCall()
  {
    if (checkParameters() == true) {
      createRequestSequence(); // create the request sequence object
      if (((InteractionSearchMethodPanel) methodPane.getSelectedComponent())
          .allowResearch() == true) {
        // create an execution thread which search interaction
        ThreadWait waitThread = new ThreadWait((JFrame) this.getParent(),
            "Searching interactions...");
        waitThread.start();
        SearchThread searchThread = new SearchThread(waitThread);
        searchThread.start();
      }
    }
  }

  /**
   * Exit of the current window
   */
  public void windowExit()
  {
    hide();
    dispose();
  }

  /**
   * Build the JDialog which displays the text containing in the doc.
   * 
   * @param doc Document to display in the jdialog
   */
  public void buildInformationDialog(StyledDocument doc)
  {
    JPanel groupPanel = new JPanel(new BorderLayout());
    JPanel buttonPanel = new JPanel(true);
    JButton button = new JButton("OK");
    JTextPane messagePane = new JTextPane();
    messagePane.setEditable(false);
    messagePane.setBackground(Color.white);
    messagePane.setStyledDocument(doc);

    button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e)
      {
        informationDialog.setVisible(false);
      }
    });
    buttonPanel.add(button);
    buttonPanel.setBackground(bgColor);
    button.setBackground(InteractionSearchSetDialog.buttonColor);

    groupPanel.add(messagePane, BorderLayout.CENTER);
    groupPanel.add(buttonPanel, BorderLayout.SOUTH);
    groupPanel.setBackground(bgColor);
    informationDialog = new JDialog(this, "More about search regions", false);
    informationDialog.getContentPane().add(groupPanel, BorderLayout.CENTER);
    informationDialog.pack();
  }

  /**
   * Build a document which describes the search interaction regions.
   * 
   * @return styled document containing description of the search interaction
   *         regions
   */
  public StyledDocument createDocument()
  {
    String title = "How define the search regions?\n";
    String introduction = "The search regions are characterized by properties. " +
        "Only the regions that satisfy ALL the selected properties \nare explored. " +
        "See below for examples of properties and underneath the corresponding search " +
        "regions painted in gray:\n" +
        "(start codons are painted in green, stop codons in red)\n\n";

    // define styles
    StyledDocument doc = new DefaultStyledDocument();

    Style defaultStyle = StyleContext.getDefaultStyleContext().getStyle(
        StyleContext.DEFAULT_STYLE);
    Style commonStyle = doc.addStyle("common", defaultStyle);
    StyleConstants.setFontFamily(commonStyle, "Courier");
    Style titleStyle  = doc.addStyle("standard", defaultStyle);
    Style grayStyle   = doc.addStyle("gray", commonStyle);
    Style redStyle    = doc.addStyle("red", commonStyle);
    Style greenStyle  = doc.addStyle("green", commonStyle);
    Style paramStyle  = doc.addStyle("blue", commonStyle);

    StyleConstants.setBold(titleStyle, true);
    StyleConstants.setFontSize(titleStyle, 14);
    StyleConstants.setForeground(paramStyle, Color.BLUE);
    StyleConstants.setBackground(grayStyle, Color.GRAY);
    StyleConstants.setBackground(redStyle, Color.RED);
    StyleConstants.setBackground(greenStyle, Color.GREEN);

    // texts to display in the document
    String[] descriptions = {
        "* Genomic positions ", "500", " to ", "1500", 
        " AND Strand ", "+",
        ":\n                   500                                    1500\n",
        "                    |                                       |              \n",
        "....................", ".........................................", 
        "......................",
        "\n\n\n* Genomic positions ", "1", " to ", "2000", 
        " AND Defined regions ", "100-500,1000-1500",
        " AND Strand ", "+",
        ":\n1  100             500              1000                  1500                  2000\n",
        "|   |               |                 |                     |                     |\n",
        "....", ".................", ".................", 
        ".......................", "......................",
        "\n\n\n* Genomic positions ", "500", " to ", "1500", 
        " AND Around start codon: -", "10", " nt AND Strand ", "+",
        ":\n",
        "                   500                                    1500\n",
        "                    |     10nt         10nt                 |\n",
        ".............", "............", "......", "...", "....", "......", "...", 
        "..................", "...", "...............",
        "\n\n\n* Genomic positions ", "1", " to ", "2000", 
        " AND Strand ", "+", " AND Intergenic regions:\n",
        "1                                                                               2000\n",
        "|                                                                                 |\n",
        "...............................", // ig
        "...",  "...", "...", "....", "...",// start, gene, stop, ig, start
        "..", "...", ".............", // gene, stop, ig,
        "...", "..", "...", "..........\n"//  start, gene, stop, ig
        };

    // styles applying to the text containing in descriptions
    Style[] styles = {
        commonStyle, paramStyle, commonStyle, paramStyle, 
        commonStyle, paramStyle, 
        commonStyle, 
        commonStyle, 
        commonStyle, grayStyle, 
        commonStyle, 
        commonStyle, paramStyle, commonStyle, paramStyle, 
        commonStyle, paramStyle,
        commonStyle, paramStyle,
        commonStyle, 
        commonStyle,
        commonStyle, grayStyle, commonStyle, 
        grayStyle, commonStyle, 
        commonStyle, paramStyle, commonStyle, paramStyle, 
        commonStyle, paramStyle, commonStyle, paramStyle, 
        commonStyle,
        commonStyle, 
        commonStyle, 
        commonStyle, commonStyle, grayStyle, greenStyle, commonStyle, grayStyle, greenStyle, 
        commonStyle, greenStyle, commonStyle, 
        commonStyle, paramStyle, commonStyle, paramStyle, 
        commonStyle, paramStyle, commonStyle,
        commonStyle, 
        commonStyle, 
        grayStyle, 
        greenStyle,commonStyle, redStyle, grayStyle, greenStyle, 
        commonStyle, redStyle, grayStyle, 
        greenStyle, commonStyle, redStyle, grayStyle};

    // write on the document the text with the correponding style
    try {
      doc.insertString(doc.getLength(), title, titleStyle);
      doc.insertString(doc.getLength(), introduction, defaultStyle);
      for (int i = 0; i < descriptions.length; i++) {
        doc.insertString(doc.getLength(), descriptions[i], styles[i]);
      }

    }
    catch (Exception e) {
      System.err.println(e);
    }
    return doc;
  }

  /**
   * This class is a independent thread that search interactions
   */
  class SearchThread extends Thread
  {
    private ThreadWait tWait;

    public SearchThread(ThreadWait thread)
    {
      tWait = thread;
    }

    public void run()
    {
      int selectedIndex = methodPane.getSelectedIndex();

      // search the targets with the selected method
      LinkedList targetList = interactionSearch.search(requestSequence,
          getRegionsParams(), selectedIndex,
          ((InteractionSearchMethodPanel) methodPane
              .getComponentAt(selectedIndex)).getParameters());

      // close the jprogressBar window
      tWait.stopWait();

      //	if no target was found
      if (targetList != null) {
        if (targetList.size() <= 0) {
          JOptionPane.showMessageDialog(null, "No target was found. "
              + "Try again with other parameters.", "Warning",
              JOptionPane.ERROR_MESSAGE);
        }
        else {
          // create a new window to display the resulting targets
          windowExit();
          InteractionResultFrame resultFrame = new InteractionResultFrame(
              targetList, interactionSearch);
          // create its information dialog and put it the information text
          resultFrame.createInfoDialog(
              ((InteractionSearchMethodPanel) 
                  methodPane.getComponentAt(selectedIndex)).createInfoDocument());
          resultFrame.setVisible(true);
        }
      }
    }
  }

  /**
   * Return the selected sequence
   * 
   * @return The selected sequence
   */
  public SeqFeature getSelectedSequence()
  {
    return selectedSequence;
  }

  /**
   * Return the interaction search manager
   * 
   * @return The interaction search manager
   */
  public InteractionSearch getInteractionSearch()
  {
    return interactionSearch;
  }

}
