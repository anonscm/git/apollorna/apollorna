package apollo.gui;

import apollo.datamodel.*;

/**
 * Normalized Dinucleotide variable Score Calculator
 */
public class NormalizedDinucleotideScoreCalculator extends ComputeAllScoreCalculator {

    char X;
    char Y;

    public NormalizedDinucleotideScoreCalculator(CurationSet curation, int winSize, char X_arg, char Y_arg) {
	super(winSize, curation);
	X=X_arg;
	Y=Y_arg;
	updateScores();
    }

    public void updateScores() {    
      int winSize = getWinSize();
      if (computedWinSize != winSize) {
	computedWinSize = winSize;

	char [] seqChars = curation.getRefSequence().getResidues( curation.getLow(), curation.getHigh() ).toCharArray();
	char c_remove, c_add, c_before, c_after;
	int l;
	int nXY,i,j;
	double sd, m, s;

	l = seqChars.length;
	scores = new double[l];
	
	i = halfWinSize;
	nXY=0;
	c_before='B'; // not an IUPAC nucleotide for index -1 on the sequence
	for (j= i - halfWinSize; j<i + halfWinSize +1; j++) {
	    c_add = seqChars[j];
	    if (c_add==Y && c_before==X) nXY+=1;
            c_before = c_add;
	}
	scores[i] = nXY;
	score_min = nXY;
	score_max = nXY;
	score_mean = nXY;
	
	c_remove=seqChars[0];
	for (i=halfWinSize+1; i<l-halfWinSize; i++) {
	    c_after = seqChars[i - halfWinSize];
	    if (c_remove==X)
		if (c_after==Y) nXY-=1;
	    c_remove=c_after;
	    c_add = seqChars[i + halfWinSize];
	    if (c_add==Y)
		if (c_before==X) nXY+=1;
            c_before = c_add;
	    scores[i] = nXY;
	    score_min = Math.min(nXY, score_min);
	    score_max = Math.max(nXY, score_max);
	    score_mean += nXY;
	}
	score_mean = score_mean / (double)(l-winSize+1);
	
	for (i=0; i<halfWinSize; i++) scores[i] = score_mean;
	for (i=l-halfWinSize; i<l; i++) scores[i] = score_mean;

	sd=0;
	for (i=0; i<l; i++) sd+= Math.pow(scores[i]-score_mean,2);
	sd = Math.sqrt(sd/(double)l);
	m = 0;
	score_min=Double.POSITIVE_INFINITY;
	score_max=Double.NEGATIVE_INFINITY;
	for (i=0; i<l; i++) {
	    if (sd!=0) s= (scores[i]-score_mean)/sd;
	    else s=scores[i]-score_mean;
	    scores[i]= s;
	    score_min = Math.min(s, score_min);
	    score_max = Math.max(s, score_max);
	    m += s;
	}
	score_mean = m/(double)l;

	// For test
	//writeFile("Norm"+X+Y, l);
      }
    }

}
